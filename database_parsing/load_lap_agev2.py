import os

from pandocfilters import attributes

from attributes import FacialAttributes
from glob import glob
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt


def round_to_base(x, base = 5):
    return int(base * round(float(x)/base))

def parse_database(metadata_file, root_images_dir, separator=','):

    attributes = []
    images = []
    with open(metadata_file) as file:
        lines = file.readlines()
        print(len(lines))

        for idx in range(1, len(lines)):
            line = lines[idx]
            line = line.lstrip().rstrip()
            line = line.split(separator)

            if len(line) < 3:
                continue
            photo_name = line[0]

            mean_age = round_to_base(float(line[1]))
            std_dev = float(line[2])

            images.append(os.path.join(root_images_dir, photo_name))
            attr_val = {FacialAttributes.AGE.value: mean_age}
            attributes.append(attr_val)

    return images, attributes

if __name__ == '__main__':
    images, attrs = parse_database('D:/work/image-databases/looking at people age/cvpr2016/test/test_gt/test_gt.csv',
                                   'D:/work/image-databases/looking at people age/cvpr2016/test/test_1')

    a, b = parse_database('D:/work/image-databases/looking at people age/cvpr2016/training/train_gt/train_gt.csv',
                                   'D:/work/image-databases/looking at people age/cvpr2016/training/train_1')
    images.extend(a)
    attrs.extend(b)
    a, b = parse_database('D:/work/image-databases/looking at people age/cvpr2016/validation/valid_gt/valid_gt.csv',
                                   'D:/work/image-databases/looking at people age/cvpr2016/validation/valid')
    images.extend(a)
    attrs.extend(b)

    a, b = parse_database('D:/work/image-databases/looking at people age/cvpr2015/test/TestLabels/Reference.csv',
                          'D:/work/image-databases/looking at people age/cvpr2015/test/Test', ';')
    images.extend(a)
    attrs.extend(b)

    a, b = parse_database('D:/work/image-databases/looking at people age/cvpr2015/training/TrainLabels/Train.csv',
                          'D:/work/image-databases/looking at people age/cvpr2015/training/Train', ';')
    images.extend(a)
    attrs.extend(b)

    a, b = parse_database('D:/work/image-databases/looking at people age/cvpr2015/validation/ValidationLabels/Reference.csv',
                          'D:/work/image-databases/looking at people age/cvpr2015/validation/Validation', ';')
    images.extend(a)
    attrs.extend(b)

    print(np.amin(attrs))
    print(np.amax(attrs))
    print(len(attrs))
    labels, values = zip(*Counter(attrs).items())
    print(Counter(attrs))

    indexes = np.arange(len(labels))
    width = 10

    plt.bar(indexes, values, width)
    plt.xticks(indexes + width * 0.5, labels)
    plt.show()