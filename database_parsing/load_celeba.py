import re
import os
import shutil
import random

celeb_attributes_names =  ["5_o_Clock_Shadow",
    "Arched_Eyebrows",
    "Attractive",
    "Bags_Under_Eyes",
    "Bald",
    "Bangs",
    "Big_Lips",
    "Big_Nose",
    "Black_Hair",
    "Blond_Hair",
    "Blurry",
    "Brown_Hair",
    "Bushy_Eyebrows",
    "Chubby",
    "Double_Chin",
    "Eyeglasses",
    "Goatee",
    "Gray_Hair",
    "Heavy_Makeup",
    "High_Cheekbones",
    "Male",
    "Mouth_Slightly_Open",
    "Mustache",
    "Narrow_Eyes",
    "No_Beard",
    "Oval_Face",
    "Pale_Skin",
    "Pointy_Nose",
    "Receding_Hairline",
    "Rosy_Cheeks",
    "Sideburns",
    "Smiling",
    "Straight_Hair",
    "Wavy_Hair",
    "Wearing_Earrings",
    "Wearing_Hat",
    "Wearing_Lipstick",
    "Wearing_Necklace",
    "Wearing_Necktie",
    "Young"]


def parse_attributes(attributes_path):

    global celeb_attributes_names
    celeb_data = []
    num_images = 0
    with open(attributes_path, 'r') as atributes_file:
        lines = atributes_file.readlines()
        assert(len(lines) > 3)

        # first line is the number of images line
        num_images = int(lines[0])
        # second line is the header
        for line in lines[2:]:
            values = re.split(" +", line)
            img_name = values[0]
            attibutes_map = {}
            for i in range(0, len(celeb_attributes_names)):
                attr = celeb_attributes_names[i]
                value = values[i + 1]

                attibutes_map[attr] = int(value)
            attibutes_map["image"] = img_name

            celeb_data.append(attibutes_map)
    print("found ", len(celeb_data), ' files')
    print("expected ", num_images)
    print(celeb_data[0])
    return celeb_data


def get_samples_with_attribute(samples, attrib_name):
    selected_samples = []
    for sample in samples:
        try:
            if sample[attrib_name] == 1:
                selected_samples.append(sample)
        except KeyError:
            print(sample)
    return selected_samples


def get_samples_with_attributes(samples, attrib_names):
    selected_samples = []
    for _ in range(0, len(attrib_names)):
        selected_samples.append([])

    for sample in samples:
        for i in range(0, len(attrib_names)):
            attrib_name = attrib_names[i]
            if sample[attrib_name] != 1:
                selected_samples[i].append(sample)

    return selected_samples


def parse_images_for_attributes(input_image_dir, output_dir, samples, attributes):
    selected_samples = get_samples_with_attributes(samples, attributes)

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    for i in range(0, len(selected_samples)):
        output_sample_dir = os.path.join(output_dir, attributes[i])

        if not os.path.exists(output_sample_dir):
            os.mkdir(output_sample_dir)
        random.shuffle(samples)

        for sample in selected_samples[i]:

            image_path = os.path.join(input_image_dir, sample['image'])
            image_path = image_path.replace('jpg', 'png')
            print('Copy image ', image_path, ' to ', output_sample_dir)
            shutil.copy(image_path, output_sample_dir)
    return

def parse_landmarks():
    return

def parse_bbox():
    return

if __name__ == "__main__":
    celeb_data = parse_attributes("D:/work/image-databases/celeba/Anno-20170721T124736Z-001/Anno/list_attr_celeba.txt")
    parse_images_for_attributes("D:/work/image-databases/celeba/all/Img/img_align_celeba_png/img_align_celeba_png/",
                                'D:/generated/female-celeb-a', celeb_data, ['Male'])


    # bald_celebs = get_samples_with_attribute(celeb_data, 'Bald')
    # brown_hair_celebs = get_samples_with_attribute(celeb_data, 'Brown_Hair')
    # blond_hair_celebs = get_samples_with_attribute(celeb_data, 'Blond_Hair')
    # black_hair_celebs = get_samples_with_attribute(celeb_data, 'Black_Hair')
    # grey_hair_celebs = get_samples_with_attribute(celeb_data, 'Gray_Hair')
    # print('num bald celebs', len(bald_celebs))
    # print('num brown hair celebs', len(brown_hair_celebs))
    # print('num black hair celebs', len(black_hair_celebs))
    # print('num blond hair celebs', len(blond_hair_celebs))
