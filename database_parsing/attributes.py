from enum import Enum

class FacialAttributes(Enum):
    AGE = 0,
    GENDER = 1,
    SKIN_LIGHT = 2,
    SKIN_MEDIUM = 3,
    SKIN_DARK = 4,
    HAIR_BALD = 5,
    HAIR_BLACK = 6