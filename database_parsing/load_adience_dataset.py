import os
from datetime import date, timedelta
from attributes import FacialAttributes

def round_to_base(x, base = 5):
    return int(base * round(float(x)/base))

def get_age(age_string):
    age_string = age_string.replace(')', '')
    age_string = age_string.replace('(', '')

    age_interval = age_string.rstrip().lstrip().split(',')

    age1 = int(age_interval[0])
    age1_rounded = round_to_base(age1)
    age2 = int(age_interval[1])
    age2_rounded = round_to_base(age2)

    if(abs(age1 - age1_rounded) < abs(age2 - age2_rounded)):
        return age1_rounded
    return age2_rounded


def parse_database(filepath, images_dir = None):
    images = []
    attributes = []
    with open(filepath, 'r') as f:
        adience_metadata = f.readlines()
        # user_id	original_image	face_id	age	gender	x	y	dx	dy	tilt_ang	fiducial_yaw_angle	fiducial_score
        for idx in range(1, len(adience_metadata)):
            line = adience_metadata[idx].lstrip().rstrip()

            info =  line.split()

            # if age is none ignore sample
            if (info[3] == 'None' or (not info[3].startswith('('))):
                continue

            attr_dict = {}
            user_id = info[0]
            face_id = info[2]
            image_name = 'coarse_tilt_aligned_face.'+str(face_id)+'.'+info[1]

            original_image = os.path.join(user_id, image_name)
            img_path = original_image if images_dir is None else os.path.join(images_dir, original_image)

            age_interval = info[3] + info[4]
            gender = info[5]
            if info[5] != 'm' and info[5] != 'f':
                continue

            # print(age_interval)
            attr_dict[FacialAttributes.AGE.value] = get_age(age_interval)
            attr_dict[FacialAttributes.GENDER.value] = gender

            # print(img_path , ' -> ', attr_dict)
            images.append(img_path)
            attributes.append(attr_dict)


    print('Found %d valid data ' % len(images))
    return images, attributes


if __name__ == '__main__':
    parse_database('D:/work/image-databases/adience/AgeGender-FaceImageProject/fold_0_data.txt')
    parse_database('D:/work/image-databases/adience/AgeGender-FaceImageProject/fold_1_data.txt')
    parse_database('D:/work/image-databases/adience/AgeGender-FaceImageProject/fold_2_data.txt')
    parse_database('D:/work/image-databases/adience/AgeGender-FaceImageProject/fold_3_data.txt')