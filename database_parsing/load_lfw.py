import os
from attributes import FacialAttributes
from glob import glob


def parse_lfw_gender(metadata_file, gender_tag, root_images_dir):
    files = [y for x in os.walk(root_images_dir) for y in glob(os.path.join(x[0], '*.jpg'))]

    images = []
    with open(metadata_file) as file:
        lines = file.readlines()
        print(len(lines))
        for line in lines:
            line = line.lstrip().rstrip()
            # print(line)
            # if 'Alan' in line:
            #     print(line)
            for image in files:
                # if 'Alan' in image:
                #     print('--->'+image)
                if line.lower() in image.lower():
                    images.append(image)
                    # print('found ', len(images))
                    break

    attr_val = {FacialAttributes.GENDER.value: gender_tag}
    attributes = [attr_val]*len(images)

    return images, attributes

if __name__ == '__main__':
    images, attributes = parse_lfw_gender('D:/work/image-databases/lfw/female_names.txt', 'f', 'D:/work/image-databases/lfw')
    a, b = parse_lfw_gender('D:/work/image-databases/lfw/male_names.txt', 'm', 'D:/work/image-databases/lfw')

    images.extend(a)
    attributes.extend(b)

    print('Found %d images' % len(images))