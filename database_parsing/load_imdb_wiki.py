import os
import time
import math
import scipy.io
import datetime
import shutil
from datetime import date, timedelta
from attributes import FacialAttributes

def get_age(photo_taken, dob):
    d_photo = date(photo_taken, 7, 1)
    dob_datetime = date.fromordinal(int(dob))
    age = abs(d_photo-dob_datetime).days / 365.25
    return round(age)


def round_to_base(x, base = 5):
    return int(base * round(float(x)/base))

"""
The mat file has the following structre:
dob: date of birth (Matlab serial date number)
photo_taken: year when the photo was taken
full_path: path to file
gender: 0 for female and 1 for male, NaN if unknown
name: name of the celebrity
face_location: location of the face. To crop the face in Matlab run
img(face_location(2):face_location(4),face_location(1):face_location(3),:))
face_score: detector score (the higher the better). Inf implies that no face was found in the image and the face_location then just returns the entire image
second_face_score: detector score of the face with the second highest score. This is useful to ignore images with more than one face. second_face_score is NaN if no second face was detected.
celeb_names (IMDB only): list of all celebrity names
celeb_id (IMDB only): index of celebrity name"""

def parse_database(mat_filepath, images_dir = None):

    imdb_wiki_data = scipy.io.loadmat(mat_filepath)['imdb']
    dob = imdb_wiki_data['dob'][0][0][0]
    photo_taken  = imdb_wiki_data['photo_taken'][0][0][0]
    full_path = imdb_wiki_data['full_path'][0][0][0]
    gender = imdb_wiki_data['gender'][0][0][0]

    images = []
    attributes = []
    for idx in range(0, len(dob)):

        img_path = full_path[idx][0] if images_dir is None else os.path.join(images_dir, full_path[idx][0])

        attr_dict = {}
        attr_dict[FacialAttributes.AGE.value] = get_age(photo_taken[idx], dob[idx])
        attr_dict[FacialAttributes.GENDER.value] = 'f' if gender[idx] == 0 else 'm'

        images.append(img_path)
        attributes.append(attr_dict)

    return images, attributes

def split_imdbwiki_gender(filepath, imagesdir, outdir):

    images, attributes = parse_database(filepath, imagesdir)

    male_dir = None
    female_dir = None
    if not os.path.exists(outdir):
        os.mkdir(outdir)
        male_dir = os.path.join(outdir, 'm')
        female_dir = os.path.join(outdir, 'f')
        os.mkdir(male_dir)
        os.mkdir(female_dir)
    print('created folders')
    for idx in range(0, len(images)):
        print(images[idx])
        print(attributes[idx][FacialAttributes.GENDER.value])
        if attributes[idx][FacialAttributes.GENDER.value] == 'm':
            shutil.copy(images[idx], male_dir)
        if attributes[idx][FacialAttributes.GENDER.value] == 'f':
            shutil.copy(images[idx], female_dir)

    return


def split_imdbwiki_gender(filepath, imagesdir, outdir):

    images, attributes = parse_database(filepath, imagesdir)

    male_dir = None
    female_dir = None
    if not os.path.exists(outdir):
        os.mkdir(outdir)
        male_dir = os.path.join(outdir, 'm')
        female_dir = os.path.join(outdir, 'f')
        os.mkdir(male_dir)
        os.mkdir(female_dir)
    print('created folders')
    for idx in range(0, len(images)):
        print(images[idx])
        print(attributes[idx][FacialAttributes.GENDER.value])
        if attributes[idx][FacialAttributes.GENDER.value] == 'm':
            shutil.copy(images[idx], male_dir)
        if attributes[idx][FacialAttributes.GENDER.value] == 'f':
            shutil.copy(images[idx], female_dir)

    return

def split_imdbwiki_age(filepath, imagesdir, outdir):

    images, attributes = parse_database(filepath, imagesdir)


    if not os.path.exists(outdir):
        os.mkdir(outdir)

    print('created folders')
    age_dict = {}
    for idx in range(0, len(images)):
        print(images[idx])
        at_orig = attributes[idx][FacialAttributes.AGE.value]
        at = round_to_base(at_orig)
        print(at_orig, '->', at)
        if at not in age_dict:

            age_dict[at] = os.path.join(outdir, str(at))
            if not os.path.exists(age_dict[at]):
                os.mkdir(age_dict[at])

        shutil.copy(images[idx], age_dict[at])



    return

if __name__ == '__main__':
    filepath = 'D:/work/image-databases/IMDB-WIKI/imdb_meta/imdb/imdb.mat'
    images_dir = 'D:/work/image-databases/IMDB-WIKI/imdb_crop'
    split_imdbwiki_age(filepath, images_dir, 'D:/age-imdb-wiki')
    # images, attributes = parse_database(filepath)
    # print(len(images))

