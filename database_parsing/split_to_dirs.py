import os
import shutil
from attributes import FacialAttributes
import load_adience_dataset as adience_dataset
import load_lfw as lfw
import load_imdb_wiki as imdb_wiki
import load_lap_agev2 as lap

def split_database_by_attribute(attr_type, images, attributes, outdir):

    assert (len(images) == len(attributes))

    # if os.path.exists(outdir):
    #     shutil.rmtree(outdir)

    if not os.path.exists(outdir):
        os.mkdir(outdir)
    classes_dir_dict = {}

    for idx in range(0, len(images)):
        if attributes[idx][attr_type.value] is not None:
            val =  str(attributes[idx][attr_type.value])
            out_class_path = os.path.join(outdir, val)
            if not val in classes_dir_dict:
                if not os.path.exists(out_class_path):
                    os.mkdir(out_class_path)
                classes_dir_dict[val] = out_class_path
            if os.path.exists(images[idx]):
                shutil.copy(images[idx], out_class_path)
            else:
                print('error: ', images[idx], ' does not exist')
        else:
            print(str(attr_type)+' does not exists')

    return


if __name__ == '__main__':
    print('Split database by gender')

    images, attrs = lap.parse_database('D:/work/image-databases/looking at people age/cvpr2016/test/test_gt/test_gt.csv',
                                   'D:/work/image-databases/looking at people age/cvpr2016/test/test_1')

    a, b = lap.parse_database('D:/work/image-databases/looking at people age/cvpr2016/training/train_gt/train_gt.csv',
                                   'D:/work/image-databases/looking at people age/cvpr2016/training/train_1')
    images.extend(a)
    attrs.extend(b)
    a, b = lap.parse_database('D:/work/image-databases/looking at people age/cvpr2016/validation/valid_gt/valid_gt.csv',
                                   'D:/work/image-databases/looking at people age/cvpr2016/validation/valid')
    images.extend(a)
    attrs.extend(b)

    a, b = lap.parse_database('D:/work/image-databases/looking at people age/cvpr2015/test/TestLabels/Reference.csv',
                          'D:/work/image-databases/looking at people age/cvpr2015/test/Test', ';')
    images.extend(a)
    attrs.extend(b)

    a, b = lap.parse_database('D:/work/image-databases/looking at people age/cvpr2015/training/TrainLabels/Train.csv',
                          'D:/work/image-databases/looking at people age/cvpr2015/training/Train', ';')
    images.extend(a)
    attrs.extend(b)

    a, b = lap.parse_database('D:/work/image-databases/looking at people age/cvpr2015/validation/ValidationLabels/Reference.csv',
                          'D:/work/image-databases/looking at people age/cvpr2015/validation/Validation', ';')
    images.extend(a)
    attrs.extend(b)

    # images, attr = imdb_wiki.parse_database()
    # images, attr = adience_dataset.parse_database('D:/work/image-databases/adience/AgeGender-FaceImageProject/fold_0_data.txt',
    #                                               'D:/work/image-databases/adience/faces')
    # a, b = adience_dataset.parse_database('D:/work/image-databases/adience/AgeGender-FaceImageProject/fold_1_data.txt',
    #                                       'D:/work/image-databases/adience/faces')
    # images.extend(a)
    # attr.extend(b)
    #
    # a, b  = adience_dataset.parse_database('D:/work/image-databases/adience/AgeGender-FaceImageProject/fold_2_data.txt',
    #                                        'D:/work/image-databases/adience/faces')
    # images.extend(a)
    # attr.extend(b)
    #
    # a, b = adience_dataset.parse_database('D:/work/image-databases/adience/AgeGender-FaceImageProject/fold_3_data.txt',
    #                                       'D:/work/image-databases/adience/faces')
    # images.extend(a)
    # attr.extend(b)

    # images, attr = lfw.parse_lfw_gender('D:/work/image-databases/lfw/female_names.txt', 'f',
    #                                       'D:/work/image-databases/lfw')
    # a, b = lfw.parse_lfw_gender('D:/work/image-databases/lfw/male_names.txt', 'm', 'D:/work/image-databases/lfw')
    #
    # images.extend(a)
    # attr.extend(b)

    split_database_by_attribute(FacialAttributes.AGE, images, attrs, 'age1')
    # print(len(images))