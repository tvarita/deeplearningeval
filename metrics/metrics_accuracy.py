import numpy as np

iu_per_class = {}


def calculate_all_accuracies(truth, pred, classes):
    masks_truth = {}
    masks_pred = {}
    for c in classes:
        iu_per_class[c] = 0
        masks_truth[c] = create_class_mask(truth, c)
        masks_pred[c] = create_class_mask(pred, c)

    pixel_acc = calculate_pixel_accuracy(truth, pred, classes, masks_truth, masks_pred)
    mean_pixel_acc = calculate_mean_accuracy(truth, pred, classes, masks_truth, masks_pred)
    mean_iu = calculate_iu(truth, pred, classes, masks_truth, masks_pred)
    freq_w_iu = calculate_freq_weighted_IU(truth, pred, classes, masks_truth, masks_pred)

    # pixel_acc2 = pixel_accuracy(pred,truth)
    # mean_pixel_acc2 = mean_accuracy(pred,truth)
    # mean_iu2 = mean_IU(pred,truth)
    # freq_w_iu2 = frequency_weighted_IU(pred,truth)
    #
    # print("pixel_acc2", pixel_acc2)
    # print("mean_pixel_acc2", mean_pixel_acc2)
    # print("mean_iu2", mean_iu2)
    # print("freq_w_iu2", freq_w_iu2)
    # print()
    return pixel_acc, mean_pixel_acc, mean_iu, freq_w_iu


def calculate_freq_weighted_IU(truth, pred, classes, masks_truth=None, masks_pred=None):
    sum = 0
    t_k = 0
    for c in classes:

        mask_class_truth = create_class_mask(truth, c) if masks_truth is None else masks_truth[c]
        mask_class_pred = create_class_mask(pred, c) if masks_pred is None else masks_pred[c]
        t_i = np.sum(mask_class_truth)
        t_k += t_i

        match = np.logical_and(mask_class_pred, mask_class_truth)
        n_ii = np.sum(match)

        s_nji = calculate_sum2(classes, c, truth, mask_class_pred, masks_truth)
        if n_ii == 0 or t_i == 0 or s_nji == 0:
            continue
        sum += (t_i * n_ii) / (t_i + s_nji - n_ii)
    result = sum / t_k
    return result


def calculate_iu(truth, pred, classes, masks_truth=None, masks_pred=None):
    sum = 0
    for c in classes:
        mask_class_truth = create_class_mask(truth, c) if masks_truth is None else masks_truth[c]
        mask_class_pred = create_class_mask(pred, c) if masks_pred is None else masks_pred[c]
        t_i = np.sum(mask_class_truth)
        match = np.logical_and(mask_class_pred, mask_class_truth)
        n_ii = np.sum(match)
        s_nji = calculate_sum2(classes, c, truth, mask_class_pred, masks_truth)
        if n_ii == 0 or t_i == 0 or s_nji == 0:
            continue
        sum += n_ii / (t_i + s_nji - n_ii)
    result = sum / len(classes)
    return result


def calculate_mean_accuracy(truth, pred, classes, masks_truth=None, masks_pred=None):
    sum = 0

    for c in classes:
        mask_class_truth = create_class_mask(truth, c) if masks_truth is None else masks_truth[c]
        mask_class_pred = create_class_mask(pred, c) if masks_pred is None else masks_pred[c]
        t_i = np.sum(mask_class_truth)
        match = np.logical_and(mask_class_pred, mask_class_truth)
        n_ii = np.sum(match)

        if t_i != 0:
            sum += n_ii / t_i
    result = sum / len(classes)
    return result


def calculate_pixel_accuracy(truth, pred, classes, masks_truth=None, masks_pred=None):
    total_n_ii = 0
    t_i = 0

    for c in classes:
        mask_class_truth = create_class_mask(truth, c) if masks_truth is None else masks_truth[c]
        mask_class_pred = create_class_mask(pred, c) if masks_pred is None else masks_pred[c]
        t_i += np.sum(mask_class_truth)

        n_ii = np.logical_and(mask_class_pred, mask_class_truth)

        total_n_ii += np.sum(n_ii)

    if t_i != 0:
        result = total_n_ii / t_i
    else:
        result = 0
    return result


def create_class_mask(image, cls):
    mask = image == cls
    return mask


def calculate_sum2(classes, c, truth, mask_class_pred, masks_truth=None):
    s = 0
    for c2 in classes:
        # if c != c2:
        # are from class c2 but predicted c
        mask_class_truth_c2 = create_class_mask(truth, c2) if masks_truth is None else masks_truth[c2]
        match = np.logical_and(mask_class_truth_c2, mask_class_pred)
        s += np.sum(match)

    return s


def confusion_matrix(eval_segm, gt_segm, **kwargs):
    merged_maps = np.bitwise_or(np.left_shift(gt_segm.astype('uint16'), 8), eval_segm.astype('uint16'))
    hist = np.bincount(np.ravel(merged_maps))
    nonzero = np.nonzero(hist)[0]
    pred, label = np.bitwise_and(255, nonzero), np.right_shift(nonzero, 8)
    class_count = np.array([pred, label]).max() + 1
    conf_matrix = np.zeros([class_count, class_count], dtype='uint64')
    conf_matrix.put(pred * class_count + label, hist[nonzero])
    return conf_matrix


def calculate_iu_per_class(classes, iu_per_class, pred_squeeze, truth_squeeze):
    # true positive / (true positive + false positive + false negative)
    matrix = confusion_matrix(truth_squeeze, pred_squeeze)
    for idx, c in enumerate(classes):
        tp = matrix[idx, idx]
        fp = np.sum(matrix[idx, :]) - tp  # sum form the row
        fn = np.sum(matrix[:, idx]) - tp
        class_iu = tp / (tp + fp + fn)
        iu_per_class[c].append(class_iu)
