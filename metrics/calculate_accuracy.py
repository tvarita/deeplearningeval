import numpy as np


def calculate_accuracy(y_true,y_pred):
    assert len(y_pred) == len(y_true)
    sum = np.sum(y_true == y_pred)
    if len(y_pred) != 0:
        return sum / len(y_pred)
    else:
        return None
