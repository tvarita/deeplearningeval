import glob
import os

import cv2

from annotation.faceAnnotation.find_points_util import get_ordered_insertion_index, get_smooth_area_inside_points, get_point_position_in_lists, \
    get_point_index_in_list, get_point_position_in_list, \
    get_point_position_in_all_lists
# from info_image import setup_info_img
# from mapper_util import get_color_base_on_key
# from mode_enum import Mode
# from utils_glue import exit_glue_mode
from annotation.faceAnnotation.info_image import setup_info_img
from annotation.faceAnnotation.mapper_util import get_color_base_on_key
from annotation.faceAnnotation.mode_enum import Mode
from annotation.faceAnnotation.utils_glue import exit_glue_mode

found_point_for_move = None


def decrement_values_less_than(elements, val):
    for i in range(len(elements)):
        if elements[i] > val:
            elements[i] -= 1


def click_and_crop(event, x, y, flags, params):
    global found_point_for_move
    img_base_name, classes, img, clone, refPts, selected_point_idx, found_class_idx, last_points, working_class, glue_point_list, mode = params

    if event == cv2.EVENT_RBUTTONDOWN:
        # case on right click when add a new control point (at correct position)
        _found_cls_idx, point_idx = get_point_position_in_lists([x, y], refPts)
        if point_idx == -1:
            idx = get_ordered_insertion_index(refPts[working_class[-1]], [x, y])
            refPts[working_class[-1]][idx:idx] = [[x, y]]
            # first we have to increment all indexes (larger than the current inserted) in (last_points)
            for i in range(len(last_points[working_class[-1]])):
                if last_points[working_class[-1]][i] >= idx:
                    last_points[working_class[-1]][i] += 1
            last_points[working_class[-1]].append(idx)
            img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])
            cv2.imshow(img_base_name, img)

    elif event == cv2.EVENT_LBUTTONUP:
        # case when we drop the selected control point (in move edit mode)
        if mode[0] == Mode.MOVE_POINT:
            mode[0] = Mode.NORMAL
            selected_point_idx[0] = None
            found_class_idx[0] = None

            # print("-->EXIT edit mode")
            img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])
            cv2.imshow(img_base_name, img)

    elif event == cv2.EVENT_MBUTTONDOWN:

        if mode[0] == Mode.NORMAL:
            msg = None
            # _found_cls_idx, point_idx = get_point_position_in_lists([x, y], refPts)
            # point_idx = get_point_position_in_list([x, y], refPts[working_class[-1]])
            points_idx_list_map = get_point_position_in_all_lists([x, y], refPts)
            for point_idx,class_idx in points_idx_list_map:
                del refPts[class_idx][point_idx]
                last_points[class_idx].remove(point_idx)
                decrement_values_less_than(last_points[class_idx], point_idx)
            if len(points_idx_list_map) == 0:
                print("-->didn't find that point in the working class")
                msg = "->didn't find that point in the working class<-"

            img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])
            cv2.imshow(img_base_name, img)
            setup_info_img(classes, working_class, glue_point_list, mode, msg=msg)

    elif event == cv2.EVENT_LBUTTONDOWN:

        msg = None
        if mode[0] == Mode.GLUE_HELP:
            # Case when we add initial points from the previous list (glue help mode)
            # find the annotation class that we want to glue with
            _found_cls_idx, point_idx = get_point_position_in_lists([x, y], refPts)
            if found_class_idx[0] is not None:
                point_idx_in_current_list = get_point_position_in_list([x, y], refPts[found_class_idx[0]])
                if point_idx_in_current_list != -1:
                    _found_cls_idx = found_class_idx[0]

            if point_idx != -1:
                if found_class_idx[0] is None or _found_cls_idx == found_class_idx[0]:
                    # mark as a glue class
                    found_class_idx[0] = _found_cls_idx
                    # if the point not already inserted in the selectd points list for glue
                    p = refPts[_found_cls_idx][point_idx]
                    if get_point_index_in_list(glue_point_list, p) == -1:
                        glue_point_list.append(p)
                        if len(glue_point_list) == 3:
                            exit_glue_mode(glue_point_list, refPts, found_class_idx, working_class, last_points,
                                           mode)
                            img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])
                            cv2.imshow(img_base_name, img)
                else:
                    print("Please select pt from the same shape")
                    msg = "->Please select a pt from the same shape"

        else:
            _found_cls_idx, point_idx = get_point_position_in_lists([x, y], refPts)
            if point_idx == -1 and _found_cls_idx == -1:
                # case of left click, when add a new (NORMAL) control point
                if mode[0] == Mode.NORMAL:
                    # print("-->NEW control point added")
                    refPts[working_class[-1]].append([x, y])
                    last_points[working_class[-1]].append(len(refPts[working_class[-1]]) - 1)
                    img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])
                    cv2.imshow(img_base_name, img)
            else:
                if mode[0] == Mode.MOVE_POINT:
                    selected_point_idx[0] = None
                    mode[0] = Mode.NORMAL
                else:
                    # if the point exist in the current working list make sure that we found it
                    point_idx = get_point_position_in_list([x, y], refPts[working_class[-1]])
                    if point_idx != -1:
                        _found_cls_idx = working_class[-1]
                        mode[0] = Mode.MOVE_POINT
                        selected_point_idx[0] = point_idx
                        found_class_idx[0] = _found_cls_idx
                        found_point_for_move = refPts[_found_cls_idx][point_idx]
                    else:
                        print("Can edit only the working class")
                        msg = "-->didn't find that point in the working class<--"

                    img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])
                    cv2.imshow(img_base_name, img)
        setup_info_img(classes, working_class, glue_point_list, mode, msg=msg)

    else:
        # case when we walk on image (in edit mode)
        if mode[0] == Mode.MOVE_POINT:
            found_point_for_move[0] = x
            found_point_for_move[1] = y
            img, ok = re_draw_points_and_shape(clone[0], refPts, found_class_idx[0], draw_contour=True,
                                               transparency=0.7)
            cv2.imshow(img_base_name, img)


def get_smooth_areas(refPtsList):
    smooth_areas = []
    for pts in refPtsList:
        smooth_area = get_smooth_area_inside_points(pts)
        smooth_areas.append(smooth_area)
    return smooth_areas


def make_sure_dir_exists(dir):
    if not os.path.isdir(dir):
        os.mkdir(dir)


def get_out_dir_basename(images_directory, name="pretty_img_"):
    s = images_directory.split("/")
    if s[-1] == '':
        s.pop()
        dir_name = s.pop()
    else:
        dir_name = s.pop()
    return "/".join(s) + "/" + name + dir_name + "/"


def fill_poly_with_areas(image, smooth_areas, colors=None):
    for i, smooth_area in enumerate(smooth_areas):
        if len(smooth_area) > 3:
            if colors is None:
                color = get_color_base_on_key(i)
            else:
                color = colors[i]
            cv2.fillPoly(image, pts=[smooth_area], color=color)
    return image


def re_draw_points_and_shape(clone, refPts, class_contour_idx, draw_contour=False, smooth_area=None, transparency=0.5):
    img = clone.copy()
    return draw_points_and_shape(img, refPts, class_contour_idx, draw_contour, smooth_area, transparency)


def draw_points_and_shape(img, refPtsList, class_contour_idx, draw_contour, smooth_area, transparency):
    for refPts in refPtsList:
        for pt in refPts:
            # cv2.circle(img, (pt[0], pt[1]), 2, (0, 0, 255), -1)
            cv2.rectangle(img, (pt[0]-3, pt[1]-3),(pt[0]+3, pt[1]+3), (0, 255, 255))

    return draw_shape_on_imgs(refPtsList, img, class_contour_idx, draw_contour, smooth_area, transparency)


def draw_shape_on_imgs(refPtsList, img, class_contour_idx, draw_contour, smooth_areas, transparency):
    ok = False

    if smooth_areas is None:
        smooth_areas = get_smooth_areas(refPtsList)


    if draw_contour:
        color = (0, 0, 255)
        for pt in smooth_areas[class_contour_idx]:
            img[pt[0][1]:pt[0][1] + 1, pt[0][0]:pt[0][0] + 1] = color

    for i, smooth_area in enumerate(smooth_areas):
        color = get_color_base_on_key(i)
        if len(smooth_area) > 0:
            _full_filled_img = img.copy()
            cv2.fillPoly(_full_filled_img, pts=[smooth_area], color=color)
            cv2.addWeighted(_full_filled_img, 1 - transparency, img, transparency, 0, img)
            ok = True
    return img, ok


def rezise_points(refPts, factor_x, factor_y, enlarge: bool):
    for i, pts in enumerate(refPts):
        for p in pts:
            point_idx, _ = get_point_position_in_lists(p, refPts[i + 1:], margin=1)
            if point_idx == -1:
                if enlarge:
                    p[0] = p[0] * factor_x
                    p[1] = p[1] * factor_y
                else:
                    p[0] = p[0] / factor_x
                    p[1] = p[1] / factor_y


def points_to_int(refPts):
    for pts in refPts:
        for p in pts:
            p[0] = int(p[0])
            p[1] = int(p[1])


def remove_prefix_mark(images_directory, extensions, mark_prefix):
    img_paths = []
    for extension in extensions:
        img_paths.extend([path for path in glob.glob(pathname=images_directory + "/**/*." + extension, recursive=True)])
    for img_path in img_paths:
        os.rename(img_path, img_path.replace(mark_prefix, ""))


def add_to_resize_factor(factor, resize_factor_size):
    # if -1 <= factor + resize_factor_size <= 1:
    if abs(factor) == 1:
        factor = (1 + resize_factor_size)
        # factor = 1 + resize_factor_size - abs(factor + 1)
    else:
        factor += resize_factor_size
    return factor


def subtract_to_resize_factor(factor, resize_factor_size):
    # if -1 <= factor - resize_factor_size <= 1:
    if abs(factor) == 1:
        # factor = -(1 + (resize_factor_size - abs(factor - 1)))
        factor = -(1 + resize_factor_size)
    else:

        factor -= resize_factor_size
    return factor


def mixed_copy_list(refPts, list_idx_to_copy):
    # print("mixed_copy_list",list_idx_to_copy)
    new_list = []
    for p in refPts[list_idx_to_copy]:
        class_idx, position = get_point_position_in_lists(p, refPts, margin=1)
        if class_idx != -1 :
            new_list.append(p)
        else:
            new_list.append([p[0], p[1]])
    return new_list
