import cv2


def resize_below_threshold(img, img_out_size=250):
    original_size = img.shape
    height = original_size[0]
    width = original_size[1]
    if height > img_out_size or width > img_out_size:
        scaling_factor = img_out_size / float(height)
        if img_out_size / float(width) < scaling_factor:
            scaling_factor = img_out_size / float(width)
        return cv2.resize(img,None,fx=scaling_factor,fy=scaling_factor,interpolation=cv2.INTER_AREA)

def get_new_resize_shape(original_width, original_height, resize_factor):
    if resize_factor <= -1:
        new_width = abs(round(original_width / resize_factor))
        new_height = abs(round(original_height / resize_factor))
    else:
        new_width = round(original_width * resize_factor)
        new_height = round(original_height * resize_factor)
    return new_width, new_height

