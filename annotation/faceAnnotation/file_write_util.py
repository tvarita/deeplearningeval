import os
import xlwt
from lxml import etree


def write_info_about_classes(classes, out_dir):
    book = xlwt.Workbook()
    sheet1 = book.add_sheet("Sheet 1")
    sheet1.write(0, 0, "Class index")
    sheet1.write(0, 1, "Class name")
    for i, class_name in enumerate(classes):
        row = i + 2
        sheet1.write(row, 0, i+1)
        sheet1.write(row, 1, class_name)
    book.save(os.path.join(out_dir, "class_info.xls"))


def write_image_contours_to_xml(img_name, img_size, contours, xml_file_path, classes):
    if not os.path.exists(xml_file_path):
        root = etree.Element('annotations')
        tree = etree.ElementTree(root)
    else:
        parser = etree.XMLParser(remove_blank_text=True)
        tree = etree.parse(xml_file_path, parser)
        root = tree.getroot()

    annotation_node = etree.SubElement(root, "image", name=img_name,
                                       size="(" + str(img_size[1]) + " " + str(img_size[0]) + ")")
    for i, pts in enumerate(contours):
        if len(pts) != 0:
            str_pts = ""
            for p in pts:
                str_pts += "[" + str(p[0]) + " " + str(p[1]) + "],"
            str_pts = str_pts[:-1]
            etree.SubElement(annotation_node, classes[i].replace(" ", "")).text = str_pts

    tree.write(xml_file_path, pretty_print=True)
