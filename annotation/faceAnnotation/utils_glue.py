from annotation.faceAnnotation.find_points_util import get_all_points_contained
from annotation.faceAnnotation.mode_enum import Mode


def exit_glue_mode(glue_point_list,refPts,found_class_idx,working_class,last_points,mode):
    print("Exit  helper mode")
    # Exit the help mode and initialize current refPts list
    assert len(glue_point_list) > 0
    prev_len_size = len(refPts[found_class_idx[0]])
    inserted_count, initial_ref_pts = get_all_points_contained(refPts[found_class_idx[0]], glue_point_list)
    refPts[working_class[-1]] = initial_ref_pts

    for i in range(inserted_count):
        last_points[found_class_idx[0]].append(prev_len_size + i)
    last_points[working_class[-1]] = [i for i in range(len(initial_ref_pts))]

    mode[0] = Mode.NORMAL
    glue_point_list.clear()