import glob
import os
import numpy as np
import cv2
import copy

# import sys
# sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import sys

from info_image import setup_info_img
from mode_enum import Mode
from resize_utils import resize_below_threshold, get_new_resize_shape
from utils import get_out_dir_basename, make_sure_dir_exists, re_draw_points_and_shape, \
    click_and_crop, \
    get_smooth_areas, fill_poly_with_areas, decrement_values_less_than, rezise_points, \
    points_to_int, remove_prefix_mark, subtract_to_resize_factor, add_to_resize_factor, mixed_copy_list
from file_write_util import write_info_about_classes, write_image_contours_to_xml

selected_point_idx = [None]
# one list element
found_class_idx = [None]
# one list element
img_base_name = ""
mark_prefix = "ANNOTATED-"
extensions = ["jpg","jpeg","png","bmp"]
start_zoom = False
start_zoom_value = 0
# set to None if you want to work with original size image
img_out_size = 250
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))



def zoom_plus():
    global resize_factor, new_width, new_height, img_resize, img, refactor_to_origin, enlarge_to_origin, enlarge, ok
    resize_factor = add_to_resize_factor(resize_factor, resize_factor_size)
    new_width, new_height = get_new_resize_shape(original_width, original_height, resize_factor)
    img_resize = cv2.resize(original_image, dsize=(new_width, new_height))
    clone[0] = img_resize.copy()
    img = clone[0].copy()
    refactor_to_origin = abs(resize_factor) - resize_factor_size
    enlarge_to_origin = False
    if resize_factor <= 1:
        enlarge_to_origin = True
        refactor_to_origin = abs(resize_factor) + resize_factor_size
    enlarge = True
    if resize_factor < -1:
        enlarge = False
    rezise_points(refPts, refactor_to_origin, refactor_to_origin, enlarge=enlarge_to_origin)
    rezise_points(refPts, abs(resize_factor), abs(resize_factor), enlarge=enlarge)
    points_to_int(refPts)
    img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])

def zoom_minus():
    global resize_factor, new_width, new_height, refactor_to_origin, enlarge_to_origin, enlarge, img_resize, img, ok
    resize_factor = subtract_to_resize_factor(resize_factor, resize_factor_size)
    new_width, new_height = get_new_resize_shape(original_width, original_height, resize_factor)
    refactor_to_origin = abs(resize_factor) - resize_factor_size
    enlarge_to_origin = True
    if resize_factor >= -1:
        enlarge_to_origin = False
        refactor_to_origin = abs(resize_factor) + resize_factor_size
    enlarge = True
    if resize_factor < -1:
        enlarge = False
    img_resize = cv2.resize(original_image, dsize=(new_width, new_height))
    clone[0] = img_resize.copy()
    img = clone[0].copy()
    rezise_points(refPts, refactor_to_origin, refactor_to_origin, enlarge=not enlarge)
    rezise_points(refPts, abs(resize_factor), abs(resize_factor), enlarge=enlarge)
    points_to_int(refPts)
    img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])


if __name__ == "__main__":
    resize_factor_size = 0.5
    resize_factor = 1
    classes = []
    with open("config") as f:
        start_content = 0
        content = f.readlines()
        if content[0].strip() == "zoom":
            start_zoom = True
            start_content += 1
        images_directory = content[start_content].strip()
        for line in content[start_content+1:]:
            classes.append(line.strip())
    colors_class = [(i + 1, i + 1, i + 1) for i in range(len(classes))]
    out_dir = get_out_dir_basename(images_directory)
    out_dir = os.path.join(ROOT_DIR, out_dir)
    out_dir2 = get_out_dir_basename(images_directory, "class_seg_")
    out_dir2 = os.path.join(ROOT_DIR, out_dir2)
    make_sure_dir_exists(out_dir)
    make_sure_dir_exists(out_dir2)
    annotation_xml_file_path = os.path.join(out_dir2, "annotations.xml")
    write_info_about_classes(classes, out_dir2)

    msg = None

    # remove_prefix_mark(images_directory, extensions, mark_prefix)
    img_paths = []
    for extension in extensions:
        img_paths.extend([path for path in glob.glob(pathname=images_directory + "/**/*." + extension, recursive=True)])

    for img_idx, img_path in enumerate(img_paths):
        resize_factor = 1
        img_base_name = os.path.basename(img_path)
        if img_base_name.find(mark_prefix) == -1:
            working_class = [0]
            img = cv2.imread(img_path, cv2.IMREAD_COLOR)

            if img_out_size is not None:
                if img.shape[0] > img_out_size or img.shape[1] > img_out_size:
                    img = resize_below_threshold(img,img_out_size)
                    cv2.imwrite(img_path,img)
            refPts = [[] for _ in range(len(classes))]
            last_points = [[] for _ in range(len(classes))]
            glue_point_list = []
            mode = [Mode.NORMAL]

            original_size = img.shape
            original_height = original_size[0]
            original_width = original_size[1]

            clone = [img.copy()]
            original_image = img.copy()

            working_img_height, working_img_width, ch = img.shape

            setup_info_img(classes, working_class, glue_point_list, mode, msg=msg)

            cv2.namedWindow(img_base_name)
            cv2.setMouseCallback(img_base_name, click_and_crop,
                                 [img_base_name, classes, img, clone, refPts, selected_point_idx, found_class_idx,
                                  last_points, working_class, glue_point_list, mode])
            if start_zoom:
                for i in range(abs(start_zoom_value)):
                    if start_zoom_value > 0:
                        zoom_plus()
                    else:
                        zoom_minus()

            while True:

                cv2.imshow(img_base_name, img)
                key = cv2.waitKey(0) & 0xFF

                if key == ord("r") or key == ord("R"):
                    img = clone[0].copy()
                    pretty_save_img = np.zeros((working_img_height, working_img_width, 3), np.uint8)
                    for i in range(len(refPts)):
                        refPts[i].clear()
                        last_points[i].clear()
                    working_class[-1] = 0
                    selected_point_idx[0] = None
                    found_class_idx[0] = None
                    mode[0] = Mode.NORMAL
                    glue_point_list.clear()

                    setup_info_img(classes, working_class, glue_point_list, mode, msg=msg)
                    img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])

                elif key == 27:
                    # ESC key
                    try:
                        if len(last_points[working_class[-1]]) > 0:
                            index_to_be_removed = last_points[working_class[-1]][-1]
                            refPts[working_class[-1]].pop(index_to_be_removed)
                            last_points[working_class[-1]].pop()
                            # after remove a point we have to decrease all the indexes larger than our index
                            decrement_values_less_than(last_points[working_class[-1]], index_to_be_removed)
                            selected_point_idx[0] = None
                            mode[0] = Mode.NORMAL
                            img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])
                        # else:
                        #     print("List is empty")

                    except Exception as e:
                        print(e)

                elif ord("0") <= key <= ord("9"):
                    k = (key - ord("0"))
                    if k < len(classes):
                        if len(refPts[working_class[-1]]) == 0:
                            working_class[-1] = k
                        else:
                            working_class.append(k)
                    else:
                        print("Not such class number", k)

                    img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])
                    setup_info_img(classes, working_class, glue_point_list, mode=mode, msg=msg)

                elif key == ord("e") or key == ord("E"):
                    msg = "Please choose a different class"
                    setup_info_img(classes, working_class, glue_point_list, mode=mode, msg=msg)
                    choose_correct_diff_class = False
                    while not choose_correct_diff_class:
                        key2 = cv2.waitKey(0)
                        if ord("0") <= key2 <= ord("9") and key2 != ord(str(working_class[-1])):
                            new_class = (key2 - ord("0"))

                            mode[0] = Mode.NORMAL
                            refPts[new_class] = mixed_copy_list(refPts,working_class[-1])
                            last_points[new_class] = copy.deepcopy(last_points[working_class[-1]])
                            refPts[working_class[-1]].clear()
                            last_points[working_class[-1]].clear()
                            working_class[-1] = new_class
                            msg = None
                            setup_info_img(classes, working_class, glue_point_list, mode=mode, msg=msg)
                            break
                        else:
                            msg = "Please choose a correct class.Waiting ..."
                            print("incorrect mode", mode[0])
                        setup_info_img(classes, working_class, glue_point_list, mode=mode, msg=msg)
                    img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])

                elif key == ord("s") or key == ord("S"):
                    file_name = os.path.basename(img_path)

                    enlarge = True if resize_factor <= -1 else False

                    rezise_points(refPts, abs(resize_factor), abs(resize_factor), enlarge)
                    points_to_int(refPts)

                    to_be_save_image_classes = np.zeros((original_height, original_width, 3), np.uint8)

                    smooth_areas = get_smooth_areas(refPts)

                    pretty_save_img = original_image.copy()
                    _full_filled_img = original_image.copy()
                    _full_filled_img = fill_poly_with_areas(_full_filled_img, smooth_areas)
                    cv2.addWeighted(pretty_save_img, 0.5, _full_filled_img, 0.5, 0, pretty_save_img)
                    pretty_file_name = os.path.join(out_dir, file_name)
                    cv2.imwrite(pretty_file_name, pretty_save_img)

                    new_file_name2 = os.path.join(out_dir2, file_name)
                    new_file_name2 = new_file_name2.replace('.jpg', '.bmp')
                    new_file_name2 = new_file_name2.replace('.jpeg', '.bmp')
                    new_file_name2 = new_file_name2.replace('.png', '.bmp')

                    to_be_save_image_classes = fill_poly_with_areas(to_be_save_image_classes, smooth_areas,
                                                                    colors_class)
                    to_be_save_image_classes = to_be_save_image_classes[:, :, 0]

                    cv2.imwrite(new_file_name2, to_be_save_image_classes)
                    write_image_contours_to_xml(file_name, original_size, refPts, annotation_xml_file_path, classes)

                    print("image saved to", new_file_name2)
                    msg = "->image saved  as" + file_name
                    cv2.destroyWindow(img_base_name)

                    root_path = os.path.dirname(os.path.abspath(img_path))
                    new_base_name = mark_prefix + img_base_name
                    os.rename(img_path, os.path.join(root_path, new_base_name))
                    break

                elif key == ord("g") or key == ord("G"):
                    if len(working_class) > 1:
                        if len(refPts[working_class[-1]]) == 0:

                            if mode[0] != Mode.GLUE_HELP:
                                print("Enter helper mode")
                                found_class_idx[0] = None
                                # Enter in help mode for the current class
                                mode[0] = Mode.GLUE_HELP
                                msg = None
                            else:
                                msg = "Already in help glue mode"
                        else:
                            print("->Please select a new class to enter in glue mode")
                            msg = "->Please select a new class to enter in glue mode"
                    else:
                        print("->Cannot enter in glue mode from first class")
                        msg = "->Cannot enter in glue mode from first class"
                    setup_info_img(classes, working_class, glue_point_list, mode=mode, msg=msg)
                    img, ok = re_draw_points_and_shape(clone[0], refPts, working_class[-1])

                elif key == ord("n"):
                    # refPts.clear():
                    cv2.destroyWindow(img_base_name)
                    break

                elif key == ord("+"):

                    start_zoom_value +=1

                    zoom_plus()

                elif key == ord("-"):
                    start_zoom_value -= 1
                    zoom_minus()
                else:
                    print("Incorrect key")

    cv2.destroyAllWindows()
