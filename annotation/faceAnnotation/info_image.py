import cv2
import numpy as np

from annotation.faceAnnotation.mapper_util import get_color_base_on_key
from annotation.faceAnnotation.mode_enum import Mode


def setup_info_img(classes, current_class, glue_point_list, mode, msg=None):
    unit_distance = 32
    current_class_idx = current_class[-1]
    w = 380
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 0.5
    font_color = (255, 0, 255)
    lineType = 1

    if mode[0] == Mode.EDIT_CLASS:
        if msg is not None:
            h = 230
            text_position = (0, int(h / 2))
            # text_position = (0, text_position[1] + unit_distance)
            info_img = np.zeros((h, w, 3), np.uint8)
            cv2.putText(info_img, msg,
                        text_position,
                        font,
                        fontScale,
                        font_color,
                        lineType)
    elif mode[0] != Mode.GLUE_HELP:
        texts = ["Left Mouse Btn - add control point",
                 "Right Mouse Btn - add (ordered) control point",
                 "Middle Mouse Btn - remove hover point",
                 "ESC - remove last added point",
                 "n : Move to next image",
                 "g : Enter in glue helper mode",
                 "r : restart annotation",
                 "s : save image",
                 "e : edit current class",
                 "+ : zoom out",
                 "- : zoom in"]

        h = len(texts) * (unit_distance + 2) + unit_distance * len(classes)
        if msg is not None:
            h += unit_distance
        info_img = np.zeros((h, w, 3), np.uint8)

        for i, txt in enumerate(texts):
            text_position = (0, unit_distance * (i + 1))
            cv2.putText(info_img, txt,
                        text_position,
                        font,
                        fontScale,
                        font_color,
                        lineType)
        last_i = i + 1
        texts = ["Class " + str(i) + ": " + cls + (" ok " if i in current_class and i != current_class[-1] else "") for
                 i, cls in enumerate(classes)]
        if msg is not None:
            texts.append(msg)
        for i, txt in enumerate(texts):
            font_color = (255, 0, 255)
            if i == current_class_idx:
                font_color = get_color_base_on_key(i)
            text_position = (0, unit_distance * (last_i + i + 1))
            cv2.putText(info_img, txt,
                        text_position,
                        font,
                        fontScale,
                        font_color,
                        lineType)
    else:
        h = 230
        info_img = np.zeros((h, w, 3), np.uint8)
        text_position = (0, int(h / 2))
        if len(glue_point_list) == 0:
            txt = "Please select a point from a class"
        else:
            txt = "You have selected " + str(len(glue_point_list)) + "/3 points"

        cv2.putText(info_img, txt,
                    text_position,
                    font,
                    fontScale,
                    font_color,
                    lineType)
        if msg is not None:
            text_position = (0, text_position[1] + unit_distance)
            cv2.putText(info_img, msg,
                        text_position,
                        font,
                        fontScale,
                        font_color,
                        lineType)

    cv2.imshow("info", info_img)
    cv2.moveWindow("info", 0, 0)
