import sys
from cx_Freeze import setup, Executable

setup(
    name = "Annotate",
    version = "1.0",
    description = "Annotation tool.",
    options = {"build_exe": {"packages": ["numpy"],"excludes":["scipy.spatial.cKDTree"]}},
    executables = [Executable("annotate.py", base = "Win32GUI")])