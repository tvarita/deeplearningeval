import numpy as np
import sys
from scipy.spatial import distance

from scipy.interpolate import splprep, splev


def get_point_betwen(p1, p2):
    w1 = 9
    w2 = 10-w1
    return [int( (w1*p1[0] + w2*p2[0]) / (w1+w2) ), int( ( w1*p1[1] + w2*p2[1] ) / (w1+w2)) ]


def get_all_points_contained(all_points, selected_pts):
    return_points = []

    first_selected = selected_pts[0]
    last_selected = selected_pts[len(selected_pts) - 1]
    middle_selected = selected_pts[int(len(selected_pts) / 2)]

    idx_first_selected = get_point_index_in_list(all_points, first_selected)
    idx_last_selected = get_point_index_in_list(all_points, last_selected)

    is_upper_shape = middle_selected[1] < first_selected[1] or middle_selected[1] < last_selected[1]

    if is_upper_shape:
        if all_points[idx_first_selected][0] < all_points[idx_last_selected][0]:
            direction = 1
        else:
            direction = -1
    else:
        if all_points[idx_first_selected][0] < all_points[idx_last_selected][0]:
            direction = -1
        else:
            direction = 1

    # if is_upper_shape:
    if direction == 1:
        next_first_selected = idx_first_selected + 1 if idx_first_selected + 1 < len(all_points) else 0
        next_last_selected = idx_last_selected - 1 if idx_last_selected - 1 >= 0 else len(all_points) - 1

        first_add = get_point_betwen(all_points[idx_first_selected], all_points[next_first_selected])
        last_add = get_point_betwen(all_points[idx_last_selected], all_points[next_last_selected])

        inserted_count = 0
        all_points[next_first_selected: next_first_selected] = [first_add]
        inserted_count += 1

        if idx_first_selected > idx_last_selected and next_first_selected != 0:
            all_points[idx_last_selected :idx_last_selected ] = [last_add]
        else:
            all_points[idx_last_selected + inserted_count:idx_last_selected + inserted_count] = [last_add]
        inserted_count += 1
    else:
        next_first_selected = idx_first_selected - 1 if idx_first_selected - 1 >= 0 else len(all_points) - 1
        next_last_selected = idx_last_selected + 1 if idx_last_selected + 1 < len(all_points) else 0

        first_add = get_point_betwen(all_points[idx_first_selected], all_points[next_first_selected])
        last_add = get_point_betwen(all_points[idx_last_selected], all_points[next_last_selected])

        inserted_count = 0
        all_points[next_last_selected: next_last_selected] = [last_add]

        inserted_count += 1
        if idx_first_selected < idx_last_selected and next_last_selected != 0:
            all_points[idx_first_selected :idx_first_selected] = [first_add]
        else:
            all_points[idx_first_selected + inserted_count:idx_first_selected + inserted_count] = [first_add]
        inserted_count += 1
    # else:
    #     if direction == -1:
    #         next_first_selected = idx_first_selected - 1 if idx_first_selected -1 >= 0 else len(all_points) - 1
    #         next_last_selected = idx_last_selected + 1 if idx_last_selected + 1 < len(all_points) else 0

    current_idx = get_point_index_in_list(all_points,first_selected)
    last_idx = get_point_index_in_list(all_points,last_selected)
    while current_idx != last_idx:
        return_points.append(all_points[current_idx])
        current_idx += direction
        if direction == -1 and current_idx == -1:
            current_idx = len(all_points) - 1
        if direction == 1 and current_idx == len(all_points):
            current_idx = 0
    return_points.append(all_points[current_idx])

    return inserted_count,return_points

def get_point_position_in_all_lists(pt, ptsList,margin = 4):
    points_idx_list_map = []
    for class_idx, pts in enumerate(ptsList):
        for i, p in enumerate(pts):
            if p[0] - margin <= pt[0] <= p[0] + margin and p[1] - margin <= pt[1] <= p[1] + margin:
                points_idx_list_map.append((i,class_idx))
    return points_idx_list_map

def get_point_position_in_list(pt, ptsList,margin = 4):

    for i, p in enumerate(ptsList):
        if p[0] - margin <= pt[0] <= p[0] + margin and p[1] - margin <= pt[1] <= p[1] + margin:
            return i
    return -1


def get_point_position_in_lists(pt, ptsList,margin = 4):
    for class_idx, pts in enumerate(ptsList):
        for i, p in enumerate(pts):
            if p[0] - margin <= pt[0] <= p[0] + margin and p[1] - margin <= pt[1] <= p[1] + margin:
                return class_idx, i
    return -1, -1


def get_smooth_area_inside_points(pts):
    if len(pts) > 3:
        x = np.array([item[0] for item in pts])
        y = np.array([item[1] for item in pts])
        # make shape circular
        okay = np.where(np.abs(np.diff(x)) + np.abs(np.diff(y)) > 0)
        # a = x[okay]
        xp = np.r_[x[okay], x[-1], x[0]]
        yp = np.r_[y[okay], y[-1], y[0]]
        tck, u = splprep([xp, yp], u=None, s=0, per=1)
        mm = max(p[0] for p in pts)
        u_new = np.linspace(u.min(), u.max(), 250)
        for p in pts:
            idx = np.searchsorted(u_new, p[0] / mm, side='right')
            if idx == 250:
                idx -= 1
            u_new[idx] = p[0] / mm

        x_new, y_new = splev(u_new, tck, der=0)
        res_array = [[[int(i[0]), int(i[1])]] for i in zip(x_new, y_new)]
        area = np.asarray(res_array, dtype=np.int32)
        return area
    else:
        return []


def find_points_in_smooth(pts, smooth_points):
    ctrl_point_indices = []
    dd = 9999999
    min_i = -1
    for p in pts:
        range_X = range(p[0] - 3, p[0] + 3)
        range_Y = range(p[1] - 3, p[1] + 3)
        inserted = False
        for i, smooth_p in enumerate(smooth_points):
            if smooth_p[0][0] in range_X and smooth_p[0][1] in range_Y:
                ctrl_point_indices.append(i)
                inserted = True
                break
            dst = distance.euclidean(smooth_p[0], p)
            if dd > dst:
                dd = dst
                min_i = i
        if not inserted:
            ctrl_point_indices.append(min_i)
    return ctrl_point_indices


def get_ordered_insertion_index(ref_pts: list, point):
    min_dist = 9999999
    idx_between_smth_point = 0
    smooth_pts = get_smooth_area_inside_points(ref_pts)

    if len(smooth_pts) > 0:

        ctrl_point_indices = find_points_in_smooth(ref_pts, smooth_pts)
        for i in range(len(smooth_pts)):
            next_i = i + 1 if (i < len(smooth_pts) - 1) else 0
            dst = distance.euclidean(smooth_pts[next_i][0], point) + distance.euclidean(smooth_pts[i][0], point)
            if dst < min_dist:
                min_dist = dst
                idx_between_smth_point = i

        if idx_between_smth_point == 0:
            print("get_ordered_insertion_index because of the 0 out of", len(ref_pts))
            return len(ref_pts)
        final_idx_2 = len(ref_pts)

        for i in range(len(ctrl_point_indices)):
            next_idx = i + 1 if i != len(ctrl_point_indices) - 1 else 0
            ctrl_idx = ctrl_point_indices[i]
            ctrl_idx_next = ctrl_point_indices[next_idx]
            if ctrl_idx <= idx_between_smth_point <= ctrl_idx_next:
                final_idx_2 = next_idx
                break
        return final_idx_2
    else:
        return len(ref_pts)


def get_point_from_list(point_list, search_point):
    margin = 4
    for i, pt in enumerate(point_list):
        if search_point[0] - margin <= pt[0] <= search_point[0] + margin and search_point[1] - margin <= pt[1] <= \
                        search_point[1] + margin:
            return True, pt
    return False, search_point


def get_point_index_in_list(list_pointS, search_point):
    for i, p in enumerate(list_pointS):
        if p[0] == search_point[0] and p[1] == search_point[1]:
            return i
    return -1
