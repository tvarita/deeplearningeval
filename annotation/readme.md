## Annotation Tool

Tool for annotation images with different classes.

### Usage
The program uses the config file: **config** where it is written the path to the input directory and
the list of possible classes, one on each line. The possible commands are shown in the
information screen. Those are:
1. Left click: add a **normal** control point at the end of the list.
1. Right click: add a control point in between the most close control points.
1. Middle click: remove the selected point.
1. ESC : remove the last added point.
1. n: move to the next image (without saving).
1. g: enter in **glue mode** (see **Glue** mode chapter).
1. r: restart the annotation (remove all annotated classes from current image).
1. s: save the modifications (see **Save** chapter).
1. e: edit (change) current selected class (with other avalaible class).
1. +: zoom in.
1. -: zoom out.
1. 0-n: Change the current working class (n: number of classes -1).

### Glue mode
When selecting a new class, you can press the **g** key to enter in glue mode. Select
3 points (contour of the shape you want to glue with -start, middle, end-)
and the current working class list will have all the points from the selected shape.

### Save
On save, a new images is created in the following way:
* background pixels have the value 0
* each annotated pixel has the value of the corresponding class
The image is save to directory **class_seg_**(input dir name).
* In plus, the control points of each shape are writen to the **annotation.xml** file
from the above mentioned folder.
* A **pretty**(original image plus annotations) image is saved in the directory **pretty_img_**(input dir name)
* To the name of the original image is add a the **ANNOTATED-** prefix. And it will not be
read next time for annotation.



