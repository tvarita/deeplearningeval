import cv2
import glob
import os
import sys
import json
import shutil
import requests
import tempfile
from time import sleep

# Endpoint: https://westcentralus.api.cognitive.microsoft.com/face/v1.0
# Key 1: e280e71051834a12b152d9e36f153fcb
# Key 2: 51b41aa9f14645c6b655a5d431b66485
# Key 3 facebook: fd324da06f9a46bb88d7238f3cff3e16
# key 4 tudor microsoft: bad7253b8367464888e75bd65835d788
# key 5 tudor github: 7562d55267574274a6be771e17bcf673
# key 6 tudor github: a367d30b04b44f2a8827f5018aeb544b
# key 7 linkedin tudor: 47c2ba9a66eb463a914b2be80bf95dd7
# key 8 facebook clau: 192814348730423bba44a4e3f485c638
# key 9 github gabi d65b96d1d10a444588f046f8fee80b57
# key 9 github gabi2 20409842a5d44fdca6e31da5fc40f6e9

g_subscription_key = "20409842a5d44fdca6e31da5fc40f6e9"
g_face_api_url = 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect'
input_dir = '/work/image-databases/imdb_crop/23/'
total = 0

def predict_image(image_path):
    global g_subscription_key
    global g_face_api_url
    global total
    # img = cv2.imread(image_path)
    # cv2.imshow('',img)
    # cv2.waitKey(0)
    headers = {'Ocp-Apim-Subscription-Key': g_subscription_key,
               'Content-Type': 'application/octet-stream'}

    bin_image = None
    with open(image_path, 'rb') as img_file:
        bin_image = img_file.read()

    assert bin_image
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
    }
    total +=1
    print("total: ",total)
    # response = requests.post(g_face_api_url, params=params, headers=headers, json={"url": bin_image})
    response = requests.post(g_face_api_url, params=params, headers=headers, data=bin_image)
    faces = response.json()
    image_data = {}
    face_idx = 0
    for face_info in faces:
        faces_json = face_info
        face_idx += 1
        if faces_json == 'error':
           image_data['error'] = faces['error']
        else:
            image_data['face' + str(face_idx)] = faces_json
    out_file_path = image_path + '.json'

    root_json = {}
    root_json['info'] = image_data

    with open(out_file_path, 'w') as outfile:
        json.dump(root_json, outfile)

    return


def write_to_log_file(log_filename, image):
    with tempfile.NamedTemporaryFile(
            'w', dir=os.path.dirname(log_filename), delete=False) as tf:
        tf.write(image)
        tempname = tf.name
    shutil.move(tempname, log_filename)


def predict_on_images_timed(dir_path, images_per_minute=20, log_file_path=None):
    if not os.path.exists(dir_path):
        print('Error: input dir ', dir_path, ' does not exist')
        exit(-2)

    # number of seconds to be suspended
    sleep_time = 60 / images_per_minute + 1
    if log_file_path is None:
        log_file_path = os.path.join(dir_path, 'log.txt')

    files = [ f for f in glob.glob(pathname=dir_path+"**/*.jpg",recursive=True)]
    print("-------------->found:",len(files))
    files = sorted(files)

    start_index = 0
    # read the last entry from the log_path
    if os.path.exists(log_file_path):
        with open(log_file_path, 'r') as log_file:
            last_entry = log_file.readline()
            last_entry = last_entry.strip()
            # print(files)
            start_index = files.index(last_entry) if last_entry in files else 0
            print('Log file exists: start from index ', start_index)
    c = 0
    for idx in range(start_index, len(files)):
        file = files[idx]
        if file.endswith('.png') or file.endswith('.jpg') or file.endswith('.bmp') or file.endswith(
                'jpeg') or file.endswith('.tiff'):
            print(c,'Run prediction on image: ', file)
            c+=1
            image_path = os.path.join(dir_path, file)
            predict_image(image_path)
            write_to_log_file(log_file_path, file)
            sleep(sleep_time)

    return


def parse_command_line_args(args):
    if (len(args) < 3):
        print(
            'Usage microsoft_face_api_annotation.py [input_dir] [app_key]. Rquired arguments \n\t[input dir] - path where the images are saved'
            ' \n\t[app key] - widnows subscription key for the FaceAPI')
        exit(-1)

    input_dir = args[1]
    api_key = args[2]

    return input_dir, api_key


if __name__ == '__main__':
    args = sys.argv
    # input_dir, api_key = parse_command_line_args(args)
    # running 24 ... 29
    # running 29 ... 34
    # g_subscription_key = api_key
    imdb_idx = 24
    root_dir = '/work/image-databases/imdb_crop/'

    for dir_idx in range(imdb_idx,imdb_idx+5):
        input_dir = root_dir + str(dir_idx)+"/"
        total += 1
        print('Input dir: ', input_dir, '; subscription key: ', g_subscription_key)
        predict_on_images_timed(input_dir)
