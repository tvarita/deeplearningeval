import numpy as np
from sklearn.metrics import classification_report, precision_recall_fscore_support

from annotation.automatic_annotation.parse_json import parse_dictionary
from metrics.calculate_accuracy import calculate_accuracy


def get_truth_color(file_path):
    split  =file_path.split('/')
    return split[-2]

color_mapper = {'gray':0, 'red':1, 'brown':2, 'blond':3, 'black':4,'bald':5}

if __name__ == "__main__":
    pred_colors = set()
    truth_colors = set()
    y_true = []
    y_pred = []
    total_imgs = 0
    input_dir = "/work/hair/realBald-dataset/bald/"
    age_list, gender_dict, hair_dict = parse_dictionary(input_dir)
    correct_pred = 0
    for pred_color in hair_dict.keys():
        print("---------------",pred_color)
        files = hair_dict[pred_color]
        total_imgs += len(files)
        for file_path in files:
            truth_color = get_truth_color(file_path)
            y_true.append(color_mapper[truth_color])
            y_pred.append(color_mapper[pred_color])
            if truth_color == pred_color:
                correct_pred +=1

            truth_colors.add(truth_color)
            pred_colors.add(pred_color)
    print("total",total_imgs)
    print("correct_pred",correct_pred)
    print(color_mapper)
    c_report = classification_report(y_true, y_pred)
    print(c_report)

    # matrix = tf.confusion_matrix(y_true, y_pred)
    # print(matrix)
    # print(sess.run(matrix))

    print("---> Testing_accuracie: {:g}".format(calculate_accuracy(np.asarray(y_true), np.asarray(y_pred))))
    precision, recall, fbeta_score, support = precision_recall_fscore_support(y_true, y_pred, average="macro")

    print("precision", precision)
    print("recall", recall)
    print("fbeta_score", fbeta_score)
    print("support", support)