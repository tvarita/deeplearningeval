import glob
import json
import os
import shutil
import tempfile
from shutil import copyfile

import numpy as np


def make_sure_dir_exists(dir):
    if not os.path.isdir(dir):
        os.mkdir(dir)


def write_to_log_file(log_filename, image):
    with tempfile.NamedTemporaryFile(
            'w', dir=os.path.dirname(log_filename), delete=False) as tf:
        tf.write(image)
        tempname = tf.name
    shutil.move(tempname, log_filename)


def parse_dictionary(input_dir):
    gender_err = 0
    age_err = 0
    hair_color_err = 0
    json_files = [file for file in glob.glob(input_dir + "**/*.json", recursive=True)]
    json_files = sorted(json_files)
    log_file_path = os.path.join(input_dir, 'log-parse.txt')
    start_index = 0
    # if os.path.exists(log_file_path):
    #     with open(log_file_path, 'r') as log_file:
    #         last_entry = log_file.readline()
    #         last_entry = last_entry.strip()
    #         start_index = json_files.index(last_entry) if last_entry in json_files else 0
    print("start parsing from:", start_index)
    age_list = []
    gender_dict = {"female": [], "male": []}
    hair_dict = {}

    for idx in range(start_index, len(json_files)):
        j_file = json_files[idx]
        image_file_name = j_file[:-5]
        data = json.load(open(j_file))
        try:
            gender = data['info']['face1']['faceAttributes']['gender']
            gender_dict[gender].append(image_file_name)
        except Exception:
            gender_err += 1
        try:
            age = data['info']['face1']['faceAttributes']['age']
            age_list.append((image_file_name, age))
        except Exception:
            age_err += 1
        try:
            color_tags = []
            hair_baldness = data['info']['face1']['faceAttributes']['hair']['bald']
            hair_colors = data['info']['face1']['faceAttributes']['hair']['hairColor']
            confidences = np.zeros(shape=7, dtype=np.float32)
            confidences[6] = hair_baldness

            for i, d in enumerate(hair_colors):
                confidences[i] = d['confidence']
                color_tags.append(d['color'])
            color_tags.append('bald')
            max_idx = np.argmax(confidences)
            if len(color_tags) != 1:
                max_pred_color = color_tags[max_idx]
            else:
                max_pred_color = 'bald'
            if max_pred_color in hair_dict:
                hair_dict[max_pred_color].append(image_file_name)
            else:
                hair_dict[max_pred_color] = [image_file_name]
        except Exception:
            # print(data)
            hair_color_err += 1

        write_to_log_file(log_file_path, j_file)
    print("error on parsing gender", gender_err)
    print("error on parsing age", age_err)
    print("error on parsing hair color", hair_color_err)
    print("from a total of ",len(json_files))
    return age_list, gender_dict, hair_dict


def make_sure_output_classes_dir_exist(age_classes,output_dir):
    make_sure_dir_exists(os.path.join(output_dir, 'hair'))
    for hair_color in hair_dict.keys():
        make_sure_dir_exists(os.path.join(output_dir, 'hair', hair_color))
    make_sure_dir_exists(os.path.join(output_dir, 'gender'))
    for gender in gender_dict.keys():
        make_sure_dir_exists(os.path.join(output_dir, 'gender', gender))
    make_sure_dir_exists(os.path.join(output_dir, 'age'))
    for age in age_classes[:-1]:  # type:
        make_sure_dir_exists(os.path.join(output_dir, 'age', str(age)))


if __name__ == "__main__":
    rangeDirs = range(0,21)
    input_dirs = []
    for i in rangeDirs:
        print(i)
        nr = ""
        if 0 <= i <= 9:
            nr = "0"+str(i)
        else:
            nr = str(i)
        input_dirs.append("/work/image-databases/imdb_crop/"+nr)

    # input_dirs = [
    #     "/work/image-databases/imdb_crop/00",
    #     "/work/image-databases/imdb_crop/01",
    #     "/work/image-databases/imdb_crop/02",
    #     "/work/image-databases/imdb_crop/03",
    #     "/work/image-databases/imdb_crop/04",
    #     "/work/image-databases/imdb_crop/11",
    #     "/work/image-databases/imdb_crop/12",
    #     "/work/image-databases/imdb_crop/13",
    #     "/work/image-databases/imdb_crop/14",
    #     "/work/image-databases/imdb_crop/15",
    #     "/work/image-databases/imdb_crop/16",
    #     "/work/image-databases/imdb_crop/17",
    # ]
    output_dir = '/work/image-databases/imdb_crop_classes'
    age_classes = [0, 5, 10, 20, 25, 30, 35, 40, 50, 60, 1020]
    # make_sure_output_classes_dir_exist(age_classes, output_dir)

    for input_dir in input_dirs:
    # input_dir = "D:/work/image-databases/adience/flatten_adience/"

        age_list, gender_dict, hair_dict = parse_dictionary(input_dir)
        make_sure_output_classes_dir_exist(age_classes, output_dir)

        # for hair_color in hair_dict.keys():
        #     for img_path in hair_dict[hair_color]:
        #         new_path = os.path.join(output_dir, 'hair', hair_color, os.path.basename(img_path))
        #         copyfile(img_path, new_path)

        for gender in gender_dict.keys():
            for img_path in gender_dict[gender]:
                new_path = os.path.join(output_dir, 'gender', gender, os.path.basename(img_path))
                copyfile(img_path, new_path)

        # for img_path, age in age_list:
        #     for i, age_cls in enumerate(age_classes[1:]):
        #         # here i is relatively to age_classes[1:], so we do not do i-1
        #         if age < age_cls:
        #             final_class_age = age_classes[i]
        #             new_path = os.path.join(output_dir, 'age', str(final_class_age), os.path.basename(img_path))
        #             copyfile(img_path, new_path)
