import glob

import cv2
import os

def remove_padd_img(dir):
    c = 0
    for f in glob.glob(pathname=dir + "**/*PADD*.jpg",recursive=True):
        os.remove(f)
        c+=1
    print("removed",c,"files")


def create_padd_imgs(dir_name):
    WHITE = [255, 255, 255]
    for img_path in glob.glob(dir_name + "**/*.jpg", recursive=True):
        original_img = cv2.imread(img_path)
        img_path_no_ext = "".join(img_path.split('.')[:-1])
        height, width, ch = original_img.shape
        img_padd_top = cv2.copyMakeBorder(original_img, top=int(1 / 2 * height), bottom=0, left=0, right=0,
                                          borderType=cv2.BORDER_CONSTANT, value=WHITE)
        img_padd_left = cv2.copyMakeBorder(original_img, top=0, bottom=0, left=int(1 / 2 * width), right=0,
                                           borderType=cv2.BORDER_CONSTANT, value=WHITE)
        img_padd_right = cv2.copyMakeBorder(original_img, top=0, bottom=0, left=0, right=int(1 / 2 * width),
                                            borderType=cv2.BORDER_CONSTANT, value = WHITE)
        path_top = img_path_no_ext + "_PADD_top.jpg"
        path_left = img_path_no_ext + "_PADD_left.jpg"
        path_right = img_path_no_ext + "_PADD_right.jpg"

        cv2.imwrite(path_top, img_padd_top)
        cv2.imwrite(path_left, img_padd_left)
        cv2.imwrite(path_right, img_padd_right)

        # cv2.imshow('top',img_padd_top)
        # cv2.waitKey(0)
        # cv2.imshow('top',img_padd_left)
        # cv2.waitKey(0)
        # cv2.imshow('top',img_padd_right)
        # cv2.waitKey(0)

        print(img_path_no_ext)


if __name__=="__main__":
    dir_name = "D:/work/image-databases/dataset_hair_6classes_face_cut-test/"
    # remove_padd_img(dir_name)
    create_padd_imgs(dir_name)
