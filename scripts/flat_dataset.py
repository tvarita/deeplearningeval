import glob
from shutil import copyfile
import os

if __name__ == "__main__":
    input_dir = "D:/work/image-databases/face-shape-data/"
    output_dir = "D:/work/image-databases/face-shape-data-flatten/"
    count = 0
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    for img_path in glob.glob(pathname=input_dir+"**/*.jpg",recursive=True):
        count += 1
        copyfile(img_path,os.path.join(output_dir,os.path.basename(img_path)))

    print("was copied",count,"images")
