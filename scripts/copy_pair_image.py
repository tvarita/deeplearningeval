import glob
import os
import shutil

import cv2
import numpy as np
from scipy.ndimage import imread
from scipy.misc import imresize


def add_prediction_maks(input_img, annotation_img):
    class_color_mapping = {0: (46, 255, 0), 2: (0, 0, 255)}
    h, w = annotation_img.shape
    annotation_color_img = np.zeros((h, w, 3), np.uint8)

    for label in [2]:
        annotation_color_img[annotation_img == label] = class_color_mapping[label]
        for i in range(annotation_color_img.shape[0]):
            for j in range(annotation_color_img.shape[1]):
                if annotation_img[i, j] != label:
                    annotation_color_img[i, j] = input_img[i, j]

    # display_img = cv2.addWeighted(input_img, 0.5, annotation_color_img, 0.5, 0)
    # display_img = remove_black_contour(annotation_color_img)

    return annotation_color_img

input_img_dir = "/work/hair/deeplearningeval/mobile-semantic-segmentation/data-images/Figaro1k-face/original/"
input_pair_dir = "/work/hair/deeplearningeval/mobile-semantic-segmentation/data-images/Figaro1k/GT/"
out = "/work/hair/deeplearningeval/mobile-semantic-segmentation/data-images/Figaro1k-face/masks/"
if __name__ == "__main__":
    img_paths = [p for p in glob.glob(pathname=input_img_dir + "**/*.jpg", recursive=True)]
    img__pair_paths = [p for p in glob.glob(pathname=input_pair_dir + "**/*.bmp", recursive=True)]
    for img_path in img_paths:
        for img_pair_path in img__pair_paths:
            p = os.path.basename(img_pair_path)[:-7]
            i =os.path.basename(img_path)[:-4]
            if p in i:
                # shutil.copy(img_pair_path,os.path.join(out,os.path.basename(img_pair_path)))
                mask = cv2.imread(img_pair_path, cv2.IMREAD_GRAYSCALE)
                img = imread(img_path)
                img_with_pred_scipy_face = add_prediction_maks(img, mask)
                cv2.imshow('', img_with_pred_scipy_face)
                cv2.waitKey(0)