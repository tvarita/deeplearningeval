import matplotlib.pyplot as plt
import numpy as np
from math import factorial


def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order + 1)
    half_window = (window_size - 1) // 2
    # precompute coefficients
    b = np.mat([[k ** i for i in order_range] for k in range(-half_window, half_window + 1)])
    m = np.linalg.pinv(b).A[deriv] * rate ** deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs(y[1:half_window + 1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window - 1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve(m[::-1], y, mode='valid')

def get_vals(csv_file):
    vals = []
    x_axis = []
    with open(csv_file) as f:
        lis = [line.split() for line in f]
        for i, x in enumerate(lis):
            if i > 0:
                values = x[0].split(',')
                vals.append(float(values[2]))
                x_axis.append(int(values[1]))
    return np.asarray(x_axis),np.asarray(vals)


if __name__ == "__main__":
    from matplotlib.font_manager import FontProperties

    font = FontProperties()
    font.set_family('serif')
    font.set_name('Times New Roman')
    font.set_size(16)
    # font.set_style('italic')

    csv_file_train = "D:/work/models/tfevents/train_loss.csv"
    csv_file_val = "D:/work/models/tfevents/val_loss.csv"

    x_axis, losses_train = get_vals(csv_file_train)
    _, losses_val = get_vals(csv_file_val)
    losses_val = savitzky_golay(losses_val,21,3)
    # plt.ylim(ymin=-1,ymax=-0.7)
    # plt.set
    # plt.plot(x_axis, losses_train, 'r','Training Epochs', x_axis, losses_val, 'b','Loss Value')
    # plt.show()
    fig, ax = plt.subplots()
    # fig.subplots_adjust(bottom=0.15, left=0.2)
    ax.plot(x_axis,losses_train)
    ax.plot(x_axis,losses_val)
    ax.set_xlabel('Training epochs', fontproperties=font)
    ax.set_ylabel('Loss values', fontproperties=font)

    plt.show()
