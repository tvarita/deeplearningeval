import os
import cv2
import dlib
import shutil
# !!! pip install imutils
import imutils as imutils
from imutils import face_utils


g_shape_predictor_path = '/work/hair/deeplearningeval/dlip_face_pred/shape_predictor_68_face_landmarks.dat'
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(g_shape_predictor_path)

def has_dib_face(image_path):
    image = cv2.imread(image_path)
    h, w, c = image.shape
    requested_width = 500
    scale_factor = requested_width / float(w)
    image = imutils.resize(image, requested_width)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # detect faces in the grayscale image
    rects = detector(gray, 1)

    if len(rects) == 0:
        return False
    return True

def make_image_square(img,border_type = cv2.BORDER_REPLICATE):
    BLACK = [0, 0, 0]
    height, width, channels = img.shape
    print('size:', height, width, channels)
    # Create a black image
    x = height if height > width else width
    y = height if height > width else width

    border_x = abs(x - width)
    border_y = abs(y - height)

    square = cv2.copyMakeBorder(img,  border_y//2, border_y//2, border_x//2, border_x//2, border_type,value=BLACK)
    return square

def parse_figaro(figaro_imgs_path, figaro_annos_path,
                 output_path , sz ):

    imgs = os.listdir(figaro_imgs_path)

    if not os.path.exists(output_path):
        os.mkdir(output_path)

    print(imgs)
    for img in imgs:
        img_path = os.path.join(figaro_imgs_path, img)
        anno_path = os.path.join(figaro_annos_path, img.replace('-org.jpg','-gt.pbm'))
        print(img_path)
        print(anno_path)
        i = cv2.imread(img_path)
        anno = cv2.imread(anno_path)

        if i is None or anno is None:
            continue

        if not has_dib_face(img_path):
            continue
        anno_square = make_image_square(anno,border_type=cv2.BORDER_CONSTANT)
        i_square = make_image_square(i)


        # display
        dst = cv2.addWeighted(i_square, 0.5, anno_square, 0.5, 0)

        anno_square = cv2.resize(anno_square, sz)
        i_square = cv2.resize(i_square, sz)
        cv2.imwrite(os.path.join(output_path, os.path.basename(img_path)), i_square)
        cv2.imwrite(os.path.join(output_path, os.path.basename(anno_path)), anno_square)

        # cv2.imshow("anno", anno_square)
        # cv2.imshow("img", i_square)
        # cv2.imshow("blend", dst)
        #
        # cv2.waitKey(0)
    return

def get_dlib_landmarks(image_path):
    global g_shape_predictor_path

    # load the input image, resize it, and convert it to grayscale
    image = cv2.imread(image_path)
    h, w, c = image.shape
    requested_width = 500
    scale_factor = requested_width/float(w)
    image = imutils.resize(image, requested_width)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # detect faces in the grayscale image
    rects = detector(gray, 1)

    biggest_face_area = 0
    biggest_face_lands = None
    # loop over the face detections
    for (i, rect) in enumerate(rects):
        # determine the facial landmarks for the face region, then
        # convert the facial landmark (x, y)-coordinates to a NumPy
        # array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        # convert dlib's rectangle to a OpenCV-style bounding box
        # [i.e., (x, y, w, h)], then draw the face bounding box
        (x, y, w, h) = face_utils.rect_to_bb(rect)
        area = w*h
        if biggest_face_area < area:
            biggest_face_area = area
            biggest_face_lands = shape

        # display functions
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # show the face number
        cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        # loop over the (x, y)-coordinates for the facial landmarks
        # and draw them on the image
        for (x, y) in shape:
            cv2.circle(image, (x, y), 1, (0, 0, 255), -1)

    # show the output image with the face detections + facial landmarks
    # cv2.imshow("Output", image)
    # cv2.waitKey(0)

    return biggest_face_lands

if __name__ == '__main__':
    # get_dlib_landmarks('10.jpg')
    imgs_path = '/work/segmentation_fcn/data_skin/testFigaro1k/annotations/'
    annos_path = '/work/segmentation_fcn/data_skin/testFigaro1k/images/'
    parse_figaro(imgs_path, annos_path, 'test', (224, 224))