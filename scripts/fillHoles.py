import glob
import numpy as np
import cv2


import tensorflow as tf

FLAGS = tf.flags.FLAGS
tf.flags.DEFINE_string('input_dir', "D:/work/image-databases/segmentated/dataset6/only_class/", "images dir")


def test_fill():
    for path_img in glob.glob(pathname=FLAGS.input_dir + "**/*.bmp", recursive=True):
        im_in = cv2.imread(path_img, cv2.IMREAD_GRAYSCALE)
        im_in[im_in == 0] = 255
        im_in[im_in == 2] = 0
        cv2.imshow("im_in", im_in)
        cv2.waitKey(0)
        th, im_th = cv2.threshold(im_in, 254, 255, cv2.THRESH_BINARY_INV)
        im_floodfill = im_th.copy()

        h, w = im_th.shape[:2]
        mask = np.zeros((h + 2, w + 2), np.uint8)

        cv2.floodFill(im_floodfill, mask, (0, 0), 255)
        im_floodfill_inv = cv2.bitwise_not(im_floodfill)
        im_out = im_th | im_floodfill_inv

        # cv2.imshow("im_in", im_in)
        # cv2.imshow("im_out", im_out)
        # cv2.waitKey(0)
        print()

def fillHoles(only_interest_img,interest_label,label_to_be_excluded = 1):
    original_interest_img = only_interest_img.copy()
    only_interest_img[only_interest_img != interest_label] = 0
    only_interest_img[only_interest_img == interest_label] = 255


    only_interest_img = np.asarray(only_interest_img,dtype=np.uint8)
    im_th = only_interest_img
    # th, im_th = cv2.threshold(only_interest_img, 254, 255, cv2.THRESH_BINARY_INV)
    # cv2.imshow("only interest",only_interest_img)
    im_floodfill = im_th.copy()
    h, w = im_th.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)
    # mask[:] = 255
    l = im_floodfill.shape[0]

    im_floodfill[0:2,0:l] = 0
    im_floodfill[0:l,0:2] = 0
    im_floodfill[0:l,l-2:l] = 0
    im_floodfill[l-2:l,0:l] = 0

    cv2.floodFill(im_floodfill, mask, (0, 0), 255)
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)
    im_out = im_th | im_floodfill_inv
    only_interest_img[im_out == 255] = interest_label
    only_interest_img[original_interest_img == label_to_be_excluded] = 0

    return only_interest_img


def get_only_hair_color_pixels(only_interest_img, real_img, interest_label, backgroud_label):
    only_interest_img[only_interest_img != interest_label] = 255
    only_interest_img[only_interest_img == interest_label] = 0
    only_interest_img = np.asarray(only_interest_img)
    th, im_th = cv2.threshold(only_interest_img, 254, 255, cv2.THRESH_BINARY_INV)

    im_floodfill = im_th.copy()
    h, w = im_th.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)
    cv2.floodFill(im_floodfill, mask, (0, 0), 255)
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)
    im_out = im_th | im_floodfill_inv
    real_img[im_out == 0] = [0,0,0]

    cv2.imshow("out gray", im_out)
    cv2.imshow("only hair real", real_img)
    cv2.waitKey(0)


if __name__ == "__main__":
    # for img_path in glob.glob("D:/work/image-databases/annotated-florin/blond_hair_2-results/class_seg_imgs/"+"**/*.png",recursive=True):
    #     pass
    class_img = cv2.imread("D:/work/image-databases/annotated-hair_only/annotations/0000157.bmp",
                           cv2.IMREAD_GRAYSCALE)
    real_img = cv2.imread("D:/work/image-databases/annotated-hair_only/images/0000157.jpg")
    get_only_hair_color_pixels(class_img, real_img, interest_label=2, backgroud_label=0)
