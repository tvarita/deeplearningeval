import os, argparse
import cv2
import tensorflow as tf
import numpy as np

# The original freeze_graph function
# from tensorflow.python.tools.freeze_graph import freeze_graph

dir = os.path.dirname(os.path.realpath(__file__))


def freeze_graph(model_dir, output_node_names):
    """Extract the sub graph defined by the output nodes and convert
    all its variables into constant
    Args:
        model_dir: the root folder containing the checkpoint state file
        output_node_names: a string, containing all the output node's names,
                            comma separated
    """
    if not tf.gfile.Exists(model_dir):
        raise AssertionError(
            "Export directory doesn't exists. Please specify an export "
            "directory: %s" % model_dir)

    if not output_node_names:
        print("You need to supply the name of a node to --output_node_names.")
        return -1

    # We retrieve our checkpoint fullpath
    checkpoint = tf.train.get_checkpoint_state(model_dir)
    input_checkpoint = checkpoint.model_checkpoint_path

    # We precise the file fullname of our freezed graph
    absolute_model_dir = "/".join(input_checkpoint.split('/')[:-1])
    output_graph = absolute_model_dir + "/frozen_model.pb"

    # We clear devices to allow TensorFlow to control on which device it will load operations
    clear_devices = True

    # We start a session using a temporary fresh Graph
    with tf.Session(graph=tf.Graph()) as sess:
        # We import the meta graph in the current default Graph
        saver = tf.train.import_meta_graph(input_checkpoint + '.meta', clear_devices=clear_devices)

        # We restore the weights
        saver.restore(sess, input_checkpoint)

        # [print(n.name) for n in tf.get_default_graph().as_graph_def().node]
        graph = tf.get_default_graph()
        for op in graph.get_operations():
            print(op.name)
        input_graph_def = graph.as_graph_def()
        # We use a built-in TF helper to export variables to constants
        output_graph_def = tf.graph_util.convert_variables_to_constants(
            sess,  # The session is used to retrieve the weights
            tf.get_default_graph().as_graph_def(),  # The graph_def is used to retrieve the nodes
            output_node_names.split(",")  # The output node names are used to select the usefull nodes
        )

        # Finally we serialize and dump the output graph to the filesystem
        with tf.gfile.GFile(output_graph, "wb") as f:
            f.write(output_graph_def.SerializeToString())
        print("%d ops in the final graph." % len(output_graph_def.node))

    return output_graph_def


def load_graph(frozen_graph_filename):
    # We load the protobuf file from the disk and parse it to retrieve the
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we import the graph_def into a new Graph and returns it
    with tf.Graph().as_default() as graph:
        # The name var will prefix every op/nodes in your graph
        # Since we load everything in a new graph, this is not needed
        tf.import_graph_def(graph_def)
    return graph

# saving
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_dir", type=str, default="/work/hair/hair_color_workspace/hair_color_models/model-correct-norm-lab-bins8-2048", help="Model folder to export")
    parser.add_argument("--output_node_names", type=str, default="activationOutputLayer",
                        help="The name of the output nodes, comma separated.")
    args = parser.parse_args()

    freeze_graph(args.model_dir, args.output_node_names)
# end saving


if __name__ != '__main__':
    # Let's allow the user to pass the filename as an argument
    # model_dir = "D:/work/models/row/row-age/"
    # freeze_graph(model_dir,[""])

    frozen_model_filename = 'D:/deeplearningeval/scripts/gender_frozen.pb'
    # We use our "load_graph" function
    graph = load_graph(frozen_model_filename)

    # We can verify that we can access the list of operations in the graph
    for op in graph.get_operations():
        try:
            print(op.name, ' ', graph.get_tensor_by_name(op.name+':0'))
        except:
            pass
        # prefix/Placeholder/inputs_placeholder
        # ...
        # prefix/Accuracy/predictions

    # We access the input and output nodes
    x = graph.get_tensor_by_name('prefix/input:0')
    print(x)
    y = graph.get_tensor_by_name('prefix/output:0')
    # print(graph.get_tensor_by_name('prefix/MobilenetV1/Predictions:0'))

    # We launch a Session
    with tf.Session(graph=graph) as sess:
        # Note: we don't nee to initialize/restore anything
        # There is no Variables in this graph, only hardcoded constants
        # img = cv2.imread('D:/angi-cropped/2-crop-0.jpeg')
        # img = img.astype(float)
        # img /= 255.0
        # img = img - 0.5
        # img *= 2
        # input = np.expand_dims(img, axis =0)

        # print(input.shape)
        # print(y)

        img_path = 'D:/angi-cropped/angie.jpeg'
        img_data = cv2.imread(img_path)
        img_data = cv2.cvtColor(img_data, cv2.COLOR_BGR2RGB)
        img_data = img_data.astype(np.float32)
        img_data /= 255.0
        img_data -= 0.5
        img_data *= 2.0
        img_data = np.expand_dims(img_data, axis= 0)

        # print(img_data)
        print(img_data.shape)
        if img_data is None:
            print('Error! could not load image %s', img_path)
            exit(-1)

        # img_data = tf.gfile.FastGFile(img_path, 'rb').read()
        print(img_path)
        y_out = sess.run(y, feed_dict={
            x: img_data # < 45
        })
        # I taught a neural net to recognise when a sum of numbers is bigger than 45
        # it should return False in this case
        # print(np.mean(y_out, axis = 0))  # [[ False ]] Yay, it works!
        print(y_out)
        # probabilities = tf.nn.softmax(y_out)
        # print(sess.run(probabilities))