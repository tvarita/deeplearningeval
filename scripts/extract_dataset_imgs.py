import glob

import os
import shutil

test_imgs = 50

def extract_lfw(input_imgs,input_annotations,dataset_names,out_imgs,out_annotations):
    global  test_imgs
    for in_img,in_ann in zip(input_imgs,input_annotations):
        basen = os.path.basename(in_img)
        assert basen[:-4] == os.path.basename(in_ann)[:-4]
        if basen in dataset_names:
            test_imgs -=1
            shutil.move(in_img,out_imgs)
            shutil.move(in_ann,out_annotations)
            if test_imgs == 0:
                break

if __name__ == "__main__":
    dataset_dir = '/work/segmentation_fcn/data_skin/lfw/images-lfw'

    dataset_store_dir_images = '/work/segmentation_fcn/data_skin/images/'
    dataset_store_dir_annotations = '/work/segmentation_fcn/data_skin/annotations/'

    out_root_dir = '/work/segmentation_fcn/data_skin/testFigaro1k/'

    dataset_names = [os.path.basename(file_name) for file_name in glob.glob(pathname=dataset_dir+"/**/*.jpg",recursive=True)]

    out_imgs = os.path.join(out_root_dir,"images")
    out_annotations = os.path.join(out_root_dir,"annotations")

    if not os.path.exists(out_imgs):
        os.mkdir(out_imgs)
        os.mkdir(out_annotations)

    input_imgs = [ file for file in glob.glob(dataset_store_dir_images+"**/*.jpg",recursive=True)]
    input_annotations = [ file for file in glob.glob(dataset_store_dir_annotations+"**/*.bmp",recursive=True)]

    input_imgs = sorted(input_imgs)
    input_annotations = sorted(input_annotations)

    # extract_lfw(input_imgs,input_annotations,dataset_names,out_imgs,out_annotations)
    for in_img,in_ann in zip(input_imgs,input_annotations):
        basen = os.path.basename(in_img)[:-4]
        in_ann_comp = os.path.basename(in_ann)[:-4]

        if 'Frame' in basen:
            basen = basen[:-4]
            in_ann_comp = in_ann_comp.split('-')[0]
        # if basen != in_ann_comp:
        #     print()
        # assert basen == in_ann_comp
        if 'Frame' in in_ann_comp :
            test_imgs -=1
            # os.remove(in_img)
            os.remove(in_ann)

        if 'Frame' in basen :
            test_imgs -=1
            os.remove(in_img)
            # os.remove(in_ann)
            # shutil.move(in_img,out_imgs)
            # shutil.move(in_ann,out_annotations)
            # if test_imgs == 0:
            #     break