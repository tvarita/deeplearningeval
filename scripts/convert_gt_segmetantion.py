import glob

import cv2
import os

if __name__ == "__main__":
    dataset_dir = '/work/segmentation_fcn/data_skin/testFigaro1k/annotations/'
    in_dataset = '/work/segmentation_fcn/data_skin/testFigaro1k/images/'
    ims = [ os.path.basename(f)[:-4] for f in glob.glob(in_dataset+"**/*.jpg",recursive=True)]
    annots = [img_path for img_path in glob.glob(dataset_dir+"**/*.bmp",recursive=True)]

    for img_path in annots:

        img = cv2.imread(img_path,0)
        img[img > 100] = 2
        cv2.imwrite(img_path,img)

