import os
import sys


def generate_checkpoint_file(input_dir):

    if not os.path.exists(input_dir):
        print('Error! Directory ', input_dir, ' does not exist')
        return

    orig_checkpoint_path = os.path.join(input_dir, '__checkpoint')
    checkpoint_path = os.path.join(input_dir, 'checkpoint')

    orig_exists = os.path.exists(orig_checkpoint_path)
    checkpoint_exists = os.path.exists(checkpoint_path)

    if not orig_exists and not checkpoint_exists:
        print('Error! Checkpoint file not found at ', input_dir)
        return

    if not orig_exists and checkpoint_exists:
        # generate the original original checkpoint file
        os.rename(checkpoint_path, orig_checkpoint_path)

    checkpoint_file = open(checkpoint_path, 'w')

    with open(orig_checkpoint_path, 'r') as f:

        lines = f.readlines()
        for line in lines:
            text = line.split(':')
            if len(text) < 2:
                continue

            ckpt_desc = text[0]
            ckpt_path = text[1]
            # strip the checkpoint path part of spaces, new line chars and quotes
            # TODO use regex for this !
            ckpt_path = ckpt_path.rstrip()
            ckpt_path = ckpt_path.lstrip()
            ckpt_path = ckpt_path.replace('"', '')

            ckpt_basename = os.path.basename(ckpt_path)
            new_ckpt_path = os.path.join(input_dir, ckpt_basename).replace('\\', '/')

            checkpoint_file.write('%s: "%s"\n' % (ckpt_desc, new_ckpt_path))

    checkpoint_file.close()
    print('Done. Successfully generated file %s ' % os.path.normpath(checkpoint_path))


def display_help():
    print('generate_checkpoint_file.py [input_dir]: \n'
          'input_dir - path to the directory where the checkpoint files are stored \n'
          'This script will automatically generate the "checkpoint" file with the description of'
          ' all the checkpoints from the [input_dir].'
          'The original checkpoint file is renamed to __checkpoint')

if __name__ == '__main__':
    args = sys.argv

    if len(args) < 2:
        display_help()
        exit(-1)
    input_dir = args[1]
    generate_checkpoint_file(input_dir)
