import cv2

def padd_to_make_square(img):
    WHITE = [255, 255, 255]
    h = img.shape[0]
    w = img.shape[1]
    padd_bottom = 0
    padd_top = 0
    padd_left = 0
    padd_right = 0
    max_shape = max(h,w)

    if h < max_shape:
        diff = max_shape - h
        padd_top = diff // 2
        padd_bottom = diff // 2
        if diff % 2 == 1:
            padd_top += 1
    elif w < max_shape:
        diff = max_shape - w
        padd_left = diff // 2
        padd_right = diff // 2
        if diff % 2 == 1:
            padd_right += 1
    assert h + padd_top + padd_bottom == w + padd_right + padd_left

    img = cv2.copyMakeBorder(img, top=int(padd_top), bottom=padd_bottom, left=padd_left, right=padd_right,
                                          borderType=cv2.BORDER_CONSTANT, value=WHITE)
    return img
