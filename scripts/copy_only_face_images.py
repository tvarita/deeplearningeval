import glob
import os

import cv2

from dlip_face_pred.dlib_face_landmarks_detection import get_dlib_landmarks

input_img_dir = "/work/hair/deeplearningeval/mobile-semantic-segmentation/data-images/Figaro1k/Original/Training/"
out = "/work/hair/deeplearningeval/mobile-semantic-segmentation/data-images/Figaro1k-face/"
if __name__ == "__main__":
    img_paths = [p for p in glob.glob(pathname=input_img_dir + "**/*.jpg", recursive=True)]
    for img_path in img_paths:
        img = cv2.imread(img_path)
        rects = get_dlib_landmarks(img_path)
        if rects is not None:
            cv2.imwrite(os.path.join(out,os.path.basename(img_path)),img)
        #     cv2.imshow('face',cv2.imread(img_path))
        #     cv2.waitKey(0)
        # else:
        #     cv2.imshow('', cv2.imread(img_path))
        #     cv2.waitKey(0)

