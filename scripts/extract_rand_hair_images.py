import glob
import os
from random import shuffle
import shutil

import cv2

img_number = 6826

if __name__ == "__main__":
    count = 0
    out_dir = "bald"
    input_dataset_dir = "/work/hair/6classes/"
    output_dir = "/work/hair/hair-noHair-dataset/hair"
    class_dirs = []
    for dir in os.listdir(input_dataset_dir):

            dir_abs_path = os.path.join(input_dataset_dir, dir)
            if os.path.isdir(dir_abs_path) and dir != out_dir:
                class_dirs.append(dir_abs_path)

    img_per_dir =int( img_number / len(class_dirs))
    print("will extract",img_per_dir,"images from each dir class")
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    for dir in class_dirs:
        imgs = []
        for file in glob.glob(pathname=dir+"**/*.*",recursive=True):
            imgs.append(file)
        shuffle(imgs)

        for img_path in imgs[:img_per_dir]:

            split_by_dot = img_path.split('.')
            extension = split_by_dot[-1]
            if extension != "jpg":
                path_without_extension = '.'.join(split_by_dot[:-1])
                img = cv2.imread(img_path)
                jpg_path = path_without_extension + str(".jpg")
                cv2.imwrite(jpg_path, img)
                os.remove(img_path)
                img_path = jpg_path

            name_with_class = str(os.path.basename(img_path)).replace('.jpg', '-%s.jpg' % os.path.basename(dir))
            print(name_with_class)
            shutil.copyfile(img_path,os.path.join(output_dir,name_with_class))
            count += 1
    print("was copied",count,"imgs")

