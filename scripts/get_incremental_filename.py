import os


def get_incremental_filename(pattern):
    i = 0
    while os.path.exists(pattern % i):
        i += 1
    return pattern % i