import cv2


def showWrongClassifications(pred_results, true_result, images,class_mapper):
    # class_mapper example {1:'black', 2:'blond', 3:'brown', 4:'gray', 5:'red'}
    for i in range(len(pred_results)):
        if pred_results[i] != true_result[i]:
            true_class_name = class_mapper[true_result[i]]
            pred_class_name = class_mapper[pred_results[i]]
            print("True class:",true_class_name,"Predicted class:",pred_class_name)
            cv2.imshow("True:"+true_class_name + " Pred:" + pred_class_name,images[i])
            cv2.waitKey(0)