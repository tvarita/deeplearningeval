import os
import cv2

wrong_img_count = 0
def show_wrong_classifications(dir, pred_results, true_result, images, class_mapper, image_names = None):
    global wrong_img_count
    # class_mapper example {1:'black', 2:'blond', 3:'brown', 4:'gray', 5:'red'}
    if not os.path.exists(dir):
        os.mkdir(dir)
    for i in range(len(pred_results)):
        if pred_results[i] != true_result[i]:
            true_class_name = class_mapper[true_result[i]]
            pred_class_name = class_mapper[pred_results[i]]
            cls_dir = os.path.join(dir, "is_" + true_class_name + "_pred_" + pred_class_name)
            if not os.path.exists(cls_dir):
                os.mkdir(cls_dir)
            if image_names is None:
                img_path = os.path.join(cls_dir, str(wrong_img_count) + ".jpg")
            else:
                img_path = os.path.join(cls_dir, os.path.basename(os.path.basename(image_names[i])))
            wrong_img_count += 1
            # print(img_path)
            # cv2.imshow('',images[i])
            # cv2.waitKey(0)
            cv2.imwrite(img_path, images[i])