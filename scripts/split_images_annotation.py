import glob
import shutil
import os

if __name__ == "__main__":
    input_img_dir = "/work/hair/deeplearningeval/scripts/test/"
    img_paths = [p for p in glob.glob(pathname=input_img_dir + "**/*.jpg", recursive=True)]
    mask_paths = [p for p in glob.glob(pathname=input_img_dir + "**/*.pbm", recursive=True)]
    if not os.path.exists(os.path.join(input_img_dir,"images")):
        os.mkdir(os.path.join(input_img_dir,"images"))
        os.mkdir(os.path.join(input_img_dir,"annotations"))
    c=0
    for img_path, mask_path in zip(img_paths, mask_paths):

        new_img_path = os.path.join(input_img_dir,"images",os.path.basename(img_path))
        new_mask_path = os.path.join(input_img_dir,"annotations",os.path.basename(mask_path))

        shutil.copy(img_path,os.path.join(input_img_dir,"images",os.path.basename(img_path).replace("-org",'')))
        shutil.copy(mask_path,os.path.join(input_img_dir,"annotations",os.path.basename(mask_path).replace("-gt",'')))
        c+=1
    print("copied",c,"images pairs")