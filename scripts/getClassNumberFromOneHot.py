import numpy as np


def getClassNumberFromOneHot(test_annotations):
    res = []
    for one_hot in test_annotations:
        res.append(np.argmax(one_hot))
    return res