import os
import glob
from random import shuffle


def make_sure_path_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)


# root_dir = os.path.dirname(__file__)
root_dir = "/work/hair/"
dataset_root_dir = os.path.join(root_dir, "hair-noHair")
dataset_root_dir_test = os.path.join(root_dir, "hair-noHair-test")
make_sure_path_exists(dataset_root_dir_test)
test_imgs_number_per_class = 1825

count = 0
for dir in os.listdir(dataset_root_dir):
    # if(dir == "bald"):
    class_dir = os.path.join(dataset_root_dir, dir)
    files = glob.glob(class_dir + "**/*.*")
    shuffle(files)
    class_dir_test = os.path.join(dataset_root_dir_test, dir)
    make_sure_path_exists(class_dir_test)
    for img in files[:test_imgs_number_per_class]:
        count += 1
        os.rename(img, os.path.join(class_dir_test, os.path.basename(img)))
print("Transfered from directory[", dataset_root_dir, "]", count, "files to directory [", dataset_root_dir_test, "]")
