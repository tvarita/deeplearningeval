import glob
import shutil

import cv2
import numpy as np
import os
from extract_face import add_prediction_maks


def padd_to_max_zise(img_class, img_original, max_allow_size):
    BLACK = [0]
    BLACK_RGB = [0, 0, 0]
    # cv2.imshow("img_class", img_class)
    # cv2.imshow("img_original", img_original)
    # cv2.waitKey(0)
    original_size = img_original.shape
    if original_size[0] != original_size[1]:
        print("original_size", original_size)
        height = original_size[0]
        width = original_size[1]
        add_width = max_allow_size - width
        add_height = max_allow_size - height
        top = int(add_height / 2)
        bottom = int(add_height / 2)
        left = int(add_width / 2)
        right = int(add_width / 2)
        h_diff = max_allow_size - top - bottom - height
        w_diff = max_allow_size - left - right - width
        assert h_diff == 0 or w_diff == 0
        # if h_diff != 0 or w_diff == 0:
        #     print(h_diff,w_diff)
        top += h_diff
        left += w_diff

        assert max_allow_size - top - bottom - height == 0 and max_allow_size - left - right - width == 0
        img_class_padd = cv2.copyMakeBorder(img_class, top, bottom, left, right, cv2.BORDER_CONSTANT, value=BLACK)
        img_class_original = cv2.copyMakeBorder(img_original, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                                value=BLACK_RGB)
        # print(img_class_padd.shape)
        print(img_class_original.shape)
        # cv2.imshow("img_class_padd", img_class_padd)
        # cv2.imshow("img_class_original", img_class_original)
        # cv2.waitKey(0)
        return img_class_padd, img_class_original
    else:
        return img_class, img_original

def add_prediction_maks(input_img, annotation_img):
    class_color_mapping = {0: (46, 255, 0), 2: (0, 0, 255)}
    h, w = annotation_img.shape
    annotation_color_img = np.zeros((h, w, 3), np.uint8)

    for label in [2]:
        annotation_color_img[annotation_img == label] = class_color_mapping[label]
        for i in range(annotation_color_img.shape[0]):
            for j in range(annotation_color_img.shape[1]):
                if annotation_img[i, j] != label:
                    annotation_color_img[i, j] = input_img[i, j]

    # display_img = cv2.addWeighted(input_img, 0.5, annotation_color_img, 0.5, 0)
    # display_img = remove_black_contour(annotation_color_img)

    return annotation_color_img

if __name__ == "__main__":
    input_img_dir = "/work/hair/deeplearningeval/mobile-semantic-segmentation/data-images/Figaro1k/Figaro1k/Original/Testing/"
    input_mask_dir = "/work/hair/deeplearningeval/mobile-semantic-segmentation/data-images/Figaro1k/Figaro1k/GT/Testing/"
    # input_mask_dir = "/work/segmentation_fcn/data_skin/annotations/"
    out_img_root = "/work/segmentation_fcn/data_skin/images/"
    out_mask_root = "/work/segmentation_fcn/data_skin/annotations/"
    img_paths = [p for p in glob.glob(pathname=input_img_dir + "**/*.jpg", recursive=True)]
    mask_paths = [p for p in glob.glob(pathname=input_mask_dir + "**/*.bmp", recursive=True)]
    img_paths.sort()
    mask_paths.sort()

    for img_path, mask_path in zip(img_paths, mask_paths):

        # shutil.copyfile(img_path, os.path.join(out_img_root,os.path.basename(img_path)))
        # shutil.copyfile(mask_path, os.path.join(out_mask_root,os.path.basename(mask_path)))
        mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)
        img = cv2.imread(img_path)
        img_mask = add_prediction_maks(img,mask)

        cv2.imshow('',img_mask)
        cv2.waitKey(0)
        max_allow_size = max(img.shape[0], img.shape[1])

        only_hair, img_original = padd_to_max_zise(mask, img, max_allow_size)

        if False:
            img_original = cv2.resize(img_original,(224,224))
            only_hair = cv2.resize(only_hair,(224,224))
        # img_with_pred_scipy_face = add_prediction_maks(img_original, only_hair)
        # cv2.imshow('with mask', img_with_pred_scipy_face)
        # cv2.waitKey(0)
        # cv2.imwrite(img_path, img_original)
        only_hair2 = np.zeros(shape=(224,224),dtype=np.uint8)
        only_hair2[only_hair > 155] = 2
        cv2.imwrite(mask_path, only_hair2)
