import glob
import os
import cv2

if __name__ == "__main__":
    count = 1
    dir_name = "/work/hair/test-final-egal/black2/"
    files = [file for file in glob.glob(dir_name+"**/*.png",recursive=True)]
    print("ready to convert",len(files))
    for file in files:
        print(file)
        count +=1
        split_by_dot = file.split('.')
        extension = split_by_dot[-1]
        if extension != 'jpg':
            path_without_extension = '.'.join(split_by_dot[:-1])
            if extension != "jpg":
                    img = cv2.imread(file)
                    cv2.imwrite( path_without_extension + str(".jpg"),img)
                    os.remove(file)
    print("procesed",count,"files")
