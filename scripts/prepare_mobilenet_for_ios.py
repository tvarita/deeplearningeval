import numpy as np
import tensorflow as tf

# dataset loading
from datasets import dataset_factory

# network loading
from nets import nets_factory
import nets as nets

#  preprocessing loading
from preprocessing import preprocessing_factory

slim = tf.contrib.slim

def resave_model(checkpoint_path = 'D:/work/models/row/row-gender', model_name='mobilenet_v1', dataset_name='gender', dataset_dir='D:/deeplearningeval/datasets-definitions/dataset_gender',
                 preprocessing_name = None, eval_image_size = 224, output_graph='gender_frozen.pb'):
    graph = tf.Graph()

    if preprocessing_name is None:
        preprocessing_name = model_name

    dataset_split_name = 'train'
    labels_offset = 0
    with graph.as_default():
        # load checkpoint file
        if tf.gfile.IsDirectory(checkpoint_path):
            checkpoint_path = tf.train.latest_checkpoint(checkpoint_path)

        print('Checkpoint path is ', checkpoint_path)
        # image_string = tf.placeholder(
        #     tf.string, name='input/image_name')  # Entry to the computational graph, e.g. image_string = tf.gfile.FastGFile(image_file).read()


        # image = tf.image.decode_jpeg(image_string, channels=3, try_recover_truncated=True,
        #                              acceptable_fraction=0.3)  ## To process corrupted image files

        image = tf.placeholder(tf.float32, shape=(1, eval_image_size, eval_image_size, 3),
                               name='input')

        print(image)
        # select the dataset
        dataset = dataset_factory.get_dataset(
            dataset_name, dataset_split_name, dataset_dir)

        # select the network
        network_fn = nets_factory.get_network_fn(
            model_name,
            num_classes=(dataset.num_classes - labels_offset),
            is_training=False)

        # # select the prerpocessing
        # preprocessing_name = preprocessing_name or model_name
        # image_preprocessing_fn = preprocessing_factory.get_preprocessing(
        #     preprocessing_name,
        #     is_training=False)

        print('preprocessing name ', preprocessing_name)
        # eval_image_size = eval_image_size or network_fn.default_image_size

        # processed_image = image_preprocessing_fn(image, eval_image_size, eval_image_size)

        # print('preprocessed image: ', processed_image)
        # processed_images = tf.reshape(processed_image, (1, eval_image_size, eval_image_size, 3))

        # print('preprocessed images: ', processed_images)
        # images = tf.placeholder(tf.float32, [None, 225, 224, 3])
        #  for the inception networks, load the inception arg scope
        arg_scope = None
        if 'inception' in model_name:
            if model_name == 'inception_resnet_v2':
                arg_scope = nets.inception_resnet_v2.inception_resnet_v2_arg_scope()
            if model_name == 'inception_v4':
                arg_scope = nets.inception_v4.inception_v4_arg_scope()
        if model_name == 'mobilenet_v1':
            arg_scope = nets.mobilenet_v1.mobilenet_v1_arg_scope()

        print("*************** arg scope is ", arg_scope)
        if arg_scope is not None:
            with slim.arg_scope(arg_scope):
                conv_net = nets_factory.networks_map[model_name](image, dataset.num_classes)
        else:
            conv_net = nets_factory.networks_map[model_name](image, dataset.num_classes)


        variables_to_restore = slim.get_variables_to_restore()
        # for var in variables_to_restore:
            # print(var)

        # variables_to_restore = slim-eval.get_variables_to_restore(exclude=['vgg_19/fc8'])  #
        init_fn = slim.assign_from_checkpoint_fn(checkpoint_path, variables_to_restore)
        # print(init_fn)
        sess = tf.Session(graph=graph)
        # sess = tf.Session()
        init_fn(sess)

        logits, _ = network_fn(image)
        probabilities = tf.nn.softmax(logits, name='output')

        nodes = [n for n in tf.get_default_graph().as_graph_def().node]
        for n in nodes:
            print(n)
        output_node_names='output'
        # We use a built-in TF helper to export variables to constants
        output_graph_def = tf.graph_util.convert_variables_to_constants(
            sess,  # The session is used to retrieve the weights
            tf.get_default_graph().as_graph_def(),  # The graph_def is used to retrieve the nodes
            output_node_names.split(",")  # The output node names are used to select the usefull nodes
        )

        # Finally we serialize and dump the output graph to the filesystem
        with tf.gfile.GFile(output_graph, "wb") as f:
            f.write(output_graph_def.SerializeToString())
        print("%d ops in the final graph." % len(output_graph_def.node))


if __name__ == '__main__':
    resave_model()

