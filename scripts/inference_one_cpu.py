import glob

import tensorflow as tf
import cv2
import numpy as np

import os
import time

pb_model_path = "D:/work/models/mobile-gender-age-one_out/frozen_model.pb"
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

def load_graph(frozen_graph_filename):
    # We load the protobuf file from the disk and parse it to retrieve the
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we import the graph_def into a new Graph and returns it
    with tf.Graph().as_default() as graph:
        # The name var will prefix every op/nodes in your graph
        # Since we load everything in a new graph, this is not needed
        tf.import_graph_def(graph_def, name="prefix")
    return graph


if __name__ == '__main__':
    start_total_time = time.time()
    graph = load_graph(pb_model_path)
    x = graph.get_tensor_by_name('prefix/input:0')
    # y_age = graph.get_tensor_by_name('prefix/age/output:0')
    # y_gender = graph.get_tensor_by_name('prefix/gender/output:0')
    y = graph.get_tensor_by_name('prefix/gender/output_concat:0')


    imgs_path =[ img_path for img_path in glob.glob("C:/Users/Tudor/Documents/slim-eval/movidius/images_test_angie/**/*.jpeg" ,recursive=True)]
        # img_path = "C:/Users/Tudor/Documents/slim-eval/one_file_eval/test-imgs/1007187.jpg"
    imgs = np.empty(shape=(len(imgs_path),224,224,3))
    with tf.Session(graph=graph) as sess:
        for idx,img_path in enumerate(imgs_path):
            img = cv2.imread(img_path)
            img = np.resize(img,(224,224,3))
            # imgs[idx] = img
            start_time = time.time()
            # y_out = sess.run([y_age,y_gender], feed_dict={x:[img]})
            y_out = sess.run(y, feed_dict={x:[img]})

            print(img_path)
            # print(np.argmax(y_out[0]))
            # print(np.argmax(y_out[1]))
            print(y_out)

        # print("--- %s inference time ---" % (time.time() - start_time))
        # print("--- %s total time ---" % (time.time() - start_total_time))
        # print(y_out)