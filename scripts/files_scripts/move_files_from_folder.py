import glob
from shutil import copyfile
import os

file_extension = "jpg"
dir = "/work/image-databases/imdb_crop/00/"
out_dir = "/work/image-databases/imdb_crop/00-copy"

if not os.path.exists(out_dir):
    os.mkdir(out_dir)

limit = 100
for idx,file in enumerate(glob.glob(pathname=dir+"**/*."+file_extension,recursive=True)):
    print(os.path.join(out_dir,os.path.basename(file)))
    copyfile(file,os.path.join(out_dir,os.path.basename(file)))
    if idx == limit:
        break