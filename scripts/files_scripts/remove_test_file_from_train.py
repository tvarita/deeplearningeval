import glob

import os

if __name__ == "__main__":
    test_dir = "/work/hair/realBald-dataset/bald/"
    train_dir = "/work/hair/hair-noHair/bald/"
    test_files = [os.path.basename(f) for f in glob.glob(test_dir+"**/*.jpg",recursive=True)]
    train_files = [f for f in glob.glob(train_dir+"**/*.jpg",recursive=True)]
    print("test",len(test_files))
    print("train",len(train_files))
    c=0
    for f in train_files:
        b = os.path.basename(f)
        if b in test_files:
            os.remove(f)
            c+=1


    print(c)