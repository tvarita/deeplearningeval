import glob
import os

if __name__ == "__main__":
    all_full_img_dir = "/work/hair/6classes/"
    input_dir = "/work/hair/only_hair_5classes_test/"
    all_test = [os.path.basename(f)[:-4] for f in glob.glob(input_dir+"**/*.bmp",recursive=True)]
    print(len(all_test))
    c = 0
    for dir in os.listdir(all_full_img_dir):
        all_paths = [f for f in glob.glob(all_full_img_dir+dir+"/"+"**/*.*",recursive=True)]
        for f in all_paths:
            if os.path.basename(f)[:-4] in all_test:
                c+=1
    print(c)


