import glob

import tensorflow as tf
import cv2
import numpy as np

import os
import time

pb_model_path = "/home/tudor/Desktop/workspace/gender-age/2-outs/frozen_model-age-gender.pb"
img_size = 224
labels_age = {}
labels_gender = {}
labels_age_path = "/home/tudor/Desktop/workspace/gender-age/age/labels-age.txt"
labels_gender_path = "/home/tudor/Desktop/workspace/gender-age/gender/labels-gender.txt"

with open(labels_age_path) as f:
    content = f.readlines()
    for line in content:
        class_nr = int(line.split(':')[0])
        class_description = int(line.split(':')[1])
        labels_age[class_nr] = class_description

with open(labels_gender_path) as f:
    content = f.readlines()
    for line in content:
        class_nr = int(line.split(':')[0])
        class_description = line.split(':')[1]
        labels_gender[class_nr] = class_description
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

def load_graph(frozen_graph_filename):
    # We load the protobuf file from the disk and parse it to retrieve the
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we import the graph_def into a new Graph and returns it
    with tf.Graph().as_default() as graph:
        # The name var will prefix every op/nodes in your graph
        # Since we load everything in a new graph, this is not needed
        tf.import_graph_def(graph_def, name="prefix")
    return graph


if __name__ == '__main__':
    start_total_time = time.time()
    graph = load_graph(pb_model_path)

    graph2 = tf.get_default_graph()
    input_graph_def = graph2.as_graph_def()
    # for op in graph.get_operations():
    #     try:
    #         print(op.name, ' ', graph.get_tensor_by_name(op.name + ':0'))
    #     except:
    #         pass

    x = graph.get_tensor_by_name('prefix/input:0')
    print(x)
    y_age = graph.get_tensor_by_name('prefix/age/output:0')
    print(y_age)
    y_gender = graph.get_tensor_by_name('prefix/gender/output:0')
    print(y_gender)
    # y = graph.get_tensor_by_name('MobilenetV1/Predictions/Softmax')

    imgs_path = [img_path for img_path in
                 glob.glob("../movidius/poze/**/*.jpg", recursive=True)]
    # imgs = np.empty(shape=(len(imgs_path),224,224,3))

    with tf.Session(graph=graph) as sess:
        for idx, img_path in enumerate(imgs_path):
            img_data = cv2.imread(img_path)
            # img = np.resize(img, (224, 224, 3))
            img_data = cv2.resize(img_data, (img_size, img_size))
            img_data = cv2.cvtColor(img_data, cv2.COLOR_BGR2RGB)
            img_data = img_data.astype(np.float32)
            img_data /= 255.0
            img_data -= 0.5
            img_data *= 2.0
            img_data = np.expand_dims(img_data, axis=0)

            # imgs[idx] = img
            start_time = time.time()
            # y_out = sess.run([y_age,y_gender], feed_dict={x:[img]})
            y_out = sess.run([y_age, y_gender], feed_dict={x: img_data})

            max_idx_age = np.argmax(y_out[0])
            max_idx_gender = np.argmax(y_out[1])
            label_g = labels_gender[max_idx_gender]
            label_a = labels_age[max_idx_age]
            print(img_path)
            print("gender ", label_g, "age: ", label_a)
            # print(y_out)

        # print("--- %s inference time ---" % (time.time() - start_time))
        # print("--- %s total time ---" % (time.time() - start_total_time))
        # print(y_out)
