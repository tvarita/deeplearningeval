import os
import cv2
import pickle
import numpy as np
import time
from collections import Counter
from sklearn.cluster import KMeans

minBGR = np.array([1, 1, 1])
maxBGR = np.array([256, 256, 256])


def extract_hist_from_image(image, colorspace, num_bins, hist_sizes):
    h, w, c = image.shape
    in_range_mask = np.ravel(cv2.inRange(image, minBGR, maxBGR))


    if colorspace is None or colorspace == 'rgb':
        processing_image = image
    if colorspace == 'hsv':
        processing_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    if colorspace == 'lab':
        processing_image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    if colorspace == 'ycrcb':
        processing_image = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb)


    processing_image = processing_image.flatten().reshape(h * w, 3)
    nonzero_pixels = np.asarray([processing_image[i] for i in range(processing_image.shape[0]) if in_range_mask[i] != 0],
                                dtype=np.float32)


    c1, c2, c3 = nonzero_pixels[:,  0], nonzero_pixels[:,  1], nonzero_pixels[:,  2]

    hist_c1, bins_c1 = np.histogram(c1, num_bins[0], hist_sizes[0])
    norm_hist_c1 = hist_c1 / np.linalg.norm(hist_c1, ord=0)

    hist_c2, bins_c2 = np.histogram(c2, num_bins[1], hist_sizes[1])
    norm_hist_c2 = hist_c2 / np.linalg.norm(hist_c2, ord = 0)

    hist_c3, bins_c3 = np.histogram(c3, num_bins[2], hist_sizes[2])
    norm_hist_c3 = hist_c3 / np.linalg.norm(hist_c3, ord=0)

    all_hist = np.concatenate((norm_hist_c1, norm_hist_c2, norm_hist_c3))
    # print("--- %s seconds ---" % (time.time() - start_time))
    # print(len(all_hist))
    return all_hist



def load_class_data(input_dir, colorspace = None, max_files = 100):
    all_pixels = []

    idx = 0
    file_list = os.listdir(input_dir)


    num_bins = [255, 255, 255]
    hist_sizes = [[0, 255], [0, 255], [0, 255]]

    for img_path in file_list:
        # print('Loading image # ', idx, '/', len(file_list), ' class: ', os.path.basename(input_dir))
        idx+=1
        if img_path.endswith('.bmp') or img_path.endswith('.jpg') or img_path.endswith('.jpeg') or img_path.endswith(
                '.png'):
            if idx > max_files:
                # print('max files ', 100, 'idx: ', idx, 'BREAK')
                break
            img_path = os.path.join(input_dir, img_path)
            img = cv2.imread(img_path)
            hist = extract_hist_from_image(img, colorspace, num_bins, hist_sizes)
            # print(len(hist))
            all_pixels.append(hist)
    return all_pixels


def save_classifier_data(root_dir, colorspace, out_data_file = None):
    pixels_class = {}
    idx = 0
    if out_data_file is None:
        suffix = 'rgb'
    if colorspace is not None:
        suffix = colorspace
    out_data_file = 'hist' + suffix + '.pkl'

    if not os.path.exists(out_data_file):
    # if True:
        for file in os.listdir(root_dir):
            file_path = os.path.join(root_dir, file)
        if os.path.isdir(file_path):
            pixels = load_class_data(file_path, colorspace)
        pixels_class[file] = pixels
        idx += 1
        pickle.dump(pixels_class, open(out_data_file, "wb"))
    else:
        with open(out_data_file, 'rb') as f:
            pixels_class = pickle.load(f)

    return pixels_class


def train_kmeans(data, colorspace, filepath = None, num_clusters = 7):
    print('train kmeans')
    if filepath is None:
        filepath = 'kmeanshist-' + colorspace + '.pkl'
    means = []
    x = []

    print(len(data))
    for key in data:
        # mn = np.mean(data[key], axis=0)
        x.extend(data[key])

    init_clusters = means

    if num_clusters == None:
        num_clusters = len(data)

    kmeans = KMeans(n_clusters=num_clusters, random_state=0).fit(x)

    print('clusters are: ', kmeans.cluster_centers_)
    pickle.dump(kmeans, open(filepath, "wb"))
    print('end kmeans')

    return kmeans

def test_image(img_path, colorspace, kmeans_classifier):
    img = cv2.imread(img_path)

    num_bins = [255, 255, 255]
    hist_sizes = [[0, 255], [0, 255], [0, 255]]

    hist_image = extract_hist_from_image(img, colorspace, num_bins, hist_sizes)

    indices = kmeans_classifier.predict(hist_image)
    # distances, sorted_indices = compute_cluster_distances(superpix, cluster_centers)

    cnt = Counter(indices)

    srt = cnt.most_common()
    print(cnt)
    predicted_cluster = srt[0][0]
    return predicted_cluster

if __name__ == '__main__':
    colorspace = 'hsv'
    root_dir = 'D:/work/hair_segmentation/test-images-annotated-no-holes/'
    data = save_classifier_data(root_dir, colorspace)
    kmeans = train_kmeans(data, colorspace)

    # test classifier
    idx = 0
    exec_time_sum = 0
    exec_it = 0

    clazz = 'black'

    for clazz in ['black', 'blond', 'red', 'gray', 'brown']:
        y_pred = []
        input_test_dir = 'D:/work/hair_segmentation/test-images-annotated-no-holes/'+clazz+'/'
        for img in os.listdir(input_test_dir):
            if idx < 100:
                idx += 1
                continue
            if img.endswith('bmp') or img.endswith('png'):
                img_path = os.path.join(input_test_dir, img)
                # view_votes(kmeans, os.path.join('D:/work/hair_segmentation/test-images-annotated-no-holes/blond/', img),
                #               colorspace)
                start_time = time.time()
                cluster_idx = test_image(img_path,
                                            colorspace, kmeans)
                print('Predicted cluster is ', cluster_idx)
                exec_time = (time.time() - start_time)
                exec_time_sum += exec_time
                exec_it += 1
                y_pred.append(cluster_idx)
                # print("--- exec time %s seconds ---" % exec_time)

        print('---------------- class ', clazz)
        cnt = Counter(y_pred)
        print(cnt)
        print('---------------- end class ', clazz)
        pickle.dump({'predictions': y_pred, 'clusters': kmeans.cluster_centers_},
                    open('preidctions-hist' + clazz + '.pkl', "wb"))
        print('Average execution time: ', exec_time_sum / exec_time)



