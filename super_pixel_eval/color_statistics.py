import numpy as np
import os
import cv2
import math
from sklearn.cluster import KMeans
from random import randint
from skimage.segmentation import slic
from skimage.util import img_as_float
from skimage import io
import pickle
from collections import Counter
import time
from tqdm import tqdm
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)


def convert_pixels_to_rgb(in_pix, colorspace):
    if colorspace is None:
        return in_pix

    rgb_pix = np.asarray(in_pix)
    rgb_pix = np.expand_dims(rgb_pix, axis=1)
    rgb_pix = rgb_pix.astype(np.uint8)

    if colorspace == 'lab':
        rgb_pix = cv2.cvtColor(rgb_pix, cv2.COLOR_LAB2BGR)
    if colorspace == 'hsv':
        rgb_pix = cv2.cvtColor(rgb_pix, cv2.COLOR_HSV2BGR)
    if colorspace == 'ycrcb':
        rgb_pix = cv2.cvtColor(rgb_pix, cv2.COLOR_YCrCb2BGR)

    rgb_pix = np.squeeze(rgb_pix, axis=1)
    for i in range(0, len(rgb_pix)):
        ctr = rgb_pix[i]
        rgb_pix[i] = [int(ctr[0]), int(ctr[1]), int(ctr[2])]
        tpl = tuple(rgb_pix[i])
        rgb_pix[i] = tpl

    return rgb_pix


def superpixel_SLIC_fast(image_path, colorspace=None, display=True, num_super_pix=50):
    image = io.imread(image_path)

    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, img_gray = cv2.threshold(img_gray, 1, 1, cv2.THRESH_BINARY)

    # im2, contours, hierarchy = cv2.findContours(img_gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours, hierarchy = cv2.findContours(img_gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    h, w, c = image.shape
    min_x = w
    min_y = h
    max_x = -1
    max_y = -1
    for cnt in contours:
        brect = cv2.boundingRect(cnt)
        cx, cy, cw, ch = brect

        if cx < min_x:
            min_x = cx
        if cx + cw > max_x:
            max_x = cx + cw
        if cy < min_y:
            min_y = cy
        if cy + ch > max_y:
            max_y = cy + ch

    min_x = min_x
    min_y = min_y
    max_x = max_x
    max_y = max_y
    small_img = image[min_y: max_y,
                min_x: max_x]

    # print('small img before ', small_img.shape, ' ', min_x, ' ', max_x, ' ', min_y, ' ', max_y)
    small_img = cv2.cvtColor(small_img, cv2.COLOR_RGB2BGR)
    # print('small img after ', small_img.shape)

    # print(small_img.shape)
    img_gray = img_gray[min_y: max_y,
               min_x: max_x]

    if colorspace == 'lab':
        img_colorspace = cv2.cvtColor(small_img, cv2.COLOR_BGR2Lab)
    if colorspace == 'hsv':
        img_colorspace = cv2.cvtColor(small_img, cv2.COLOR_BGR2HSV)
    if colorspace == 'ycrcb':
        img_colorspace = cv2.cvtColor(small_img, cv2.COLOR_BGR2YCrCb)

    # start_time = time.time()
    img_copy = np.copy(small_img)
    segments = slic(img_copy, sigma=3, convert2lab=True, n_segments=num_super_pix)
    num_segments = np.amax(segments)
    colors = []
    mean_colors_image = np.ones((h, w, 3), dtype=np.uint8)
    for i in range(0, num_segments):
        # mean_color = np.mean(small_img[np.where(np.logical_and(np.equal(segments,i),np.not_equal(img_gray, 0)))], axis=0)
        mean_color = np.mean(small_img[segments == i], axis=0)
        mean_color_mask = np.sum(img_gray[segments == i], axis=0)
        # mean_color = np.mean(small_img[np.where(np.logical_and(np.equal(segments,i),np.not_equal(img_gray, 0)))], axis=0)#np.mean(small_img[segments == i], axis=0)

        sz = len(small_img[segments == i])

        if sz > 0 and mean_color_mask / sz > 0.6:
            # color = [randint(0, 255), randint(0, 255), randint(0, 255)]
            if display:
                mean_colors_image[np.where(
                    segments == i)] = mean_color  # [mean_color[2] * 255, mean_color[1] * 255, mean_color[0] * 255]
                # colored_image[np.where(segments == i)] = [mean_color[2] * 255, mean_color[1] * 255, mean_color[0] * 255]
                # small_img[np.where(segments == i)] = color
            if colorspace is not None:
                mean_color_colorspace = np.mean(img_colorspace[segments == i],
                                                axis=0)  # np.mean(img_colorspace[np.where(np.logical_and(np.equal(segments,i),np.not_equal(img_gray, 0)))], axis=0)
                colors.append(mean_color_colorspace)
            else:
                colors.append([mean_color[2] * 255, mean_color[1] * 255, mean_color[0] * 255])
                mean_colors_image[np.where(segments == i)] = [mean_color[2] * 255, mean_color[1] * 255,
                                                              mean_color[0] * 255]

    if display:
        cv2.imshow('fast-segments', mean_colors_image)
        cv2.imshow('fast-small-image', small_img)
        cv2.imshow('fast-image', image)
        cv2.waitKey(0)
    return colors


# colorspace - string taht indicates the colorspace we want to convert to
# default None -> no conversion
def superpixel_SLIC(image_path, colorspace=None, display=True, num_super_pix=100):
    # load the image and convert it to a floating point data type
    image = img_as_float(io.imread(image_path))
    img_cv = cv2.imread(image_path)

    if colorspace == 'lab':
        img_colorspace = cv2.cvtColor(img_cv, cv2.COLOR_BGR2Lab)
    if colorspace == 'hsv':
        img_colorspace = cv2.cvtColor(img_cv, cv2.COLOR_BGR2HSV)
    if colorspace == 'ycrcb':
        img_colorspace = cv2.cvtColor(img_cv, cv2.COLOR_BGR2YCrCb)

    ori = img_cv
    img_gray = cv2.cvtColor(ori, cv2.COLOR_BGR2GRAY)
    _, img_gray = cv2.threshold(img_gray, 1, 1, cv2.THRESH_BINARY)
    # apply SLIC and extract (approximately) the supplied number of segments
    segments = slic(image, sigma=3, convert2lab=True, n_segments=num_super_pix)
    h, w = segments.shape
    colored_image = np.ones((h, w, 3), dtype=np.uint8)
    colors = []
    max_value = np.amax(segments)

    for i in range(0, max_value):
        # img_gray[segments == i]
        mean_color_mask = np.sum(img_gray[segments == i], axis=0)
        mean_color = np.mean(image[segments == i], axis=0)
        sz = len(image[segments == i])
        # print(sz, ' ', mean_color_mask)
        # if it is not black (is within mask)
        if mean_color_mask / sz > 0.6:
            color = [randint(0, 255), randint(0, 255), randint(0, 255)]
            colored_image[np.where(segments == i)] = [mean_color[2] * 255, mean_color[1] * 255, mean_color[0] * 255]
            img_cv[np.where(segments == i)] = color
            if colorspace is not None:
                mean_color_colorspace = np.mean(img_colorspace[segments == i], axis=0)
                colors.append(mean_color_colorspace)
            else:
                colors.append([mean_color[2] * 255, mean_color[1] * 255, mean_color[0] * 255])

    # if display:
    # cv2.imshow('segments', colored_image)
    # cv2.imshow('seg-color', img_cv)
    # cv2.imshow('original', ori)
    # cv2.waitKey(0)

    return colors


def draw_clusters(clusters, colorspace, img_w=256, img_h=256):
    img = np.full((img_h, img_w, 3), 255, dtype=np.uint8)
    num_clusters = len(clusters)

    if num_clusters <= 0:
        return img
    line_h = img_h // (num_clusters)

    idx = 0
    clusters_rgb = convert_pixels_to_rgb(clusters, colorspace)
    for cluster_color in clusters_rgb:
        # print(cluster_color)
        cluster_color = (int(cluster_color[0]),
                         int(cluster_color[1]),
                         int(cluster_color[2]))
        cv2.rectangle(img, (0, idx * line_h), (img_w, (idx + 1) * line_h), cluster_color, -1)
        idx += 1

    return img


def load_class_pixels(input_dir, colorspace=None, max_files=100):
    all_pixels = []

    idx = 0
    file_list = os.listdir(input_dir)
    for img_path in file_list:
        # print('Loading image # ', idx, '/', len(file_list), ' class: ', os.path.basename(input_dir))
        idx += 1
        if img_path.endswith('.bmp') or img_path.endswith('.jpg') or img_path.endswith('.jpeg') or img_path.endswith(
                '.png'):
            if idx > max_files:
                # print('max files ', 100, 'idx: ', idx, 'BREAK')
                break
            img_path = os.path.join(input_dir, img_path)
            superpix = superpixel_SLIC(img_path, colorspace)

            # pixels.pkl = load_image_pixels_lab(img_path)
            # if pixels.pkl is None:
            #     continue
            all_pixels.extend(superpix)
    return all_pixels


def plot_statistics(ax, min_val, max_val, mean, std_dev, color='red', ch_sz=[100, 256, 256]):
    ax.set_xlim(0, ch_sz[0])
    ax.set_ylim(0, ch_sz[2])
    ax.set_zlim(0, ch_sz[1])

    ax.scatter3D(mean[0], mean[1], mean[2], c=color, s=100)

    points = []
    for x in [min_val[0], max_val[0]]:
        for y in [min_val[1], max_val[1]]:
            for z in [min_val[2], max_val[2]]:
                points.append([x, y, z])

    points = np.asarray(points)
    ax.scatter3D(points[:, 0], points[:, 1], points[:, 2], c=color)

    return ax


def compute_statistics(pixels, class_name):
    pixels = np.asarray(pixels)

    min_val = np.amin(pixels, axis=0)
    max_val = np.amax(pixels, axis=0)

    min_val_x = np.amin(pixels[:, 0])
    min_val_y = np.amin(pixels[:, 1])
    min_val_z = np.amin(pixels[:, 2])

    print(min_val_x, ' ', min_val_y, ' ', min_val_z)
    mean = np.mean(pixels, axis=0)
    std_dev = np.std(pixels, axis=0)

    print('start ', class_name)
    print('-- min value is: ', min_val)
    print('-- max value is: ', max_val)
    print('-- mean is: ', mean)
    print('-- std dev is: ', std_dev)

    print('end ', class_name)
    return min_val, max_val, mean, std_dev


def plot_pixels(ax, pixels, color):
    pixels = np.asarray(pixels)
    ax.scatter3D(pixels[:, 0], pixels[:, 1], pixels[:, 2], c=color)
    return


def load_data(file_path):
    data = pickle.load(open(file_path, 'rb'))
    return data


def kmean_clustering(data, colorspace, filepath, init_clusters=False, num_clusters=25):
    if filepath is None:
        filepath = 'kmeans-' + colorspace + '.pkl'
    means = []
    x = []

    for key in data:
        mn = np.mean(data[key], axis=0)
        if init_clusters:
            means.append(mn)
            # print(mn)
        x.extend(data[key])

    init_clusters = means
    # initial_clusters_img = draw_clusters(init_clusters, colorspace)
    if num_clusters == None:
        num_clusters = len(data)

    kmeans = KMeans(n_clusters=num_clusters, random_state=0).fit(x)

    print('clusters count: ', len(kmeans.cluster_centers_))
    pickle.dump(kmeans, open(filepath, "wb"))
    print('end kmeans')

    return kmeans


def compute_cluster_distances(pixels, cluster_centers):
    distances = np.zeros(len(cluster_centers), dtype=np.float32)

    for pix in pixels:
        for idx in range(0, len(cluster_centers)):

            cluster = cluster_centers[idx]
            dist = 0
            for ch in range(0, 3):
                dist += (pix[ch] - cluster[ch]) * (pix[ch] - cluster[ch])

            dist = math.sqrt(dist)
            distances[idx] += dist

    sorted_indices = np.argsort(distances)
    print(distances, '->', sorted_indices)
    return distances, sorted_indices


def view_votes(kmeans, img_path, colorspace):
    # load the image and convert it to a floating point data type
    image = img_as_float(io.imread(img_path))
    img_cv = cv2.imread(img_path)

    img_colorspace = img_cv
    if colorspace == 'lab':
        img_colorspace = cv2.cvtColor(img_cv, cv2.COLOR_BGR2Lab)
    if colorspace == 'hsv':
        img_colorspace = cv2.cvtColor(img_cv, cv2.COLOR_BGR2HSV)
    if colorspace == 'ycrcb':
        img_colorspace = cv2.cvtColor(img_cv, cv2.COLOR_BGR2YCrCb)

    # apply SLIC and extract (approximately) the supplied number of segments
    segments = slic(image, sigma=3, convert2lab=True)
    h, w = segments.shape
    colored_image = np.ones((h, w, 3), dtype=np.uint8)
    predicted_image = np.ones((h, w, 3), dtype=np.uint8)
    colors = []
    max_value = np.amax(segments)
    cluster_centers = kmeans.cluster_centers_

    for i in range(0, max_value):
        mean_color = np.mean(img_colorspace[segments == i], axis=0)

        # print(mean_color)

        # if it is not black, the pixle sis within the mask
        if mean_color[0] * mean_color[1] * mean_color[2] != 0:
            # color = [randint(0, 255), randint(0, 255), randint(0, 255)]
            colored_image[
                np.where(segments == i)] = mean_color  # [mean_color[2] * 255, mean_color[1] * 255, mean_color[0] * 255]
            predicted_sample = kmeans.predict(mean_color)

            predicted_image[np.where(segments == i)] = [int(cluster_centers[predicted_sample[0]][0]),
                                                        int(cluster_centers[predicted_sample[0]][1]),
                                                        int(cluster_centers[predicted_sample[0]][2])]

            if colorspace is not None:
                mean_color_colorspace = np.mean(img_colorspace[segments == i], axis=0)
                colors.append(mean_color_colorspace)
            else:
                colors.append([mean_color[2] * 255, mean_color[1] * 255, mean_color[0] * 255])

            colored_image_display = colored_image
            predicted_image_display = predicted_image
            if colorspace is not None:
                if colorspace == 'hsv':
                    colored_image_display = cv2.cvtColor(colored_image_display, cv2.COLOR_HSV2BGR)
                    predicted_image_display = cv2.cvtColor(predicted_image_display, cv2.COLOR_HSV2BGR)

                    # cv2.imshow('original', img_cv)
                    # cv2.imshow('segments', colored_image_display)
                    # cv2.imshow('predicted', predicted_image_display)
                    # cv2.waitKey(0)


def predict_image(kmeans, img_path, colorspace, display=False, dict_prop=None, gt_class=None):
    # superpix = superpixel_SLIC(img_path, colorspace)
    superpix = superpixel_SLIC_fast(img_path, colorspace, display=False)
    cluster_centers = kmeans.cluster_centers_
    # try:
    indices = kmeans.predict(superpix)
    # except:
    #     superpixel_SLIC_fast(img_path, colorspace, display=True)
    #     a=1
    # distances, sorted_indices = compute_cluster_distances(superpix, cluster_centers)

    cnt = Counter(indices)

    srt = cnt.most_common()
    # print('most common ', srt)
    total = 0
    for v in srt:
        # print(v)
        total += v[1]

    # feature_dict cluster distributions on current image
    feature_dict = {}
    for v in srt:
        feature_dict[v[0]] = v[1] / total

    # print('dict prop:', dict_prop)
    if dict_prop is not None:

        distances = {}

        min_dist = 10000
        min_dist_class = -1

        distances_normalized = {}
        for prop in dict_prop:
            dist = 0
            # print('prop is ', prop, '->', dict_prop[prop])
            sum_dist = 0
            for i in range(0, len(cluster_centers)):
                val_train = 0
                if i in dict_prop[prop]:
                    val_train = dict_prop[prop][i]
                if i not in feature_dict:
                    feature_dict[i] = 0
                # print('compare: ', feature_dict[i], ' ', val_train)
                dist += abs(val_train - feature_dict[i])
            distances[prop] = dist

            if dist < min_dist:
                min_dist = dist
                min_dist_class = prop

    # !!!! print('predicted class', min_dist_class)

    # if min_dist_class == 'blond':
    #     print(feature_dict)
    #     if feature_dict[9] > 0.15:
    #         return 'gray'
    #
    #     # if abs(distances['blond']  - distances['gray'] ) < 0.17:
    #     #     return 'gray'
    #     # print('distances: ', distances)
    # else:
    #     print('not equal')

    # most_common_blond = [7, 0, 3, 6, 11]
    # most_common_brown = [4, 11, 5, 6, 2]
    # most_common_black = [4, 11, 5, 6, 2]
    # most_common_gray = [3, 7, 6]
    # most_common_red = [2, 8, 11]

    # if gt_class is not None and gt_class != min_dist_class:
    #     display_img = cv2.imread(img_path)
    #     cv2.putText(display_img, min_dist_class, (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (255, 0, 255))

    # cv2.circle(display_img, (20, 40), 10, rgb_centers[sorted_indices[0]], thickness=-1)
    # cv2.circle(display_img, (40, 40), 10, rgb_centers[sorted_indices[1]], -1)

    # if not os.path.exists(gt_class):
    #     os.mkdir(gt_class)
    # error_dir = os.path.join(gt_class, min_dist_class)

    # if not os.path.exists(error_dir):
    #     os.mkdir(error_dir)
    # cv2.imwrite(os.path.join(error_dir, os.path.basename(img_path)), display_img)

    # predicted_cluster = srt[0][0]
    # print('sorted is ', srt[0][0])

    if display:
        display_img = cv2.imread(img_path)
        if colorspace is not None:
            rgb_centers = np.asarray(cluster_centers)
            rgb_centers = np.expand_dims(rgb_centers, axis=1)
            rgb_centers = rgb_centers.astype(np.uint8)

            if colorspace == 'lab':
                rgb_centers = cv2.cvtColor(rgb_centers, cv2.COLOR_LAB2BGR)
            if colorspace == 'hsv':
                rgb_centers = cv2.cvtColor(rgb_centers, cv2.COLOR_HSV2BGR)
            if colorspace == 'ycrcb':
                rgb_centers = cv2.cvtColor(rgb_centers, cv2.COLOR_YCrCb2BGR)

            rgb_centers = np.squeeze(rgb_centers, axis=1)
            for i in range(0, len(rgb_centers)):
                ctr = rgb_centers[i]
                rgb_centers[i] = [int(ctr[0]), int(ctr[1]), int(ctr[2])]
                tpl = tuple(rgb_centers[i])
                rgb_centers[i] = tpl
            first_color = tuple(rgb_centers[srt[0][0]])
            second_color = tuple(rgb_centers[srt[1][0]])
        else:
            rgb_centers = cluster_centers
            first_color = tuple(rgb_centers[srt[0][0]])
            second_color = tuple(rgb_centers[srt[1][0]])

        first_color = [int(first_color[0]), int(first_color[1]), int(first_color[2])]
        second_color = [int(second_color[0]), int(second_color[1]), int(second_color[2])]

        cv2.rectangle(display_img, (0, 0), (80, 40), (0, 255, 0), -1)
        cv2.rectangle(display_img, (0, 40), (80, 80), (0, 255, 0), -1)
        cv2.circle(display_img, (20, 20), 10, color=first_color, thickness=-1)
        cv2.circle(display_img, (40, 20), 10, second_color, -1)
        cv2.putText(display_img, min_dist_class, (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (255, 0, 255))

        # cv2.circle(display_img, (20, 40), 10, rgb_centers[sorted_indices[0]], thickness=-1)
        # cv2.circle(display_img, (40, 40), 10, rgb_centers[sorted_indices[1]], -1)

        cv2.imshow('prediction', display_img)
        cv2.waitKey(0)

    return min_dist_class


def eval_classifier(data_file, input_dir, colorspace, clazz, classifier_file, class_mapping, num_clusters):
    data_dict = load_data(data_file)
    prediction_indices = [0] * 5
    correct_samples = 0
    total_samples = 0
    if not os.path.exists(classifier_file):
        kmeans = kmean_clustering(data_dict, colorspace, None, num_clusters=num_clusters)
        pickle.dump(kmeans, open(classifier_file, 'wb'))
    else:
        # print('loading kmeans from file')
        kmeans = pickle.load(open(classifier_file, 'rb'))

    dict_prop = train_on_classes(out_file_name, kmeans)

    # img_clusters = draw_clusters(kmeans.cluster_centers_, colorspace)
    # cv2.imshow('cluster_colors', img_clusters)
    # cv2.waitKey(0)

    exec_time_sum = 0
    exec_it = 0
    y_pred = []
    files = os.listdir(input_dir)
    err_count = 0
    for img in tqdm(files):

        if img.endswith('bmp') or img.endswith('png'):
            img_path = os.path.join(input_dir, img)
            # view_votes(kmeans, os.path.join('D:/work/hair_segmentation/test-images-annotated-no-holes/blond/', img), colorspace)
            start_time = time.time()
            try:
                cluster_idx = predict_image(kmeans, img_path, colorspace, False, dict_prop, clazz)
                prediction_indices[class_mapping[cluster_idx]] += 1
                if clazz in cluster_idx:
                    correct_samples += 1
                total_samples += 1
            except Exception as e:
                # print('ERROR: ', img_path, "with message", e)
                err_count += 1
                continue
            exec_time = (time.time() - start_time)
            exec_time_sum += exec_time
            exec_it += 1
            y_pred.append(cluster_idx)
            # print("--- exec time %s seconds ---" % exec_time)

    # pickle.dump({'predictions': y_pred, 'clusters': kmeans.cluster_centers_}, open('preidctions'+clazz+'.pkl', "wb"))
    # print('Average execution time: ', exec_time_sum/execf_it)
    # acc = correct_samples / total_samples
    print('\n ---  clazz: ', clazz, "|", Counter(y_pred), "err count ", err_count)
    return prediction_indices, correct_samples, total_samples


def save_classification_data(root_dir, colorspace, out_data_file=None):
    pixels_class = {}
    idx = 0
    if out_data_file is None:
        suffix = 'rgb'
        if colorspace is not None:
            suffix = colorspace
        out_data_file = 'pixels' + suffix + '.pkl'

    for file in os.listdir(root_dir):
        file_path = os.path.join(root_dir, file)
        if os.path.isdir(file_path):
            pixels = load_class_pixels(file_path, colorspace)
            pixels_class[file] = pixels
            idx += 1

    pickle.dump(pixels_class, open(out_data_file, "wb"))

    return


def train_on_classes(data_file, kmeans):
    data_dict = load_data(data_file)
    cnt_dict = {}

    for clazz in data_dict:
        colors_for_pix = data_dict[clazz]
        data = kmeans.predict(colors_for_pix)
        # print(data)
        # print(data)
        info = Counter(data).most_common()
        total = 0
        for v in info:
            total += v[1]

        # aviod division by 0
        if total == 0:
            total = 1
        dict_prop = {}
        for v in info:
            dict_prop[v[0]] = v[1] / total

        cnt_dict[clazz] = dict_prop
        # print('class: ', clazz, '->', dict_prop, ' ', total)
    return cnt_dict


if __name__ == '__main__':

    class_mapping = {"black": 0, "blonde": 1, "brown": 2, "gray": 3, "red": 4}

    for colorspace in ["hsv", "lab"]:
        print()
        print("######################## colorspace ########################", colorspace)
        print()
        for num_clusters in [5]:
            final_correct_samples = 0
            final_total_samples = 0
            print(" !!!!!! num_clusters !!!!!! ", num_clusters)
            for clazz in ['gray', 'blond', 'red', 'black', 'brown']:
                print('\nclazz is ', clazz)
                input_dir = 'Y:/hair/hair_color_workspace/only_hair_5classes_test/' + clazz + '/'  # 'C:/Users/dadi/Desktop/data-test-hair-color/'+clazz+'/'#'d:/hair/only_hair_5classes_test/'+clazz+'/'#'C:/Users/dadi/Desktop/data-test-hair-color/'+clazz+'/'#'D:/seg-samples/segmentated-5classes-sample/'+clazz+'/'#'C:/Users/dadi/Desktop/clean/blond/'#
                out_file_name = 'pixels-clean' + colorspace + '.pkl'
                classifier_file = ('kmean-%d-clusters=' % num_clusters) + colorspace + '.pkl'
                if not os.path.exists(out_file_name):
                    print('loading pixels for colorspace: ', out_file_name)
                    save_classification_data('clean/', colorspace, out_file_name)

                prediction_indices, correct_samples, total_samples = eval_classifier(out_file_name, input_dir, colorspace, clazz, classifier_file,
                                                                                     class_mapping, num_clusters=num_clusters)
                final_correct_samples += correct_samples
                final_total_samples += total_samples

                # pred_dict = pickle.load(open('preidctions'+clazz+'.pkl', 'rb'))
                # y_pred = pred_dict['predictions']
                # print('clazz:', clazz, '->', Counter(y_pred))
                # img = draw_clusters(pred_dict['clusters'], colorspace)
                # cv2.imshow('image', img)
                # cv2.waitKey(0)
            print("?? %s num_clusters: %d" % (colorspace, num_clusters))
            print("??final_total_samples space", final_total_samples)
            print("??final_correct_samples space", final_correct_samples)
            print("?? acc", final_correct_samples / final_total_samples)
