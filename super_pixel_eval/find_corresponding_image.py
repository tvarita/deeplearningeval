import os
import cv2
import shutil
import fnmatch

def recursive_glob(rootdir='.', pattern='*'):
    matches = {}
    for root, dirnames, filenames in os.walk(rootdir):
        for filename in fnmatch.filter(filenames, pattern):
            filepath = os.path.join(root, filename)
            fn, file_extension = os.path.splitext(filename)
            matches[fn] = filepath
            # print(filepath)

    return matches


def get_first_dir_name(p):
    dir_path = os.path.normpath(os.path.dirname(p))
    class_name = dir_path.split('\\')[-1]
    return class_name


def find_correspondaces(input_dir_segmentation, input_dir_original):
    segmentation_files = recursive_glob(input_dir_segmentation)
    original_files = recursive_glob(input_dir_original)

    set_segmentation = set(segmentation_files)
    set_original = set(original_files)

    pairs = {}
    for name in set_segmentation.intersection(set_original):
        pairs[name] = (original_files[name], segmentation_files[name])

    print(len(pairs))
    return pairs


def move_together(input_dir_segmentation, input_dir_original):
    pairs = find_correspondaces(input_dir_segmentation, input_dir_original)
    for p in pairs:
        try:
            img_orig_path, img_seg_path = pairs[p]
            shutil.copy(img_orig_path, os.path.dirname(img_seg_path))
        except:
            print('p is ', p)
    return


if __name__ == '__main__':
    pairs = move_together('D:/work/hair_segmentation/misclassified1/', 'D:/hair/6classes')

