from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import sys

from one_file_eval.cnn_evaluator import CnnEvaluator

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import cv2
import numpy as np
import tensorflow as tf
from tensorflow.contrib.slim import nets

from models.research.slim.datasets import dataset_factory
from models.research.slim.nets import nets_factory
from one_file_eval.preprocessing import crop_preprocessing_factory
from models.research.slim.preprocessing import preprocessing_factory

slim = tf.contrib.slim


class CnnEvaluatorMultimodel(CnnEvaluator):
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.sess1.close()
        self.fout.close()

    def __init__(self,
                 eval_batch_size,
                 crop_preprocessing_step,
                 display_result,
                 model_name,
                 checkpoint_path1,
                 checkpoint_path2,
                 preprocessing_name,
                 dataset_name1,
                 dataset_name2,
                 dataset_dir1,
                 dataset_dir2,
                 dataset_split_name,
                 eval_image_size,
                 outfile,
                 labels_offset):

        self.eval_batch_size = eval_batch_size
        self.sess1 = None
        self.image_string2 = None
        self.probabilities1 = None
        self.output_file = None
        self.display_result = display_result
        self.preprocesing_crop_name = crop_preprocessing_step
        self.output_file = outfile
        self.fout = open(self.output_file, 'a')
        # apply first preprocessing step (crop)
        self.color_labels = []
        self.labels_2_classes = ["bald", "with_hair"]
        labels_file_path = os.path.join(dataset_dir2, "labels.txt")

        with open(labels_file_path) as f:
            content = f.readlines()
            for label in content:
                self.color_labels.append(label.split('\n')[0].strip())
        if self.output_file is not None:
            if not os.path.exists(self.output_file):
                h = ['image name | ']
                h.extend(['%s | ' % i for i in self.color_labels])
                h.append('predicted_class')
                print('\t'.join(h), file=self.fout)

        graph1 = tf.Graph()
        with graph1.as_default():
            if tf.gfile.IsDirectory(checkpoint_path1):
                self.write_correct_path_checkpoint(checkpoint_path1)
                checkpoint_path1 = tf.train.latest_checkpoint(checkpoint_path1)
            print('Checkpoint1 path is ', checkpoint_path1)
            dataset1 = dataset_factory.get_dataset(dataset_name1, dataset_split_name, dataset_dir1)

            network_fn1 = nets_factory.get_network_fn(model_name, num_classes=(dataset1.num_classes - labels_offset),
                                                      is_training=False)
            self.image_string1 = tf.placeholder(
                tf.string)  # Entry to the computational graph, e.g. image_string = tf.gfile.FastGFile(image_file).read()
            image1 = tf.image.decode_jpeg(self.image_string1, channels=3, try_recover_truncated=True,
                                          acceptable_fraction=0.3)  ## To process corrupted image files
            # # select the prerpocessing
            preprocessing_name1 = preprocessing_name or model_name
            image_preprocessing_fn1 = preprocessing_factory.get_preprocessing(preprocessing_name1, is_training=False)

            eval_image_size1 = eval_image_size or network_fn1.default_image_size

            processed_image1 = image_preprocessing_fn1(image1, eval_image_size1, eval_image_size1)
            processed_images1 = tf.reshape(processed_image1,
                                           (self.eval_batch_size, eval_image_size1, eval_image_size1, 3))

            # for the inception networks, load the inception arg scope
            arg_scope = None
            if 'inception' in model_name:
                if model_name == 'inception_resnet_v2':
                    arg_scope = nets.inception_resnet_v2.inception_resnet_v2_arg_scope()
                if model_name == 'inception_v4':
                    arg_scope = nets.inception_v4.inception_v4_arg_scope()

            if arg_scope is not None:
                with slim.arg_scope(arg_scope):
                    conv_net1 = nets_factory.networks_map[model_name](processed_images1, dataset1.num_classes)
            else:
                conv_net1 = nets_factory.networks_map[model_name](processed_images1, dataset1.num_classes)

            variables_to_restore1 = slim.get_variables_to_restore()
            init_fn = slim.assign_from_checkpoint_fn(checkpoint_path1, variables_to_restore1)
            gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.47)
            self.sess1 = tf.Session(graph=graph1, config=tf.ConfigProto(gpu_options=gpu_options))

            init_fn(self.sess1)
            logits1, _ = network_fn1(processed_images1)
            self.probabilities1 = tf.nn.softmax(logits1)

        graph2 = tf.Graph()
        with graph2.as_default():

            if tf.gfile.IsDirectory(checkpoint_path2):
                self.write_correct_path_checkpoint(checkpoint_path2)
                checkpoint_path2 = tf.train.latest_checkpoint(checkpoint_path2)

            print('Checkpoint2 path is ', checkpoint_path2)

            # select the dataset

            dataset2 = dataset_factory.get_dataset(dataset_name2, dataset_split_name, dataset_dir2)
            # select the network

            network_fn2 = nets_factory.get_network_fn(model_name, num_classes=(dataset2.num_classes - labels_offset),
                                                      is_training=False)

            self.image_string2 = tf.placeholder(
                tf.string)  # Entry to the computational graph, e.g. image_string = tf.gfile.FastGFile(image_file).read()
            image2 = tf.image.decode_jpeg(self.image_string2, channels=3, try_recover_truncated=True,
                                          acceptable_fraction=0.3)  ## To process corrupted image files
            # # select the prerpocessing
            preprocessing_name2 = preprocessing_name or model_name
            image_preprocessing_fn2 = preprocessing_factory.get_preprocessing(preprocessing_name2, is_training=False)

            eval_image_size2 = eval_image_size or network_fn2.default_image_size

            processed_image2 = image_preprocessing_fn2(image2, eval_image_size2, eval_image_size2)
            processed_images2 = tf.reshape(processed_image2,
                                           (self.eval_batch_size, eval_image_size2, eval_image_size2, 3))

            # for the inception networks, load the inception arg scope
            arg_scope = None
            if 'inception' in model_name:
                if model_name == 'inception_resnet_v2':
                    arg_scope = nets.inception_resnet_v2.inception_resnet_v2_arg_scope()
                if model_name == 'inception_v4':
                    arg_scope = nets.inception_v4.inception_v4_arg_scope()

            if arg_scope is not None:
                with slim.arg_scope(arg_scope):
                    conv_net2 = nets_factory.networks_map[model_name](processed_images2, dataset2.num_classes)
            else:
                conv_net2 = nets_factory.networks_map[model_name](processed_images2, dataset2.num_classes)
            variables_to_restore2 = slim.get_variables_to_restore()
            # variables_to_restore = slim-eval.get_variables_to_restore(exclude=['vgg_19/fc8'])  #

            init_fn2 = slim.assign_from_checkpoint_fn(checkpoint_path2, variables_to_restore2)
            # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.45)
            self.sess2 = tf.Session(graph=graph2
                                    # ,config=tf.ConfigProto(gpu_options=gpu_options)
                                    )

            init_fn2(self.sess2)
            logits2, _ = network_fn2(processed_images2)

            self.probabilities2 = tf.nn.softmax(logits2)

    def eval_one_image(self, input_file_path):
        '''
        :param input_file_path:
        :return predicted class name
        :except log the error (if any)
        '''
        image_name = os.path.basename(input_file_path)
        try:
            original_image = cv2.imread(input_file_path)
            crop_preprocessing_fn = crop_preprocessing_factory.get_preprocessing(self.preprocesing_crop_name)
            cropped_image, error = crop_preprocessing_fn(original_image)
            if error:
                total_probabilities_final=[]
                probability_final=[0]
                label_final=[None]
                class_index_final=[-1]
                error_final=error
            else:
                cropped_file_path = self.get_temp_file_name(input_file_path)
                cv2.imwrite(cropped_file_path, cropped_image)

                # input_file_path = cropped_file_path
                x = tf.gfile.FastGFile(cropped_file_path, 'rb').read()
                os.remove(cropped_file_path)
                # check for baldness
                probas_has_hair = self.sess1.run(self.probabilities1, feed_dict={self.image_string1: x})
                probas_has_hair = probas_has_hair[0, 0:]

                class_has_hair_idx = np.argmax(probas_has_hair)

                label_has_hair = self.labels_2_classes[class_has_hair_idx]

                total_probabilities_final = probas_has_hair
                probability_final = [probas_has_hair[class_has_hair_idx]]
                label_final = [label_has_hair]
                class_index_final = [class_has_hair_idx]

                error_final = None
                # if hair was predicted
                if class_has_hair_idx != 0:
                    probas_hair_color = self.sess2.run(self.probabilities2, feed_dict={self.image_string2: x})
                    probas_hair_color = probas_hair_color[0, 0:]
                    total_probabilities_final = probas_hair_color
                    class_color_idx = np.argmax(probas_hair_color)
                    class_index_final = [class_color_idx]
                    label_final[0], probability_final[0] = self.color_labels[class_color_idx], probas_hair_color[
                        class_color_idx]
                    error_final = None

            if self.output_file is not None:
                a = [image_name]
                a.extend(total_probabilities_final)
                a.append(class_index_final)
                print(' | '.join([str(e) for e in a]), file=self.fout)

            print(image_name, label_final[0], "prob: ", probability_final[0],"err",error_final, file=sys.stdout)
            return label_final[0], probability_final[0], error_final

        except Exception as e:

            print(image_name, str(e), "prob: ", 0, file=sys.stdout)
            return None, 0, str(e)
