import glob

import os


class CnnEvaluator:
    def get_temp_file_name(self, file_name,to_jpg = True):
        split = file_name.split('.')
        extension = "jpg" if to_jpg else split[-1]
        file_without_extenssion = "".join(split[:-1])
        file_without_extenssion += "TEMPcnnEVAL"
        return file_without_extenssion + "." + extension

    def write_correct_path_checkpoint(self, checkpoint_path):
        checkpoint_nr = [-1]
        if os.path.isfile(checkpoint_path+"/checkpoint"):
            with open(checkpoint_path+"/checkpoint") as f:
                content = f.readlines()
                first_line = content[0]
                split_line = first_line.split('-')[-1]
                checkpoint_nr[0] = int(split_line[:-1])
        else:
            model_files = [os.path.basename(f) for f in glob.glob(pathname=checkpoint_path+"**/model-hair.ckpt-*.*",recursive=True)]
            model_number_list = []
            for f in model_files:
                if "index" in f:
                    ckp_nr = int(f.split('-')[1].split('.')[0])
                    model_number_list.append(ckp_nr)
                    break
            checkpoint_nr[0] = max(model_number_list)

        if checkpoint_nr[0] != -1:
            abs_checkpoint_path = os.path.abspath(checkpoint_path)
            with open(abs_checkpoint_path+'/checkpoint','w') as f:
                new_text = "model_checkpoint_path: \""+abs_checkpoint_path+"/model-hair.ckpt-"+str(checkpoint_nr[0])+"\""
                f.write(new_text)
            print()
        else:
            raise FileExistsError("Something went wrong with the checkpoint file."+checkpoint_path+" \nPlease make sure "
                                "you have the file \"checkpoint\" or a the model-hair files in the folder")