from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import cv2
import tensorflow as tf

from one_file_eval.utils_cnn_eval import write_text_on_image, remove_all_temp_imgs, get_images_path_from_dir, \
    code_key_dictionary
from one_file_eval.cnn_evaluator_bimodel import CnnEvaluatorMultimodel

v_dataset_dir1 = '../dataset_hair_2classes_face_cut/'
v_dataset_dir2 = '../dataset_hair_5classes_face_cut/'
v_input_dir = 'test_imgs/'
v_checkpoint_path1 = "../checkpoints-model-hair/checkpoints-2classes/"
v_checkpoint_path2 = "../checkpoints-model-hair/checkpoints-5classes/"

tf.app.flags.DEFINE_string('input_path', v_input_dir
                           # +"0index.jpeg"
                           , 'Image file path.')
tf.app.flags.DEFINE_string('outfile_file', v_input_dir + "results.txt", 'Output file for prediction probabilities.')
tf.app.flags.DEFINE_string(
    'crop_preprocessing_step',
    'face-top',
    # 'full',
    # "enlarge-all-direction",
    'The name of the preprocessing step to use befor to fed to the network')
tf.app.flags.DEFINE_string(
    'checkpoint_path1', v_checkpoint_path1,
    'The directory where the model-hair was written to or an absolute path to a checkpoint file.')
tf.app.flags.DEFINE_string(
    'checkpoint_path2', v_checkpoint_path2,
    'The directory where the model-hair was written to or an absolute path to a checkpoint file.')

tf.app.flags.DEFINE_integer(
    'num_preprocessing_threads', 4,
    'The number of threads used to create the batches.')

tf.app.flags.DEFINE_string(
    'dataset_name1', 'hair_no_hair', 'The name of the dataset to load.')
tf.app.flags.DEFINE_string(
    'dataset_name2', 'hair_5_classes', 'The name of the dataset to load.')

tf.app.flags.DEFINE_string(
    'dataset_split_name', 'test', 'The name of the train/test split.')

tf.app.flags.DEFINE_string(
    'dataset_dir1', v_dataset_dir1,
    'The directory where the dataset files are stored.')
tf.app.flags.DEFINE_string(
    'dataset_dir2', v_dataset_dir2,
    'The directory where the dataset files are stored.')

tf.app.flags.DEFINE_integer(
    'labels_offset', 0,
    'An offset for the labels in the dataset. This flag is primarily used to '
    'evaluate the VGG and ResNet architectures which do not use a background '
    'class for the ImageNet dataset.')

tf.app.flags.DEFINE_string(
    'model_name', 'vgg_19', 'The name of the architecture to evaluate.')

tf.app.flags.DEFINE_string(
    'preprocessing_name', None, 'The name of the preprocessing to use. If left '
                                'as `None`, then the model_name flag is used.')

tf.app.flags.DEFINE_integer(
    'eval_image_size', None, 'Eval image size')

tf.app.flags.DEFINE_bool(
    'display_result', True, 'The image with the class name will be displayed on screen')

FLAGS = tf.app.flags.FLAGS



def eval_images(dir_path, display_result):
    with CnnEvaluatorMultimodel(1, crop_preprocessing_step, display_result, model_name, checkpoint_path1,
                                checkpoint_path2, preprocessing_name,
                                dataset_name1, dataset_name2, dataset_dir1, dataset_dir2, dataset_split_name,
                                eval_image_size, output_file,
                                labels_offset) as evaluator:
        images_file_path = get_images_path_from_dir(dir_path)
        img_idx = 0

        predictions = []
        probabilities = []

        is_error = False
        while img_idx < len(images_file_path):
            if img_idx >= len(predictions):
                # if the current index is not already predicted
                pred_class_name, prob, error = evaluator.eval_one_image(images_file_path[img_idx])
                predictions.append(pred_class_name)
                probabilities.append(prob)

                if predictions[img_idx] is None:
                    predictions[img_idx] = error
                    probabilities[img_idx] = None
                    is_error = True

            if display_result:
                image = cv2.imread(images_file_path[img_idx])
                height, width, channels = image.shape
                if height * width > 1000 * 800 or height * width <= 400 * 300:
                    image = cv2.resize(image, (700, 900))
                write_text_on_image(image, predictions[img_idx] + " p: " + str(probabilities[img_idx]))
                cv2.imshow("Prediction", image)
                code_key = cv2.waitKeyEx(0)
                if code_key == code_key_dictionary['RightKey']:
                    img_idx += 1
                elif code_key == code_key_dictionary['LeftKey']:
                    if img_idx >= 1:
                        img_idx -= 1
            else:
                img_idx += 1
        if is_error:
            remove_all_temp_imgs(dir_path)


def eval_image(image_path: str, display_result: bool):
    with CnnEvaluatorMultimodel(1, crop_preprocessing_step, display_result, model_name, checkpoint_path1,
                                checkpoint_path2, preprocessing_name,
                                dataset_name1, dataset_name2, dataset_dir1, dataset_dir2, dataset_split_name,
                                eval_image_size, output_file,
                                labels_offset) as evaluator:
        pred_class_name, prob, error = evaluator.eval_one_image(image_path)
        if pred_class_name is None:
            pred_class_name = error
            prob = None
        if display_result:
            image = cv2.imread(image_path)
            height, width, channels = image.shape
            if height * width > 1000 * 800:
                image = cv2.resize(image, (700, 900))
            write_text_on_image(image, pred_class_name + " p: " + str(prob), draw_arrow=False)
            cv2.imshow("Prediction", image)
            cv2.waitKey(0)





if __name__ == "__main__":

    input_path = FLAGS.input_path
    output_file = FLAGS.outfile_file
    display_result = FLAGS.display_result
    model_name = FLAGS.model_name
    checkpoint_path1 = FLAGS.checkpoint_path1
    checkpoint_path2 = FLAGS.checkpoint_path2
    preprocessing_name = FLAGS.preprocessing_name
    dataset_name1 = FLAGS.dataset_name1
    dataset_name2 = FLAGS.dataset_name2
    dataset_dir1 = FLAGS.dataset_dir1
    dataset_dir2 = FLAGS.dataset_dir2
    dataset_split_name = FLAGS.dataset_split_name
    eval_image_size = FLAGS.eval_image_size
    labels_offset = FLAGS.labels_offset
    crop_preprocessing_step = FLAGS.crop_preprocessing_step

    if os.path.isdir(input_path):
        remove_all_temp_imgs(input_path)
        eval_images(input_path, display_result)
    elif os.path.isfile(input_path):
        eval_image(input_path, display_result)
