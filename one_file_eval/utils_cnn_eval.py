import glob
import cv2
import platform
import os

if platform.system() == 'Linux':
    code_key_dictionary = {
        'LeftKey': 65361,
        'RightKey': 65363,
    }
else:
    code_key_dictionary = {
        'LeftKey': 2424832,
        'RightKey': 2555904,
    }


def get_images_path_from_dir(dir_path):
    paths = []
    for ext in ('.jpg', ".JPG", ".png", ".PNG", ".jpeg", ".JPEG"):
        paths.extend([f for f in glob.glob(pathname=dir_path + "**/*" + ext, recursive=True)])
    return paths


def write_text_on_image(image, text, draw_arrow=True):
    font = cv2.FONT_HERSHEY_SIMPLEX
    text_position = (10, 30)
    fontScale = 1
    fontColor = (255, 0, 255)
    lineType = 2
    height, width, channels = image.shape
    cv2.putText(image, text,
                text_position,
                font,
                fontScale,
                fontColor,
                lineType)
    if draw_arrow:
        cv2.arrowedLine(image, (60, height - 20), (10, height - 20), 5, 3)
        cv2.arrowedLine(image, (width - 60, height - 20), (width - 10, height - 20), 5, 3)


def remove_all_temp_imgs(dir_path):
    for temp_img_path in glob.glob(pathname=dir_path + "**/*TEMPcnnEVAL.jpg", recursive=True):
        print("remove file", temp_img_path)
        os.remove(temp_img_path)
