from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import sys

from one_file_eval.cnn_evaluator import CnnEvaluator

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import cv2
import numpy as np
import tensorflow as tf
from tensorflow.contrib.slim import nets

from models.research.slim.datasets import dataset_factory
from models.research.slim.nets import nets_factory
from one_file_eval.preprocessing import crop_preprocessing_factory
from models.research.slim.preprocessing import preprocessing_factory

slim = tf.contrib.slim


class CnnEvaluatorOneModel(CnnEvaluator):

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.sess.close()
        self.fout.close()

    def __init__(self,
                 eval_batch_size,
                 crop_preprocessing_step,
                 display_result,
                 model_name,
                 checkpoint_path,
                 preprocessing_name,
                 dataset_name,
                 dataset_dir,
                 dataset_split_name,
                 eval_image_size,
                 outfile,
                 labels_offset):

        self.eval_batch_size = eval_batch_size
        self.sess = None
        self.image_string = None
        self.probabilities = None
        self.output_file = None
        self.display_result = display_result
        self.preprocesing_crop_name = crop_preprocessing_step
        self.output_file = outfile
        self.fout = open(self.output_file, 'a')
        # apply first preprocessing step (crop)
        self.labels = []
        labels_file_path = os.path.join(dataset_dir, "labels.txt")

        with open(labels_file_path) as f:
            content = f.readlines()
            for label in content:
                self.labels.append(label.split('\n')[0].strip())
        if self.output_file is not None:
            if os.path.exists(self.output_file) == False:
                h = ['image name | ']
                h.extend(['%s | ' % i for i in self.labels])
                h.append('predicted_class')
                print('\t'.join(h), file=self.fout)

        graph = tf.Graph()
        with graph.as_default():
            if tf.gfile.IsDirectory(checkpoint_path):
                self.write_correct_path_checkpoint(checkpoint_path)
                checkpoint_path = tf.train.latest_checkpoint(checkpoint_path)
            print('Checkpoint path is ', checkpoint_path)

            # select the dataset
            dataset = dataset_factory.get_dataset(dataset_name, dataset_split_name, dataset_dir)
            # select the network
            network_fn = nets_factory.get_network_fn(model_name, num_classes=(dataset.num_classes - labels_offset),
                                                     is_training=False)

            self.image_string = tf.placeholder(
                tf.string)  # Entry to the computational graph, e.g. image_string = tf.gfile.FastGFile(image_file).read()
            image = tf.image.decode_jpeg(self.image_string, channels=3, try_recover_truncated=True,
                                         acceptable_fraction=0.3)  ## To process corrupted image files
            # # select the prerpocessing
            preprocessing_name = preprocessing_name or model_name
            image_preprocessing_fn = preprocessing_factory.get_preprocessing(preprocessing_name, is_training=False)

            eval_image_size = eval_image_size or network_fn.default_image_size

            processed_image = image_preprocessing_fn(image, eval_image_size, eval_image_size)
            processed_images = tf.reshape(processed_image, (self.eval_batch_size, eval_image_size, eval_image_size, 3))

            # for the inception networks, load the inception arg scope
            arg_scope = None
            if 'inception' in model_name:
                if model_name == 'inception_resnet_v2':
                    arg_scope = nets.inception_resnet_v2.inception_resnet_v2_arg_scope()
                if model_name == 'inception_v4':
                    arg_scope = nets.inception_v4.inception_v4_arg_scope()

            if arg_scope is not None:
                with slim.arg_scope(arg_scope):
                    conv_net = nets_factory.networks_map[model_name](processed_images, dataset.num_classes)
            else:
                conv_net = nets_factory.networks_map[model_name](processed_images, dataset.num_classes)
            variables_to_restore = slim.get_variables_to_restore()
            # variables_to_restore = slim-eval.get_variables_to_restore(exclude=['vgg_19/fc8'])  #
            init_fn = slim.assign_from_checkpoint_fn(checkpoint_path, variables_to_restore)

            self.sess = tf.Session(graph=graph)
            init_fn(self.sess)
            logits, _ = network_fn(processed_images)
            self.probabilities = tf.nn.softmax(logits)

    def eval_one_image(self, input_file_path):
        '''
        :param input_file_path:
        :return predicted class name
        :except log the error (if any)
        '''
        image_name = os.path.basename(input_file_path)
        try:
            original_image = cv2.imread(input_file_path)
            crop_preprocessing_fn = crop_preprocessing_factory.get_preprocessing(self.preprocesing_crop_name)
            cropped_image,error = crop_preprocessing_fn(original_image)

            if error:
                probs_final=[]
                probability_final=0
                label_final=None
                class_index_final=-1
                error_final=error
            else:
                cropped_file_path = self.get_temp_file_name(input_file_path)
                cv2.imwrite(cropped_file_path, cropped_image)
                x = tf.gfile.FastGFile(cropped_file_path, 'rb').read()
                os.remove(cropped_file_path)


                probs = self.sess.run(self.probabilities, feed_dict={self.image_string: x})
                probs_final = probs[0, 0:]

                class_index_final = np.argmax(probs_final)
                label_final = self.labels[class_index_final]
                probability_final = probs_final[class_index_final]
                error_final = None

            if self.output_file is not None:
                a = [image_name]
                a.extend(probs_final)
                a.append(class_index_final)
                print(' | '.join([str(e) for e in a]), file=self.fout)

            print(image_name, label_final, "prob: ", probability_final,"err",error_final, file=sys.stdout)
            return label_final, probability_final, error_final

        except Exception as e:
            tf.logging.error('Error in evaluation %s' % e)
            return None, 0, str(e)



