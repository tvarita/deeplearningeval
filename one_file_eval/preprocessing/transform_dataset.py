import glob
import os

import cv2

from annotation.faceAnnotation.utils import make_sure_dir_exists
from one_file_eval.preprocessing.crop_preprocessing_factory import get_preprocessing

if __name__ == "__main__":
    root_input_dir = "/work/hair/dataset_hair_6classes/"
    root_output_dir = "/work/hair/dataset_hair_6classes-face-top/"
    make_sure_dir_exists(root_output_dir)
    for class_dir in os.listdir(root_input_dir):

        class_dir = os.path.basename(class_dir)
        print('process dir', class_dir)
        c = 0
        dir_path = os.path.join(root_input_dir, class_dir)
        out_dir = os.path.join(root_output_dir, class_dir)
        make_sure_dir_exists(out_dir)

        for img_path in glob.glob(pathname=dir_path + "**/*.jpg"):
            original_img = cv2.imread(img_path)
            prep_fn = get_preprocessing("face-top")
            cropped_image, error = prep_fn(original_img)
            if not error:
                output_file_path = os.path.join(out_dir, os.path.basename(img_path))
                cv2.imwrite(output_file_path, cropped_image)
                c += 1
        print("cropped", c, "images")
