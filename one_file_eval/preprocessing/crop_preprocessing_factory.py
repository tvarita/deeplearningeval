from one_file_eval.preprocessing import crop_top_preprocessing, full_preprocessing, enlarge_all_directions


def get_preprocessing(name,default_args=None):
    preprocessing_fn_map = {
        'face-top': crop_top_preprocessing,
        'full': full_preprocessing,
        'enlarge-all-direction': enlarge_all_directions
    }
    if name not in preprocessing_fn_map:
        raise ValueError('Preprocessing name [%s] was not recognized' % name)
    if not default_args:
        if name == 'face-top':
            default_args = (1.3, 1.9, 1, 1, 1, 1 / 2)
        elif name == 'enlarge-all-direction':
            default_args = (1.4, 1.4, 1, 1, 1, 1)
        else:
            default_args = ()

    def preprocessing_fn(image, tuple_params: tuple = default_args):
        return preprocessing_fn_map[name].preprocess_image(image, tuple_params)

    return preprocessing_fn
