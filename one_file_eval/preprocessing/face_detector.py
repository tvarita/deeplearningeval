import os
import cv2


cascade_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "haarcascades/haarcascade_frontalface_alt.xml")
face_cascade = cv2.CascadeClassifier(cascade_path)

def find_face(img):
    global face_cascade
    faces = face_cascade.detectMultiScale(
        img,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
    )
    return faces