def preprocess_image(image, crop_factors: tuple = (1.3, 1.9, 1, 1, 1, 1/2)):
    '''
        Returns the cropped image with the human face.

        Params:
            image: input image
            crop_factors: (tuple) describing how much we keep from image after we detect the face.
                -> first 2 values represent the factors with we enlarge the face image (width, height)
                    (relatively to top left corner of the face in image)
                -> last 4 values represent the factors we enlarge (or cut) from the resulting face image after first enlarge.
                    First 2 values are multiply by the top left coordinates of the face image and the next 2 values
                    are multiplied by the width and height of the image.
        Result:
            Cropped image relatively to the position of the face.
            Is not allow to enlarge over the initial image bounds. The resulting image is enlarge up to the initial bounds.
        Examples:
            image : cv::MAT
            crop_factors : (1.3, 1.9, 1, 1, 1, 1/2)
            Returns the top of the face. We enlarge the face image with 1.3 on width (face will be centered) and with 1.9
                on height. After this the half top part (1/2) is returned.
    '''
    crop_factor_face = [crop_factors[0], crop_factors[1]]
    relatively_crop_factor_face = [crop_factors[2], crop_factors[3], crop_factors[4], crop_factors[5]]

    cropped_face,enlarge_boundary = crop_face_from_img(image, crop_factor_face, relatively_crop_factor_face)
    if len(cropped_face) == 0:
        return cropped_face,"No face found"
    return cropped_face,None,enlarge_boundary


from one_file_eval.preprocessing.crop_preprocessing import crop_face_from_img
