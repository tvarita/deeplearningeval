import numpy as np

from one_file_eval.preprocessing.face_detector import find_face


def crop_face_from_img(img, crop_factor=(1.0, 1.0), relatively_crop_factor=(1, 1, 1, 2 / 4)):
    if img is None:
        return np.array([])

    faces = find_face(img)
    max_face_area = -1
    max_face = None

    # select the biggest face from the detected faces
    for (x, y, w, h) in faces:
        if w * h > max_face_area:
            max_face_area = 2 * h
            max_face = (x, y, w, h)

    if max_face_area == -1:
        return np.array([]),None

    width_diff = max_face[2] * (crop_factor[0] - 1.0)
    height_diff = max_face[3] * (crop_factor[1] - 1.0)

    enlarged_face1 = (max(max_face[0] - width_diff / 2, 0),
                      max(max_face[1] - height_diff / 2, 0),
                      max_face[2] + width_diff,
                      max_face[3] + height_diff)

    img_height, img_width, img_channels = img.shape

    enlarge_face2 = (min(enlarged_face1[0] * relatively_crop_factor[0], img_width),
                     min(enlarged_face1[1] * relatively_crop_factor[1], img_height),
                     enlarged_face1[2] * relatively_crop_factor[2],
                     enlarged_face1[3] * relatively_crop_factor[3])

    # if the enlarged/cropped face is not within the image boundaries, then we get the max possible boundaries
    enlarge_face2_in_boundary = (int(enlarge_face2[0]),
                                 int(enlarge_face2[1]),
                                 int(min(enlarge_face2[2], img_width - enlarge_face2[0])),
                                 int(min(enlarge_face2[3], img_height - enlarge_face2[1])) )

    cropped_image = img[
                    int(enlarge_face2_in_boundary[1]): int(enlarge_face2_in_boundary[1] + enlarge_face2_in_boundary[3]),
                    int(enlarge_face2_in_boundary[0]): int(enlarge_face2_in_boundary[0] + enlarge_face2_in_boundary[2])]
    return cropped_image.copy(),enlarge_face2_in_boundary
