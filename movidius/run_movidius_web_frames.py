import numpy as np
import cv2
import time
from one_file_eval.preprocessing.face_detector import find_face

from mvnc import mvncapi as mvnc


def read_label_dict(label_path, int_description=True):
    label_dict = {}
    with open(label_path) as f:
        content = f.readlines()
        for line in content:
            class_nr = int(line.split(':')[0])
            if int_description:
                class_description = int(line.split(':')[1])
            else:
                class_description = line.split(':')[1].replace('\n', '')
            label_dict[class_nr] = class_description
    return label_dict


mGraphFilename = "/home/tudor/Desktop/workspace/gender-age/out/model-hair-age-gender.graph"
labels_age_path = "/home/tudor/Desktop/workspace/gender-age/age/labels-age.txt"
labels_gender_path = "/home/tudor/Desktop/workspace/gender-age/gender/labels-gender.txt"
mLabelsAge = read_label_dict(labels_age_path)
mLabelsGender = read_label_dict(labels_gender_path, False)
mImageSize = 224
mDevicesList = mvnc.EnumerateDevices()
if len(mDevicesList) == 0:
    print('--> No movidius devices found')
mDevice = mvnc.Device(mDevicesList[0])
mDevice.OpenDevice()
with open(mGraphFilename, mode='rb') as f:
    mGraphfile = f.read()
mGraph = mDevice.AllocateGraph(mGraphfile)
c = 0

cap = cv2.VideoCapture(0)
while True:
    ret, frame = cap.read()

    img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    start_time = time.time()
    face_rects = find_face(img)

    # print("--- %s face detection ---" % (time.time() - start_time))
    if len(face_rects) > 0:
        x, y, h, w = face_rects[0]

        cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)

        face_img = img[y:y + h, x:x + w]

        face_img = cv2.resize(face_img, (mImageSize, mImageSize))
        # cv2.imshow("", face_img)
        # cv2.waitKey(0)

        face_img = face_img.astype(np.float32)

        face_img /= 255.0
        face_img -= 0.5
        face_img *= 2.0

        # cv2.imshow("",face_img)
        # cv2.waitKey(0)
        start_time = time.time()
        mGraph.LoadTensor(face_img.astype(np.float16), 'user object')
        output, userobj = mGraph.GetResult()
        # print("--- %s inference time ---" % (time.time() - start_time))
        output_age = np.asarray(output[0:10])
        output_gender = np.asarray(output[10:])
        label_gender = mLabelsGender[np.argmax(output_gender)]
        label_age = mLabelsAge[np.argmax(output_age)]
        print( c, "results: ", label_age, label_gender)
        c += 1

    # Display the resulting frame
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
