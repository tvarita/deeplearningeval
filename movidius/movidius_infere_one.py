import glob
import time
import cv2
import numpy as np

from movidius.mvnc import mvncapi  as mvnc

graph_filename = "D:/work/deeplearningeval/movidius/models/age-gender/model-age-gender.graph"
images_dir = "poze/"

labels_age_path = "D:/work/models/raw/raw-age/labels.txt"
labels_gender_path = "D:/work/models/raw/raw-gender/labels.txt"
labels_age = {}
labels_gender = {}

with open(labels_age_path) as f:
    content = f.readlines()
    for line in content:
        class_nr = int(line.split(':')[0])
        class_description = int(line.split(':')[1])
        labels_age[class_nr] = class_description

with open(labels_gender_path) as f:
    content = f.readlines()
    for line in content:
        class_nr = int(line.split(':')[0])
        class_description = line.split(':')[1]
        labels_gender[class_nr] = class_description

do_resize = True

img_size = 224

devices = mvnc.EnumerateDevices()
if len(devices) == 0:
    print('No devices found')
    quit()

device = mvnc.Device(devices[0])
device.OpenDevice()

with open(graph_filename, mode='rb') as f:
    graphfile = f.read()

graph = device.AllocateGraph(graphfile)

for image_path in glob.glob(pathname=images_dir + "**/*.*", recursive=True):
    img_data = cv2.imread(image_path)
    if do_resize:
        img_data = cv2.resize(img_data, (img_size, img_size))
    img_data = cv2.cvtColor(img_data, cv2.COLOR_BGR2RGB)
    img_data = img_data.astype(np.float32)
    img_data /= 255.0
    img_data -= 0.5
    img_data *= 2.0
    # img_data = np.expand_dims(img_data, axis=0)

    print('Start download to NCS...')
    start_time = time.time()
    print(start_time)
    graph.LoadTensor(img_data.astype(np.float16), 'user object')
    output, userobj = graph.GetResult()
    print("--- %s inference time ---" % (time.time() - start_time))
    output_age = np.asarray(output[0:10])
    output_gender = np.asarray(output[10:])
    max_idx_age = np.argmax(output_age)
    max_idx_gender = np.argmax(output_gender)
    label_g = labels_gender[max_idx_gender]
    label_a = labels_age[max_idx_age]
    print(image_path)
    print("gender ", label_g, "age: ", label_a)
    cv2.imshow("",img_data)
    cv2.waitKey(0)

graph.DeallocateGraph()
device.CloseDevice()
print('Finished')
