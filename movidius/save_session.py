#! /usr/bin/env python3

# Copyright(c) 2017 Intel Corporation. 
# License: MIT See LICENSE file in root directory. 


import sys
from movidius.mobilenet_v1 import *

IMGCLASS = "1.00"
IMGSIZE = "224"

if len(sys.argv) > 1:
    IMGCLASS = sys.argv[1]
    if len(sys.argv) > 2:
        IMGSIZE = sys.argv[2]
else:
    print("WARNING: using", IMGCLASS, " for MobileNet Class")
    print("WARNING: using", IMGSIZE, " for MobileNet image size")
    print("Run with save_session.py [IMGCLASS] [IMGSIZE] to change")
    # input("Press enter to continue")

IMGSIZE_HACK = str(int(IMGSIZE) - 1)

ROOT_AGE = "/home/tudor/Desktop/workspace/gender-age/age/"
ROOT_GENDER = "/home/tudor/Desktop/workspace/gender-age/gender/"

ROOT_OUTPUT = "/home/tudor/Desktop/workspace/gender-age/out/"

MOBILENETNAME_AGE = "model-hair.ckpt-617374"
MOBILENETNAME_GENDER = "model-hair.ckpt-335800"
num_class_out = 2
slim = tf.contrib.slim


def run(name, image_size):
    with tf.Graph().as_default():
        image = tf.placeholder("float", [1, image_size, image_size, 3], name="input")
        with tf.variable_scope('gender'):

            with slim.arg_scope(mobilenet_v1_arg_scope(is_training=False)):
                if IMGCLASS == "0.25":
                    logits, end_points = mobilenet_v1_025(image, num_class_out, is_training=False)
                elif IMGCLASS == "0.50":
                    logits, end_points = mobilenet_v1_050(image, num_class_out, is_training=False)
                elif IMGCLASS == "0.75":
                    logits, end_points = mobilenet_v1_075(image, num_class_out, is_training=False)
                else:
                    logits, end_points = mobilenet_v1(image, num_class_out, is_training=False)
            probabilities = tf.identity(end_points['Predictions'], name='output')
            print(probabilities)
            init_fn = slim.assign_from_checkpoint_fn(ROOT_GENDER +MOBILENETNAME_GENDER,
                                                     slim.get_model_variables('gender/MobilenetV1'))

            with tf.Session() as sess:
                init_fn(sess)
                saver = tf.train.Saver(tf.global_variables())
                saver.save(sess, ROOT_GENDER+"mobile-gender-saved")


run("", IMGSIZE)
