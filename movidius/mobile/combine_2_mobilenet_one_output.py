#! /usr/bin/env python3

# Copyright(c) 2017 Intel Corporation.
# License: MIT See LICENSE file in root directory.


import numpy as np
import tensorflow as tf
import sys

# from tensorflow.contrib.slim.nets.mobilenet_v1 import *
from movidius.mobile.mobilenet_v1 import mobilenet_v1_arg_scope, mobilenet_v1_025, mobilenet_v1_050, mobilenet_v1_075, \
    mobilenet_v1

IMGCLASS = "1.00"
IMGSIZE = "224"

if len(sys.argv) > 1:
    IMGCLASS = sys.argv[1]
    if len(sys.argv) > 2:
        IMGSIZE = sys.argv[2]
else:
    print("WARNING: using", IMGCLASS, " for MobileNet Class")
    print("WARNING: using", IMGSIZE, " for MobileNet image size")
    print("Run with combine_2_mobilenet.py [IMGCLASS] [IMGSIZE] to change")

ROOT_AGE = "/home/tudor/Desktop/workspace/gender-age/age/"
ROOT_GENDER = "/home/tudor/Desktop/workspace/gender-age/gender/"

ROOT_OUTPUT = "/home/tudor/Desktop/workspace/gender-age/out/"

MOBILENETNAME_AGE = "mobile-age-saved"
MOBILENETNAME_GENDER = "mobile-gender-saved"

ARG_SCOPE_AGE = 'age/MobilenetV1'
ARG_SCOPE_GENDER = 'gender/MobilenetV1'

NUM_CLASS_AGE = 10
NUM_CLASS_GENDER = 2

print("Saving Session for ", MOBILENETNAME_AGE)
print("Saving Session for ", MOBILENETNAME_GENDER)

slim = tf.contrib.slim

with tf.Graph().as_default():

    image = tf.placeholder("float", [1, IMGSIZE, IMGSIZE, 3], name="input")

    with tf.variable_scope('age'):
        with slim.arg_scope(mobilenet_v1_arg_scope(is_training=False)):
            logits, end_points = mobilenet_v1(image, NUM_CLASS_AGE, is_training=False)

        probabilities_age = tf.identity(end_points['Predictions'], name='output')

        output_age = tf.get_default_graph().get_tensor_by_name("age/output:0")
        # print(output_age)

    with tf.variable_scope('gender'):
        with slim.arg_scope(mobilenet_v1_arg_scope(is_training=False)):
            logits_g, end_points_g = mobilenet_v1(image, NUM_CLASS_GENDER, is_training=False)
        probabilities_gender = tf.identity(end_points_g['Predictions'], name='output')
        output_gender = tf.get_default_graph().get_tensor_by_name("gender/output:0")

        concat_outputs = tf.concat([output_age, output_gender], 1, name="output_concat")

        init_fn = slim.assign_from_checkpoint_fn(ROOT_AGE + MOBILENETNAME_AGE, slim.get_model_variables(ARG_SCOPE_AGE))
        init_fn_gender = slim.assign_from_checkpoint_fn(ROOT_GENDER + MOBILENETNAME_GENDER,
                                                        slim.get_model_variables(ARG_SCOPE_GENDER))



    # print(concat_outputs)
    # probabilities_age = tf.identity(end_points['output_concat:0'], name='output_c')

    with tf.Session() as sess:
        init_fn(sess)
        init_fn_gender(sess)

        saver = tf.train.Saver(tf.global_variables())
        a = [v for v in tf.all_variables()]
        saver.save(sess, ROOT_OUTPUT + "gender_age_freez_one_out")

print("done")
# run(MOBILENETNAME_HACK, IMGSIZE_HACK, 2)
