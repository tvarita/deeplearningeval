#! /usr/bin/env python3

# Copyright(c) 2017 Intel Corporation. 
# License: MIT See LICENSE file in root directory. 


import numpy as np
import tensorflow as tf
import sys

# from tensorflow.contrib.slim.nets.mobilenet_v1 import *
from movidius.mobile.mobilenet_v1 import mobilenet_v1_arg_scope, mobilenet_v1_025, mobilenet_v1_050, mobilenet_v1_075, \
    mobilenet_v1

IMGCLASS = "1.00"
IMGSIZE = "224"

if len(sys.argv) > 1:
    IMGCLASS = sys.argv[1]
    if len(sys.argv) > 2:
        IMGSIZE = sys.argv[2]
else:
    print("WARNING: using", IMGCLASS, " for MobileNet Class")
    print("WARNING: using", IMGSIZE, " for MobileNet image size")
    print("Run with combine_2_mobilenet.py [IMGCLASS] [IMGSIZE] to change")


ROOT_AGE = "D:/work/models/row/row-age/"
ROOT_GENDER = "D:/work/models/row/row-gender/"
ROOT_OUTPUT = "D:/work/models/mobile-gender-age/"

MOBILENETNAME_AGE = "model-hair.ckpt-617374"
MOBILENETNAME_GENDER = "model-hair.ckpt-335800"
ARG_SCOPE_AGE = 'age/MobilenetV1'
ARG_SCOPE_GENDER = 'gender/MobilenetV1'

NUM_CLASS_AGE = 10
NUM_CLASS_GENDER = 2

print("Saving Session for ", MOBILENETNAME_AGE)
print("Saving Session for ", MOBILENETNAME_GENDER)

slim = tf.contrib.slim

with tf.Graph().as_default():
    image = tf.placeholder("float", [1, IMGSIZE, IMGSIZE, 3], name="input")

    with tf.variable_scope('age'):
        with slim.arg_scope(mobilenet_v1_arg_scope(is_training=False)):
            # if IMGCLASS == "0.25":
            #     logits, end_points = mobilenet_v1_025(image, NUM_CLASS_AGE, is_training=False)
            # elif IMGCLASS == "0.50":
            #     logits, end_points = mobilenet_v1_050(image, NUM_CLASS_AGE, is_training=False)
            # elif IMGCLASS == "0.75":
            #     logits, end_points = mobilenet_v1_075(image, NUM_CLASS_AGE, is_training=False)
            # else:
            assert IMGCLASS == "1.00"
            logits, end_points = mobilenet_v1(image, NUM_CLASS_AGE, is_training=False)

        init_fn = slim.assign_from_checkpoint_fn(ROOT_AGE + MOBILENETNAME_AGE, slim.get_model_variables(ARG_SCOPE_AGE))
        probabilities_age = tf.identity(end_points['Predictions'], name='output')
        output_age = tf.get_default_graph().get_tensor_by_name("age/output:0")
        print(output_age)

    with tf.variable_scope('gender'):
        with slim.arg_scope(mobilenet_v1_arg_scope(is_training=False)):
            assert IMGCLASS == "1.00"
            logits_g, end_points_g = mobilenet_v1(image, NUM_CLASS_GENDER, is_training=False)
        init_fn_gender = slim.assign_from_checkpoint_fn(ROOT_GENDER + MOBILENETNAME_GENDER, slim.get_model_variables(ARG_SCOPE_GENDER))
        probabilities_gender = tf.identity(end_points_g['Predictions'], name='output')
        output_gender = tf.get_default_graph().get_tensor_by_name("gender/output:0")
        print(output_gender)

    with tf.Session() as sess:
        init_fn(sess)
        init_fn_gender(sess)

        # new_vars = []
        # for name, shape in vars:
        #     v = tf.contrib.framework.load_variable(ROOT, name)
        #     # new_vars.append(tf.Variable(v, name=name.replace('my-first-scope', 'my-second-scope')))
        #     new_vars.append(tf.Variable(v))

        saver = tf.train.Saver(tf.global_variables())
        a = [v for v in tf.all_variables()]
        saver.save(sess, ROOT_OUTPUT + "gender_age_freez")

print("done")
# run(MOBILENETNAME_HACK, IMGSIZE_HACK, 2)
