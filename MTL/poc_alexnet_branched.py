from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np

slim = tf.contrib.slim


def alex_net(images, num_classes_task1=10, num_classes_task2=10, is_training=False,
             dropout_keep_prob=0.5,
             prediction_fn=slim.softmax,
             scope='LeNet'):
    end_points = {}

    with tf.variable_scope(scope, 'LeNet', [images]):
        net = slim.conv2d(images, 64, [11, 11], 4, padding='VALID', scope='conv1')
        net = slim.max_pool2d(net, [3, 3], 2, scope='pool1')
        net = slim.conv2d(net, 192, [5, 5], scope='conv2')
        net = slim.max_pool2d(net, [3, 3], 2, scope='pool2')
        net = slim.conv2d(net, 384, [3, 3], scope='conv3')
        net = net_branched = slim.conv2d(net, 384, [3, 3], scope='conv4')
        net = slim.conv2d(net, 256, [3, 3], scope='conv5')
        net = slim.max_pool2d(net, [3, 3], 2, scope='pool5')
        net = slim.flatten(net)
        end_points['Flatten'] = net

        net_branched = slim.max_pool2d(net_branched, [3, 3], 2, scope='pool_branch5')
        net_branched = slim.flatten(net_branched)
        net_branched = slim.fully_connected(net_branched, 1024, scope='fc_branch1')
        net_branched = slim.dropout(net_branched, dropout_keep_prob, is_training=is_training, scope='dropout_branched3')

        net = end_points['fc3'] = slim.fully_connected(net, 1024, scope='fc1')
        net = end_points['dropout1'] = slim.dropout(
            net, dropout_keep_prob, is_training=is_training, scope='dropout1')

        logits_task1 = end_points['Logits'] = slim.fully_connected(
            net, num_classes_task1, activation_fn=None, scope='fc2')

        logits_task2 = end_points['Logits'] = slim.fully_connected(
            net_branched, num_classes_task2, activation_fn=None, scope='fc_branched2')

    prediction_task1 = prediction_fn(logits_task1, scope='Predictions')
    prediction_task2 = prediction_fn(logits_task2, scope='Predictions')

    return prediction_task1, prediction_task2


def train():
    img_size = 256
    batch_size = 1
    channels = 3
    num_class_age = 100
    num_class_gender = 2
    x_batch = np.empty(shape=(batch_size, img_size, img_size, channels), dtype=np.float32)

    X = tf.placeholder("float", [None, img_size, img_size, channels], name="X")
    Y1 = tf.placeholder(dtype=tf.int64, shape=[None, num_class_age], name="Y1")
    Y2 = tf.placeholder(dtype=tf.int64, shape=[None, num_class_gender], name="Y2")

    probs_task_age, probs_task_gender = alex_net(x_batch, num_class_age, num_class_gender)
    predicted_age = tf.argmax(probs_task_age, dimension=1)
    predicted_gender = tf.argmax(probs_task_gender,dimension=1)

    Y1_Loss = tf.nn.l2_loss(Y1 - predicted_age)
    Y2_Loss = tf.nn.l2_loss(Y2 - predicted_gender)
    joint_Loss = Y1_Loss + Y2_Loss

    Optimiser = tf.train.AdamOptimizer().minimize(joint_Loss)

    iterations = 1000
    with tf.Session() as session:
        session.run(tf.initialize_all_variables())
        for i in range(iterations):
            _, joint_Loss = session.run([Optimiser, joint_Loss],
                                        {
                                            X: np.random.rand(img_size, img_size) * batch_size,
                                            Y1: np.random.rand(num_class_age) * batch_size,
                                            Y2: np.random.rand(num_class_gender) * batch_size
                                        })
            print(joint_Loss)


if __name__ == "__main__":
    train()
