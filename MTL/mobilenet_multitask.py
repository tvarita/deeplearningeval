import os
import cv2
import sys
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../models')))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../database_parsing')))

import numpy as np
import tensorflow as tf
from sklearn.utils import shuffle


from models.research.slim.nets import mobilenet_v1
from models.research.slim.preprocessing import inception_preprocessing

from database_parsing import load_imdb_wiki
from database_parsing.attributes import FacialAttributes

slim = tf.contrib.slim



def to_categorical(label, num_classes):
    categorical_label = np.zeros(num_classes)
    categorical_label[label] = 1
    return categorical_label

def _reduced_kernel_size_for_small_input(input_tensor, kernel_size):
  """Define kernel size which is automatically reduced for small input.

  If the shape of the input images is unknown at graph construction time this
  function assumes that the input images are large enough.

  Args:
    input_tensor: input tensor of size [batch_size, height, width, channels].
    kernel_size: desired kernel size of length 2: [kernel_height, kernel_width]

  Returns:
    a tensor with the kernel size.
  """
  shape = input_tensor.get_shape().as_list()
  if shape[1] is None or shape[2] is None:
    kernel_size_out = kernel_size
  else:
    kernel_size_out = [min(shape[1], kernel_size[0]),
                       min(shape[2], kernel_size[1])]
  return kernel_size_out


class MobilenetMultitask():

    weights_age = 0.7
    weights_gender = 0.3

    def __init__(self, session, image_size = 224, batch_size = 64, num_iterations = 55, save_path = 'checkpoint'):
        self.batch_size = batch_size
        self.num_epochs = num_iterations
        self.session = session

        self.num_gender_classes = 2
        self.num_age_classes = 101
        self.image_size = image_size

        self.create_input()
        self.logits_gender, self.predictions_gender, \
            self.logits_age, self.predictions_age, _ = self.create_age_gender_multitask_mobilenet()

        self.save_path = save_path
        if not os.path.exists(save_path):
            os.mkdir(save_path)


        self.saver = tf.train.Saver()


    def load_model(self, checkpoint_dir_path):
        ckp_path = tf.train.latest_checkpoint(checkpoint_dir_path)
        if ckp_path is not None:
            self.saver.restore(self.session, ckp_path)
            print('restored classifier')
            return True
        print('unable to restore classifier')
        return False

    def inference(self, image_path):
        age, gender = 0, 0
        return age, gender

    def _configure_learning_rate(self, learning_rate, learning_rate_decay_type, learning_rate_decay_factor,
                                 num_epochs_per_decay, num_samples_per_epoch, end_learning_rate, global_step):

        decay_steps = int(num_samples_per_epoch / self.batch_size *
                          num_epochs_per_decay)

        if learning_rate_decay_type == 'exponential':
            return tf.train.exponential_decay(learning_rate,
                                              global_step,
                                              decay_steps,
                                              learning_rate_decay_factor,
                                              staircase=True,
                                              name='exponential_decay_learning_rate')
        elif learning_rate_decay_type == 'fixed':
            return tf.constant(learning_rate, name='fixed_learning_rate')
        elif learning_rate_decay_type == 'polynomial':
            return tf.train.polynomial_decay(learning_rate,
                                             global_step,
                                             decay_steps,
                                             end_learning_rate,
                                             power=1.0,
                                             cycle=False,
                                             name='polynomial_decay_learning_rate')
        else:
            raise ValueError('learning_rate_decay_type [%s] was not recognized',
                             learning_rate_decay_type)

    def create_input(self):
        self.image_string = tf.placeholder( tf.string)  # Entry to the computational graph, e.g. image_string = tf.gfile.FastGFile(image_file).read()

        image = tf.image.decode_jpeg(self.image_string, channels=3, try_recover_truncated=True,
                                     acceptable_fraction=0.3)  ## To process corrupted image files

        image_preprocessing_fn = inception_preprocessing.preprocess_image

        processed_image = image_preprocessing_fn(image, self.image_size, self.image_size)

        self.img_loading = processed_image# tf.reshape(processed_image, (1, self.image_size, self.image_size, 3))

        self.inputs = tf.placeholder(tf.float32, (None, self.image_size, self.image_size, 3))


    def create_age_gender_multitask_mobilenet(self):

        logits, endpoints = mobilenet_v1.mobilenet_v1(self.inputs, num_classes=2)

        prediction_fn = tf.contrib.layers.softmax
        conv2d_12 = endpoints['Conv2d_12_pointwise']
        conv2d_13 = endpoints['Conv2d_13_pointwise']

        kernel_size = _reduced_kernel_size_for_small_input(conv2d_12, [7, 7])
        conv2d_12 = slim.avg_pool2d(conv2d_12, kernel_size, padding='VALID',
                              scope='AvgPool_1ag')

        kernel_size = _reduced_kernel_size_for_small_input(conv2d_13, [7, 7])
        conv2d_13 = slim.avg_pool2d(conv2d_13, kernel_size, padding='VALID',
                                    scope='AvgPool_1aa')


        # create logits for gender

        logits_gender = slim.conv2d(conv2d_12, self.num_gender_classes, [1, 1], activation_fn=None,
                             normalizer_fn=None, scope='Conv2d_1gender_1x1', reuse=tf.AUTO_REUSE)

        # print(logits_gender)
        endpoints['LogitsGender'] = logits_gender
        predictions_gender = prediction_fn(logits_gender, scope = 'PredictionsGender')
        logits_gender = tf.squeeze(logits_gender, [1, 2], name='SpatialSqueezeGender')
        endpoints['PredictionGender'] = predictions_gender

        #create logits for age
        logits_age = slim.conv2d(conv2d_13, self.num_age_classes, [1, 1], activation_fn=None,
                            normalizer_fn=None, scope='Conv2d_1age_1x1', reuse=tf.AUTO_REUSE)

        predictions_age = prediction_fn(logits_age, scope='PredictionsAge')
        logits_age = tf.squeeze(logits_age, [1, 2], name='SpatialSqueezeAge')
        endpoints['LogitsAge'] = logits_age
        endpoints['PredictionsAge'] = predictions_age

        return logits_gender, predictions_gender, logits_age, predictions_age, endpoints

    def load_images_and_attributes(self, image_paths, attributes, categorical = True):

        images = []
        age_labels = []
        gender_labels = []

        for idx in range(0, len(image_paths)):
            img_path = image_paths[idx]

            age = attributes[idx][FacialAttributes.AGE.value]
            gender = 0 if attributes[idx][FacialAttributes.GENDER.value] == 'f' else 1

            try:
                if categorical:
                    age = to_categorical(age, self.num_age_classes)
                    gender = to_categorical(gender, self.num_gender_classes)
                    with open(img_path, 'rb') as f:
                        img = f.read()
            except:
                continue

            images.append(sess.run(self.img_loading, feed_dict={self.image_string: img}))
            age_labels.append(age)
            gender_labels.append(gender)

        return np.asarray(images), np.asarray(age_labels), np.asarray(gender_labels)


    def evaluate(self, image_paths):



        for idx in range(0, len(image_paths)):
            img_path = image_paths[idx]
            with open(img_path, 'rb') as f:
                img = f.read()
            images = []
            images.append(self.session.run(self.img_loading, feed_dict={self.image_string: img}))
            # gender = 0
            # age = self.session.run([self.predictions_age,
            #                                self.predictions_gender], feed_dict={self.inputs: images})
            age, gender = self.session.run([self.predictions_age,
                                           self.predictions_gender], feed_dict={self.inputs: images})

            print('age size: ',len(age))
            ag = np.argmax(age)
            g = np.argmax(gender)
            if g == 0:
                g = 'male'
            else:
                g = 'female'
            print('age: ', ag)
            print('gender: ', g)

            img_cv = cv2.imread(img_path)
            cv2.putText(img_cv, g + ' ' +str(ag), (10,10),  cv2.FONT_HERSHEY_SIMPLEX, 0.5,(255,0,255),2,cv2.LINE_AA)
            cv2.imshow('image', img_cv)
            cv2.waitKey(0)

        return None

    def train_network(self, data):

        images = data[0]
        attributes = data[1]

        images = np.array(images)
        attributes = np.array(attributes)
        epoch = 0

        labels_age_placeholder = tf.placeholder(dtype=tf.int64, shape=[None, self.num_age_classes], name="gt_age")
        labels_gender_placeholder = tf.placeholder(dtype=tf.int64, shape=[None, self.num_gender_classes],
                                                   name="gt_gender")


        loss_gender = tf.nn.softmax_cross_entropy_with_logits(
            logits=self.logits_gender, labels=labels_gender_placeholder)
        loss_age = tf.nn.softmax_cross_entropy_with_logits(
            logits=self.logits_age, labels=labels_age_placeholder)

        loss = tf.reduce_mean(loss_gender) * self.weights_gender + tf.reduce_mean(loss_age) * self.weights_age
        opt = tf.train.AdamOptimizer(
            0.01,
            beta1=0.9,
            beta2=0.999,
            epsilon=1.0).minimize(loss)

        self.session.run(tf.global_variables_initializer())

        while epoch < self.num_epochs:
            # shuffle train samples
            print('Epoch', epoch,'/', self.num_epochs)

            images, attributes = shuffle(images, attributes, random_state=0)


            for batch_index in range(0, len(images) // self.batch_size):

                batch_images = images[batch_index*self.batch_size:(batch_index+1)*self.batch_size]
                batch_attributes = attributes[batch_index*self.batch_size:(batch_index+1)*self.batch_size]


                net_input, labels_age, labels_gender = self.load_images_and_attributes(batch_images, batch_attributes,
                                                                                    categorical=True)

                if epoch % 10 == 0:
                    # print('save checkpoint to ', self.save_path)
                    self.saver.save(sess, os.path.join(self.save_path, 'mtl.ckpt'))

                _, joint_Loss = self.session.run([opt, loss],
                                            {
                                                self.inputs: net_input,
                                                labels_age_placeholder: labels_age,
                                                labels_gender_placeholder: labels_gender
                                            })

                if batch_index % 10 == 0:
                    print('Epoch %d, batch index %d, loss %f ' % (epoch, batch_index, joint_Loss))

            epoch += 1

        return

if __name__ == '__main__':
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.9)


    with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
        # sess.run(tf.initialize_all_variables())
        # sess.run(tf.initialize_local_variables())
        # net = MobilenetMultitask(sess)
        # net.load_model('D:/deeplearningeval/MTL/learned-checkpoints/checkpoint')
        #
        # indir = 'D:/generated/race-isi/test/white'
        # images = [os.path.join(indir, f) for f in os.listdir(indir)]
        # net.evaluate(images)

        imdb_wiki_anno_path = '/bigdata/users-data/dadi/image-databases/imdb_crop/imdb.mat'
        imdb_wiki_data_path = '/bigdata/users-data/dadi/image-databases/imdb_crop'
        images, attributes = load_imdb_wiki.parse_database(imdb_wiki_anno_path, imdb_wiki_data_path)

        images = images[1000:]
        attributes = attributes[1000:]

        with open('test.txt', 'w') as test_file:
            for image in images[:1000]:
                test_file.write(image+'\n')

        data = (images, attributes)
        net = MobilenetMultitask(sess)

        net.train_network(data)

