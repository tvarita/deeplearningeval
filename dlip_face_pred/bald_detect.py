import glob
import math
import os

import cv2
import dlib
# !!! pip install imutils
import numpy as np
from imutils import face_utils
from keras.models import load_model

# from scipy.misc import imresize
# from scipy.ndimage import imread
# from test_on_dataset import add_prediction_maks
# from keras_applications import mobilenet
from data import standardize

# from nets.MobileUNet import custom_objects


# def custom_objects():
#     return {
#         'relu6': tf.nn.relu6,
#         'DepthwiseConv2D': DepthwiseConv2D ,
#         'BilinearUpSampling2D': BilinearUpSampling2D,
#         'dice_coef_loss': loss.dice_coef_loss,
#         'dice_coef': loss.dice_coef,
#         'recall': loss.recall,
#         'precision': loss.precision,
#     }

g_shape_predictor_path = 'shape_predictor_68_face_landmarks.dat'
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(g_shape_predictor_path)

SAVED_MODEL = 'Y:/hair/deeplearningeval/mobile-semantic-segmentation/checkpoint_weights.150--0.92.h5'


def map_prediction_to_class(pred_scipy, thresh=0.5):
    pred_scipy[pred_scipy >= thresh] = 1
    pred_scipy[pred_scipy < thresh] = 0
    return pred_scipy


def createLineIterator(P1, P2, img):
    imageH = img.shape[0]
    imageW = img.shape[1]
    P1X = P1[0]
    P1Y = P1[1]
    P2X = P2[0]
    P2Y = P2[1]

    # difference and absolute difference between points
    # used to calculate slope and relative location between points
    dX = P2X - P1X
    dY = P2Y - P1Y
    dXa = np.abs(dX)
    dYa = np.abs(dY)

    # predefine numpy array for output based on distance between points
    itbuffer = np.empty(shape=(np.maximum(dYa, dXa), 3), dtype=np.float32)
    itbuffer.fill(np.nan)

    # Obtain coordinates along the line using a form of Bresenham's algorithm
    negY = P1Y > P2Y
    negX = P1X > P2X
    if P1X == P2X:  # vertical line segment
        itbuffer[:, 0] = P1X
        if negY:
            itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
        else:
            itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
    elif P1Y == P2Y:  # horizontal line segment
        itbuffer[:, 1] = P1Y
        if negX:
            itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
        else:
            itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
    else:  # diagonal line segment
        steepSlope = dYa > dXa
        if steepSlope:
            slope = dX.astype(np.float32) / dY.astype(np.float32)
            if negY:
                itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
            else:
                itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
            itbuffer[:, 0] = (slope * (itbuffer[:, 1] - P1Y)).astype(np.int) + P1X
        else:
            slope = dY.astype(np.float32) / dX.astype(np.float32)
            if negX:
                itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
            else:
                itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
            itbuffer[:, 1] = (slope * (itbuffer[:, 0] - P1X)).astype(np.int) + P1Y

    # Remove points outside of image
    colX = itbuffer[:, 0]
    colY = itbuffer[:, 1]
    itbuffer = itbuffer[(colX >= 0) & (colY >= 0) & (colX < imageW) & (colY < imageH)]

    # Get intensities from img ndarray
    itbuffer[:, 2] = img[itbuffer[:, 1].astype(np.uint), itbuffer[:, 0].astype(np.uint)]

    return itbuffer


def get_dlib_landmarks(image_path, model):
    global g_shape_predictor_path
    # load the input image, resize it, and convert it to grayscale
    image = cv2.imread(image_path)
    h, w, c = image.shape
    # if h != 224:
    #     image = cv2.resize(image, (224, 224))
    # hair_mask = model.predict(standardize([image])).reshape(224, 224)
    # hair_mask = map_prediction_to_class(hair_mask)
    hair_mask = cv2.imread(image_path + ".png", cv2.IMREAD_GRAYSCALE)
    hair_mask[hair_mask > 100] = 255
    c_non_zero = np.count_nonzero(hair_mask)

    h, w, c = image.shape
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # detect faces in the grayscale image
    rects = detector(gray, 1)

    biggest_face_area = 0
    biggest_face_lands = None
    # loop over the face detections
    # print(hair_mask.shape)
    # print(image.shape)

    # h1 = hair_mask * 255
    # h1 = h1.astype(np.uint8)
    # print(np.amax(h1))
    _, hair_mask_thresholded = cv2.threshold(hair_mask, 128, 255, cv2.THRESH_BINARY)
    hair_mask_thresholded = hair_mask_thresholded.astype(np.uint8)
    # cv2.imshow('m1', h1)
    # cv2.waitKey(0)
    contours_hair, hierarchy = cv2.findContours(hair_mask_thresholded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    area_total_hair = 0
    centers_hair = []
    # print(contours_hair)
    for cnt in contours_hair:
        area_part = cv2.contourArea(cnt)
        # print(area_part)
        M = cv2.moments(cnt)
        # print('append:', area_part)
        try:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            centers_hair.append((cX, cY))
            area_total_hair += area_part
        except:
            continue
            # return None, None, None, None, None

    contour = []
    for (i, rect) in enumerate(rects):
        # determine the facial landmarks for the face region, then
        # convert the facial landmark (x, y)-coordinates to a NumPy
        # array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
        middle_eyebrow_left = 21
        middlw_eyebrow_right = 22
        eyebrow_left = shape[middle_eyebrow_left]
        eyebrow_right = shape[middlw_eyebrow_right]
        for pt_idx in range(0, 16):
            contour.append(shape[pt_idx])
        # x = eyebrow_left[0]
        # y = eyebrow_left[1]
        # cv2.rectangle(image, (x, y), (x + 3, y + 3), (0, 255, 0), 2)
        middle_eyebrow = [(eyebrow_left[0] + eyebrow_right[0]) // 2,
                          (eyebrow_left[1] + eyebrow_right[1]) // 2]
        x = middle_eyebrow[0]
        y = middle_eyebrow[1]
        face_width = shape[15][0] - shape[1][0]
        R = int(0.7 * face_width)
        # print(R)
        avg_radius = 0
        num = 0

        for theta in range(360, 180, -1):
            theta_r = theta * (np.pi / 180)
            deltax = R * np.cos(theta_r)
            deltay = R * np.sin(theta_r)
            x_p = int(x + deltax)
            y_p = int(y + deltay)
            FOUND = 0
            for pp in createLineIterator((x, y), (x_p, y_p), hair_mask):
                pp_0 = int(pp[0])
                pp_1 = int(pp[1])
                color = int(pp[2])

                if color == 1:
                    FOUND = 1
                    # cv2.circle(image, (pp_0, pp_1), 5, (0, 255, 255), -1)
                    num += 1
                    avg_radius += math.sqrt(math.pow(x - pp_0, 2) + math.pow(y - pp_1, 2))
                    contour.append((pp_0, pp_1))
                    break

                    # cv2.rectangle(image, (pp_0, pp_1), (pp_0 + 1, pp_1 + 1), (0, 255, 0), 1)

            if FOUND == 0:
                if num == 0:
                    avg_radius_p = 0.55 * face_width
                else:
                    avg_radius_p = avg_radius / num
                    avg_radius_p = int(avg_radius_p)

                deltax = avg_radius_p * np.cos(theta_r)
                deltay = avg_radius_p * np.sin(theta_r)
                pp_0 = int(x + deltax)
                pp_1 = int(y + deltay)
                contour.append((min(pp_0, shape[26][0]), pp_1))
                # cv2.circle(image, (pp_0, pp_1), 5, (0, 255, 255), -1)

            if False:
                cv2.imshow("Output", image)
                cv2.waitKey(10)

        contour.append(shape[0])
    face_zeroes = np.zeros(shape=(224, 224, 3), dtype=np.uint8)
    try:
        cv2.drawContours(face_zeroes, np.asarray([contour]), 0, (255, 255, 255), -1)
        face_zeroes = cv2.cvtColor(face_zeroes, cv2.COLOR_BGR2GRAY)
        c_non_zero_face = np.count_nonzero(face_zeroes)
    except:
        return None, None, None, None, None

    # cv2.imshow('czero', face_zeroes)
    # cv2.waitKey(0)

    # display = np.copy(image)
    # display = cv2.cvtColor(display, cv2.COLOR_RGB2BGR)
    # for cpt in contour:
    #     cv2.circle(display, (cpt[0], cpt[1]), 2, (0, 255, 255), -1)
    try:
        area_face = cv2.contourArea(np.asarray(contour))
    except:
        return None, None, None, None, None
    # print('area face ', area_face)
    # print('area total hair', area_total_hair)
    # print('centers hair ', centers_hair)

    # cv2.imshow("Output", display)
    # cv2.waitKey(0)
    # if area_total_hair > area_face:
    #     print('area face ', area_face)
    #     print('area total hair', area_total_hair)
    #     cv2.imshow('display', display)
    #     cv2.imwrite('face.png', display)
    #     cv2.waitKey(0)
    # print()
    # print("c_non_zero_face", c_non_zero_face)
    # print("area_face", area_face)
    # print("c_non_zero", c_non_zero)
    # print("area_total_hair", area_total_hair)
    return contour, area_face, area_total_hair, centers_hair, contours_hair


def get_imgs_from_dir(dir, limit):
    imgs = [img for img in os.listdir(dir)]
    imgs = imgs[:limit]
    return imgs


if __name__ == '__main__':
    # in_dir = 'Y:/hair/hair-noHair-test/real-bald/'
    # non_bald_dir = 'Y:/hair/hair-noHair-test/real-non-bald/'
    in_dir = 'Y:/hair/hair_color_workspace/realBald-dataset/bald'
    non_bald_dir = 'Y:/hair/hair_color_workspace/realBald-dataset/hair'
    none = 0

    correct = 0
    incorrect = 0
    # with CustomObjectScope(custom_objects()):
    #     model = load_model(SAVED_MODEL)
    model = none
    bald_imgs = glob.glob(os.path.join(in_dir, "**/*.jpg"), recursive=True)
    hair_imgs = glob.glob(os.path.join(in_dir, "**/*.jpg"), recursive=True)
    rs = []

    # get_dlib_landmarks('/work/hair/6classes/red/002331.jpg', model=model)

    for impath in bald_imgs:
        if impath.endswith('jpg'):
            face_contour, area_face, area_total_hair, centers_hair, contours_hair = get_dlib_landmarks(impath, model=model)
            if face_contour is not None:
                # print('resuls:', area_face, area_total_hair, centers_hair)

                r = area_total_hair / area_face
                rs.append(r)
                # if r > 0.095 and len(centers_hair) == 1:
                # if r > 0.095:
                if r < 0.095:
                    correct += 1
                else:
                    incorrect += 1
                # print(r)
            else:
                none += 1
    print('correct:', correct, incorrect)
    print(rs)
    print("none", none)
    print("mean", np.mean(np.asarray(rs)))

    # get_dlib_landmarks('10.jpg')
