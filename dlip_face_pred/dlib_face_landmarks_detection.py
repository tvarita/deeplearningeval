import cv2
import dlib
# !!! pip install imutils
import imutils as imutils
from imutils import face_utils


g_shape_predictor_path = '/work/hair/deeplearningeval/dlip_face_pred/shape_predictor_68_face_landmarks.dat'
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(g_shape_predictor_path)

def get_dlib_landmarks(image_path):
    global g_shape_predictor_path

    # load the input image, resize it, and convert it to grayscale
    image = cv2.imread(image_path)
    h, w, c = image.shape
    requested_width = 500
    scale_factor = requested_width/float(w)
    image = imutils.resize(image, requested_width)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # detect faces in the grayscale image
    rects = detector(gray, 1)

    biggest_face_area = 0
    biggest_face_lands = None
    # loop over the face detections
    for (i, rect) in enumerate(rects):
        # determine the facial landmarks for the face region, then
        # convert the facial landmark (x, y)-coordinates to a NumPy
        # array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        # convert dlib's rectangle to a OpenCV-style bounding box
        # [i.e., (x, y, w, h)], then draw the face bounding box
        (x, y, w, h) = face_utils.rect_to_bb(rect)
        area = w*h
        if biggest_face_area < area:
            biggest_face_area = area
            biggest_face_lands = shape

        # display functions
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # show the face number
        cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        # loop over the (x, y)-coordinates for the facial landmarks
        # and draw them on the image
        for (x, y) in shape:
            cv2.circle(image, (x, y), 1, (0, 0, 255), -1)

        # show the output image with the face detections + facial landmarks
        # cv2.imshow("Output", image)
        # cv2.waitKey(0)

    return biggest_face_lands

if __name__ == '__main__':
    get_dlib_landmarks('10.jpg')
