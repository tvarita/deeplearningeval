import math
import os
import shutil
import time

import cv2
import matplotlib.cm as cm
import numpy as np

g_default_patch_color = (128, 128, 128)
g_default_step = 1
g_default_patch_size = (64, 64)


def mask_face_color(img, px, py, pw, ph, color):
    masked_image = np.copy(img)
    masked_roi = masked_image[py: py + ph,
                 px: px + pw]
    masked_roi[:] = color
    cv2.imshow('mask_color', masked_image)
    cv2.waitKey(0)
    return masked_image


def mask_face_blur(img, px, py, pw, ph, blursz):
    masked_image = np.copy(img)
    masked_roi = masked_image[py: py + ph,
                 px: px + pw]
    masked_image[py: py + ph,
    px: px + pw] = cv2.GaussianBlur(masked_roi, (blursz, blursz), 0)
    cv2.imshow('mask_roi', masked_roi)
    cv2.imshow('mask_image', masked_image)
    cv2.waitKey(0)
    return masked_image


def dummy_classification_function(img, x, y):
    num_classes = 5
    height, width, _ = img.shape
    # dummy_predictions = np.random.rand(num_classes)
    max_dist = math.sqrt(pow(width, 2) + pow(height, 2)) / 2
    dist = math.sqrt(pow(x - width // 2, 2) + pow(y - height // 2, 2))
    dummy_predictions = [dist / max_dist] * num_classes
    return dummy_predictions


def mask_ieratively_predict(img, patch_size, patch_color, correct_class_idx, classification_function):
    height, width, _ = img.shape

    predictions_image = np.zeros((height, width), np.float32)

    start_time = time.time()
    for y in range(patch_size[1] // 2, height - patch_size[1] // 2):
        for x in range(patch_size[0] // 2, width - patch_size[0] // 2):
            masked_image = np.copy(img)
            masked_roi = masked_image[y - patch_size[1] // 2: y + patch_size[1] // 2,
                         x - patch_size[0] // 2: x + patch_size[0] // 2]
            masked_roi[:] = patch_color

            crt_prediction = classification_function(img, x, y)[correct_class_idx]

            predictions_image[y, x] = crt_prediction

    print('Execution time: %s seconds' % (time.time() - start_time))
    color_map = cm.jet(predictions_image)
    cv2.namedWindow("color-image", cv2.WINDOW_NORMAL)
    cv2.imshow("color-image", color_map)
    cv2.waitKey(0)

    return


def mask_ieratively_and_save(img_path, output_dir, patch_size, patch_color, step, size=None):
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    else:
        shutil.rmtree(output_dir)
        os.mkdir(output_dir)

    with open(os.path.join(output_dir, 'info.txt'), 'w') as info_file:
        info_file.write('patch_size: %s\n' % str(patch_size))
        info_file.write('step: %d\n' % step)
        info_file.write('patch_color: %s\n' % str(patch_color))

    print(img_path)
    img = cv2.imread(img_path)
    if size is not None:
        img = cv2.resize(img, size)

    height, width, _ = img.shape
    print('image size: ', width, 'x', height)

    img_name = os.path.basename(img_path)
    start_time = time.time()
    for y in range(patch_size[1] // 2, height - patch_size[1] // 2, step):
        for x in range(patch_size[0] // 2, width - patch_size[0] // 2, step):
            masked_image = np.copy(img)
            masked_roi = masked_image[y - patch_size[1] // 2: y + patch_size[1] // 2,
                         x - patch_size[0] // 2: x + patch_size[0] // 2]
            masked_roi[:] = patch_color
            if ".JPG" in img_name:
                name = os.path.join(output_dir, img_name.replace('.JPG', '-%d-%d.jpg' % (x, y)))
            else:
                name = os.path.join(output_dir, img_name.replace('.jpg', '-%d-%d.jpg' % (x, y)))
            cv2.imwrite(name, masked_image)

            # cv2.namedWindow("masked-image", cv2.WINDOW_NORMAL)
            # cv2.imshow("masked-image", masked_image)
            # cv2.waitKey(0)

    print('Execution time: %s seconds' % (time.time() - start_time))
    return


def get_indices_from_image_name(image_name):
    image_basename = os.path.basename(image_name)
    image_basename = os.path.splitext(image_basename)[0]
    parts = image_basename.split('-')
    x_pos = -1
    y_pos = -1
    try:
        x_pos = int(parts[-2])
        y_pos = int(parts[-1])
        # print('(', x_pos, ',', y_pos, ')')
    except:
        pass
    return x_pos, y_pos


def load_csv_file(csv_path, num_classes):
    # split_character = '\t'
    split_character = '|'
    pos_dict = {}
    with open(csv_path) as csv_file:
        lines = csv_file.readlines()
        # ignore the first line as it is the format line
        header_line = lines[0].split(split_character)

        assert len(header_line) == (
                num_classes + 2)  # we have the image name, the probability for each class and the predicted class

        num_cells = len(header_line)
        # starting from line 0 because we have no header in results file
        for line in lines[0:]:
            cells = line.split('|')
            print(len(cells), ' ', cells)
            image_name = cells[0]
            x_pos, y_pos = get_indices_from_image_name(image_name)
            probas = []
            for i in range(0, num_classes):
                probas.append(float(cells[i + 1]))
            predicted_class = int(cells[len(cells) - 1])
            pos_dict[(x_pos, y_pos)] = probas
            # print('image %s, predictions: %s, predicted proba %d' % (image_name, str(probas), predicted_class))

    # print(pos_dict)
    return pos_dict


def generate_proba_mat_step(csv_path, step, images_dir, real_class, num_classes, save_name='colormap.jpg'):
    image_paths = [os.path.join(images_dir, f) for f in os.listdir(images_dir) if f.endswith('.jpg')]
    assert (len(image_paths) >= 1)
    img = cv2.imread(image_paths[0])
    height, width, _ = img.shape
    predictions_image = np.ones((height, width), np.float32)

    proba_dict = load_csv_file(csv_path, num_classes)
    for x, y in proba_dict:
        probas = proba_dict[(x, y)]
        # TODO ??? why do we need to invert ????
        if step == 1:
            predictions_image[y, x] = probas[real_class]
        else:
            if step > 1:
                # predictions_roi = predictions_image[y - step//2: y+step //2,
                #                      x - step//2: x+step//2]
                predictions_roi = probas[real_class]
                predictions_image[y - step // 2: y + step // 2,
                x - step // 2: x + step // 2] = predictions_roi

    predictions_image[0, 0] = 0.0

    predictions_image = cv2.normalize(predictions_image, None, 0.0, 1.0, cv2.NORM_MINMAX)
    predictions_image *= 255
    predictions_image = predictions_image.astype(np.uint8)
    predictions_image = cv2.cvtColor(predictions_image, cv2.COLOR_GRAY2BGR)
    color_map = cv2.applyColorMap(predictions_image, cv2.COLORMAP_JET)

    # color_map = cm.jet(predictions_image)
    blended = cv2.addWeighted(predictions_image, 0.5, img, 0.5, 0)

    # cv2.imshow('image', img)
    cv2.imwrite('color_map_results/blended_'+save_name+'.jpg', blended)
    # cv2.imshow("color-map", color_map)
    # cv2.imwrite('colormap.jpg', color_map)
    cv2.imwrite("color_map_results/color-map_"+save_name+".jpg", color_map)
    cv2.waitKey(100)

    return


def generate_proba_mat(csv_path, images_dir, real_class, num_classes):
    image_paths = [os.path.join(images_dir, f) for f in os.listdir(images_dir) if f.endswith('.jpg')]
    assert (len(image_paths) >= 1)
    img = cv2.imread(image_paths[0])
    height, width, _ = img.shape
    predictions_image = np.zeros((height, width), np.float32)

    proba_dict = load_csv_file(csv_path, num_classes)
    for x, y in proba_dict:
        probas = proba_dict[(x, y)]
        # TODO ??? why do we need to invert ????
        predictions_image[y, x] = 1.0 - probas[real_class]

    predictions_image = cv2.normalize(predictions_image, None, 0.0, 1.0, cv2.NORM_MINMAX)
    predictions_image *= 255
    predictions_image = predictions_image.astype(np.uint8)
    predictions_image = cv2.cvtColor(predictions_image, cv2.COLOR_GRAY2BGR)
    color_map = cv2.applyColorMap(predictions_image, cv2.COLORMAP_JET)

    # color_map = cm.jet(predictions_image)
    blended = cv2.addWeighted(predictions_image, 0.5, img, 0.5, 0)

    cv2.imshow('image', img)
    cv2.imwrite('blended.jpg', blended)
    cv2.imshow("color-map", color_map)
    cv2.imwrite('colormap.jpg', color_map)

    cv2.waitKey(100)

    return


def display_help():
    print('mask_images.py - script used to alter images\n'
          'Params:\n'
          '-h: display help.\n'
          '-it [image_path] [ouput_dir] {-pw [patch_width]} {-ph [patch_height]}  {-c [color]} {-s[step]}: mask images iteratively with color patch and save result to output dir\n'
          '\timage_path - the path to the input image\n'
          '\toutput_dir - directory where the masked images will saved\n'
          '\tpatch_width, patch_height - the size of the patch used to mask the images, default (64, 64)\n'
          '\tcolor - bgr values of the color used to mask the image: by default (128, 128, 128)\n'
          '\tstep - the step size for the masking\n'
          )

    exit(-1)
    return


def parse_params(args):
    if len(args) <= 1:
        display_help()

    if args[1] == '-it':
        print('Mask image')
        if len(args) < 4:
            display_help()
        else:
            image_path = args[2]
            output_dir = args[3]

            patch_width = g_default_patch_size[0]
            patch_height = g_default_patch_size[1]
            patch_color = g_default_patch_color
            step = g_default_step
            i = 4
            while i < len(args):
                if args[i] == '-pw':
                    print('patch width')
                    i += 1
                    try:
                        patch_width = int(args[i])
                        i += 1
                    except:
                        display_help()
                if args[i] == '-ph':
                    print('patch height')
                    i += 1
                    try:
                        patch_width = int(args[i])
                        i += 1
                    except:
                        display_help()
                if args[i] == '-c':
                    print('color')
                    i += 1
                    try:
                        b = int(args[i])
                        i += 1
                        g = int(args[i])
                        i += 1
                        r = int(args[i])
                        i += 1
                        patch_color = (b, g, r)
                    except:
                        display_help()
                if args[i] == '-s':
                    print('step')
                    i += 1
                    try:
                        step = int(args[i])
                        i += 1
                    except:
                        display_help()

            mask_ieratively_and_save(image_path, output_dir, (patch_width, patch_height), patch_color, step)


def get_temp_file_name(file_name):
    split = file_name.split('.')
    extension = split[-1]
    file_without_extenssion = "".join(split[:-1])
    file_without_extenssion += "TEMPcnnEVAL"
    a = file_without_extenssion + "." + extension
    return a


def cut_top_image(input_image_path):
    from one_file_eval.preprocessing import crop_preprocessing_factory

    original_image = cv2.imread(input_image_path)
    crop_preprocessing_fn = crop_preprocessing_factory.get_preprocessing('face-top')
    cropped_image = crop_preprocessing_fn(original_image)
    cropped_file_path = get_temp_file_name(input_image_path)
    cv2.imwrite(cropped_file_path, cropped_image)
    return cropped_file_path


if __name__ == "__main__":
    input_image = "C:/Users/Tudor/Documents/slim-eval/one_file_eval/test-imgs/IMG_3498.JPG"
    occluder_samples_dir = "C:/Users/Tudor/Documents/slim-eval/one_file_eval/occluder_samples_adi"
    cut_image_path = cut_top_image(input_image)

    # mask_ieratively_and_save(cut_image_path, occluder_samples_dir, patch_size=(40, 40), patch_color=(0, 255, 67), step=15)
    results_file = occluder_samples_dir +"/results.txt"
    generate_proba_mat_step(csv_path=results_file,step=15, images_dir=occluder_samples_dir, real_class=0, num_classes=6,save_name="adi")