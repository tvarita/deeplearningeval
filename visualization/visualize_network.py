import os
import cv2
import math
import numpy as np

import tensorflow as tf
import matplotlib as mp
from nets import mobilenet_v1
import matplotlib.pyplot as plt

# import tf.slim factories
from nets import nets_factory
from datasets import dataset_factory
from preprocessing import preprocessing_factory


class NetVisualization:


    def __init__(self, sess, model_name, dataset_name, checkpoint_path, dataset_dir, eval_image_size = 224,
                 preprocessing_name = None):
        self.session = sess

        self.model_name = model_name
        self.dataset_name = dataset_name
        self.dataset_dir = dataset_dir
        self.checkpoint_path = checkpoint_path
        self.preprocessing_name = preprocessing_name or self.model_name
        self.eval_image_size = eval_image_size

        self.logits = None
        self.end_points = None

        # load the network
        self.load_network()


    def get_conv_layer_names(self):
        return None

    def load_network(self):

        # select the dataset
        dataset = dataset_factory.get_dataset(
            self.dataset_name, 'test', self.dataset_dir)

        # select the network
        network_fn = nets_factory.get_network_fn(
            self.model_name,
            num_classes=dataset.num_classes,
            is_training=False)

        image_string = tf.placeholder(
            tf.string)  # Entry to the computational graph, e.g. image_string = tf.gfile.FastGFile(image_file).read()

        # image = tf.image.decode_image(image_string, channels=3)
        image = tf.image.decode_jpeg(image_string, channels=3, try_recover_truncated=True,
                                     acceptable_fraction=0.3)  ## To process corrupted image files

        image_preprocessing_fn = preprocessing_factory.get_preprocessing(
            self.preprocessing_name,
            is_training=False)


        processed_image = image_preprocessing_fn(image, self.eval_image_size, self.eval_image_size)
        self.inputs = tf.reshape(processed_image, (1, self.eval_image_size, self.eval_image_size, 3))

        self.logits, self.end_points = network_fn(self.inputs)

        self.saver = tf.train.Saver()
        self.saver.restore(self.session, self.checkpoint_path)

    def visualize_filters(self, variable_name,  num_cols = 8, spacing = 3):

        w = [v for v in tf.global_variables() if v.name == variable_name][0]
        w_img = self.weights_2_image(w)

        image = self.session.run(w_img)
        image = np.asarray(image)

        num_filters, width, height, channels = image.shape
        big_img = None

        filters = []

        num_rows = num_filters//num_cols + 1
        for i in range(0, num_filters):

            filter = np.asarray(image[i])
            fh, fw, fc = filter.shape
            filters.append(filter)

            if big_img is None:
                size = (num_cols - 1 + spacing) * fw, (num_rows - 1 + spacing) * fh, 3
                big_img = np.zeros(size, dtype=np.uint8)

            cell_sz = fw + spacing

            y_filter = (i // num_cols) * cell_sz
            x_filter = (i % num_cols) * cell_sz
            # print(x_filter, x_filter+fw)
            # print(y_filter, y_filter + fh)

            try:
                big_img[y_filter:y_filter + fh,
                x_filter:x_filter + fw] = filter
            except:
                pass

        cv2.namedWindow('filters', cv2.WINDOW_NORMAL)
        cv2.imshow('filters', big_img)
        cv2.waitKey(0)
        return filters, big_img

    def weights_2_image(self, weights):
        # we can only display 3 channels filters
        assert (weights.shape[0] == 3)
        x_min = tf.reduce_min(weights)
        x_max = tf.reduce_max(weights)

        weights_0_to_1 = (weights - x_min) / (x_max - x_min)
        weights_0_to_255_uint8 = tf.image.convert_image_dtype(weights_0_to_1, dtype=tf.uint8)

        # to tf.image_summary format [batch_size, height, width, channels]
        weights_transposed = tf.transpose(weights_0_to_255_uint8, [3, 0, 1, 2])
        return weights_transposed


    def get_activation(self, layer_name, image):

        layer = self.end_points[layer_name]
        units = self.session.run(layer, feed_dict={self.inputs: image})
        return units

    def get_conv_filer_names(self):
        filters = []
        for end_point in self.end_points:
            if 'conv2d' in end_point.lower():
                filters.append(end_point)

        return filters

    @staticmethod
    def save_activation(units, output_dir, colormap='jet'):

        if output_dir is not None and not os.path.exists(output_dir):
            os.mkdir(output_dir)

        num_filters = units.shape[3]
        if output_dir is not None:
            for i in range(num_filters):
                filter_image = units[0, :, :, i]
                plt.imshow(filter_image, interpolation="nearest", cmap=colormap)
                output_path = os.path.join(output_dir, 'Filter-%d.jpg' % i)
                plt.savefig(output_path)
                plt.clf()

    @staticmethod
    def plot_activations(units, colormap='jet'):

        num_filters = units.shape[3]

        n_columns = 6
        n_rows = math.ceil(num_filters / n_columns) + 1

        for i in range(num_filters):
            plt.subplot(n_rows, n_columns, i + 1)

            plt.tick_params(
                axis='x',  # changes apply to the x-axis
                which='both',  # both major and minor ticks are affected
                bottom='off',  # ticks along the bottom edge are off
                top='off',  # ticks along the top edge are off
                labelbottom='off')  # labels along the bottom edge are off
            plt.tick_params(
                axis='y',  # changes apply to the x-axis
                which='both',  # both major and minor ticks are affected
                right='off',  # ticks along the bottom edge are off
                left='off',  # ticks along the top edge are off
                labelleft='off')  # labels along the bottom edge are off
            plt.tight_layout()
            filter_image = units[0, :, :, i]
            plt.imshow(filter_image, interpolation="nearest", cmap=colormap)

        plt.show()

def visualization_test():
    checkpoint_path = 'D:/generated/trained-models/mobilenet-orig-finetune/model-hair.ckpt-452589'
    dataset_dir = 'D:/work/EyeglassesDetection/face/test-jpg'
    session = tf.Session()

    vis = NetVisualization(session, 'mobilenet_v1', 'eyeglasses', checkpoint_path, dataset_dir)
    conv_filters = vis.get_conv_filer_names()

    vis.visualize_filters('MobilenetV1/Conv2d_0/weights:0')

    img = cv2.imread('C:/Users/dadi/Desktop/0/cropped/face/IMG_1699.JPG')
    img = cv2.resize(img, (224, 224))
    img = np.expand_dims(img, axis = 0)

    for conv_filter in conv_filters:
        activation = vis.get_activation(conv_filter, img)
        vis.save_activation(activation, conv_filter)

if __name__ == '__main__':
    visualization_test()

