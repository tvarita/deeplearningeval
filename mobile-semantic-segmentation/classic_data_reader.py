import glob
import os
from random import shuffle
from scipy.ndimage import imread
from scipy.misc import imresize
import cv2
import numpy as np


class ClassicDataReader():
    def __init__(self, input_dir, test_input_dir=None, validation_per_class=0, class_mapper=None, img_shape=None,extension=".jpg"):
        if img_shape is None:
            img_shape = [224, 224, 3]
        if class_mapper is None:
            class_mapper = {'black': 0, 'blond': 1, 'brown': 2, 'gray': 3, 'red': 4}

        self.input_dir = input_dir
        self.extension = extension
        self.test_input_dir = test_input_dir
        self.validation_per_class = validation_per_class
        self.img_shape = img_shape
        self.class_mapper = class_mapper
        self.entries_train = []
        self.entries_test = []
        self.entries_validation = []
        self.read_data_sets()
        self.offset_test = 0
        self.offset_validation = 0
        self.offset_train = 0
        self.epochs = 0

    def read_data_sets(self):
        if self.input_dir is not None:
            print("not none",self.input_dir)
            if os.path.exists(self.input_dir):
                print("exists", self.input_dir)
                for directory in os.listdir(self.input_dir):
                    # if directory != "bald" :
                        # if "tfrecord" not in directory and "labels" not in directory:
                            new_entries = self.read_entries(directory, self.input_dir)

                            self.entries_train.extend(new_entries[self.validation_per_class:])
                            self.entries_validation.extend(new_entries[:self.validation_per_class])

        if self.test_input_dir is not None:
            print("not none", self.test_input_dir)
            if os.path.exists(self.test_input_dir):
                print("exists", self.test_input_dir)
                for directory in os.listdir(self.test_input_dir):
                    # if directory != "bald":
                        new_entries = self.read_entries(directory, self.test_input_dir)
                        self.entries_test.extend(new_entries)

        shuffle(self.entries_train)
        shuffle(self.entries_validation)
        shuffle(self.entries_test)

        print("Length training set:", len(self.entries_train))
        print("Length validation set:", len(self.entries_validation))
        print("Length testing set:", len(self.entries_test))

    def next_test_batch(self, batch_size, do_flatten=True):
        if self.offset_test >= len(self.entries_test):
            self.offset_test = 0
            return None, None
        end = self.offset_test + batch_size
        path_and_class = self.entries_test[self.offset_test:end]
        print("test images from",self.offset_test,self.offset_test+len(path_and_class))
        self.offset_test = end
        file_paths = [path_class[0] for path_class in path_and_class]
        return self.read_images(path_and_class, do_flatten),file_paths

    def next_validation_batch(self, batch_size, do_flatten=True):
        if self.offset_validation >= len(self.entries_validation):
            self.offset_validation = 0
            shuffle(self.entries_validation)
        end = self.offset_validation + batch_size
        paths_and_class = self.entries_validation[self.offset_validation:end]
        self.offset_validation = end
        return self.read_images(paths_and_class, do_flatten)

    def next_train_batch(self, batch_size, do_flatten=True):
        # print(self.offset_train)
        end = self.offset_train + batch_size
        paths_and_class = self.entries_train[self.offset_train:end]
        self.offset_train = end
        if self.offset_train >= len(self.entries_train):
            self.offset_train = 0
            self.epochs += 1
            shuffle(self.entries_train)
            print("---------------- new epoch for training:", self.epochs, " -------------------")
        return self.read_images(paths_and_class, do_flatten)

    def read_images(self, paths_and_class, do_flatten):

        shape = (len(paths_and_class), self.img_shape[0] *self.img_shape[1],self.img_shape[2]) if do_flatten else (
            len(paths_and_class), self.img_shape[0], self.img_shape[1], self.img_shape[2])
        x_batch = np.empty(shape=shape,dtype=np.uint8)
        y_batch = np.zeros(shape=(len(paths_and_class), len(self.class_mapper.keys())))

        for i, path_and_class in enumerate(paths_and_class):

            img = imread(path_and_class[0])
            img = imresize(img, (self.img_shape[0], self.img_shape[1]))
            flatten = img.flatten() if do_flatten else img
            reshape =  np.reshape(flatten,(self.img_shape[0] *self.img_shape[1],self.img_shape[2])) if do_flatten else img
            x_batch[i] = reshape
            y_batch[i][path_and_class[1]] = 1

        return x_batch, y_batch

    def read_entries(self, directory, input_dir):
        # print(directory)
        class_nr = self.class_mapper[directory]
        in_dir = os.path.join(input_dir, directory)
        new_entries = [[img_path, class_nr] for img_path in
                       glob.glob(pathname=in_dir + "**/*"+self.extension, recursive=True)]
        return new_entries
