from __future__ import print_function

import glob
import os
from random import shuffle

import matplotlib.pyplot as plt
import time
from scipy.misc import imresize
from scipy.ndimage import imread

import cv2 as cv2

import numpy as np
from keras.models import load_model
from keras.utils import CustomObjectScope

from annotation.faceAnnotation.utils import make_sure_dir_exists
from data import standardize
from nets.MobileUNet import custom_objects
from one_file_eval.preprocessing.crop_preprocessing_factory import get_preprocessing

from scripts.padd_to_make_square import padd_to_make_square

SAVED_MODEL = '/work/hair/deeplearningeval/mobile-semantic-segmentation/checkpoint_weights.150--0.92.h5'


def map_prediction_to_class(pred_scipy):
    pred_scipy[pred_scipy >= 0.5] = 1
    pred_scipy[pred_scipy < 0.5] = 0
    return pred_scipy


size = 224
# os.environ["CUDA_VISIBLE_DEVICES"] = "1"


def main():
    with CustomObjectScope(custom_objects()):
        model = load_model(SAVED_MODEL)

    # images_path = glob.glob(pathname='test-real/attachments/**.JPG', recursive=True)
    images_path = glob.glob(pathname='/work/hair/hair_color_france/hair_color_france2/**/*.*', recursive=True)
    prep_fn = get_preprocessing("enlarge-all-direction", default_args=(1.8, 1.8, 1, 1, 1, 1))
    out_root_dir = "/work/hair/hair_color_france-hair-only2"
    make_sure_dir_exists(out_root_dir)

    c = 0
    img_paths = [img_p for img_p in images_path]
    shuffle(img_paths)

    for img_path in img_paths:
        c += 1
        print(img_path)
        img_cv = cv2.imread(img_path)
        # img_cv = cv2.resize(img_cv, (size, size))
        # img_cv = cv2.cvtColor(img_cv, cv2.COLOR_BGR2RGB)

        img_scipy = imread(img_path)

        cropped_hair_img_scipy, _, enlarge_bound = prep_fn(img_scipy)


        print(enlarge_bound)
        if len(cropped_hair_img_scipy) != 0:
            face_x, face_y, face_h, face_w = enlarge_bound
            cropped_hair_img_scipy = padd_to_make_square(cropped_hair_img_scipy)
            cropped_hair_img_scipy_resize = imresize(cropped_hair_img_scipy, (size, size))

            cropped_hair_img_cv = img_cv[face_y:face_y+face_h,face_x:face_x+face_w]
            cropped_hair_img_cv = padd_to_make_square(cropped_hair_img_cv)
            cropped_hair_img_cv = imresize(cropped_hair_img_cv, (250, 250))

            batched_scipy = [cropped_hair_img_scipy_resize]

            pred_scipy = model.predict(standardize(batched_scipy)).reshape(size, size)

            pred_scipy = map_prediction_to_class(pred_scipy)

            pred_scipy_250 = cv2.resize(pred_scipy,(250,250))
            # pred_scipy_250 *= 100
            final_color_mask_img = imresize(cropped_hair_img_scipy, (250, 250))

            cropped_hair_img_cv[pred_scipy_250 == 0] = (0, 0, 0)
            save_to_path= os.path.join(out_root_dir,os.path.basename(img_path).replace(".jpg","").replace(".png","")+".bmp")
            cv2.imwrite(save_to_path,cropped_hair_img_cv)

            # cv2.imshow('hair color 250', cropped_hair_img_cv)
            # cv2.waitKey(0)


            # cv2.imshow('face',cropped_hair_img_scipy)
            # cv2.waitKey(0)


if __name__ == '__main__':
    main()
