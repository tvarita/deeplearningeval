from __future__ import print_function

import glob
import os

import cv2
import numpy as np
from keras.models import load_model
from keras.utils import CustomObjectScope
from scipy.misc import imresize
from scipy.ndimage import imread

from data import standardize
from nets.MobileUNet import custom_objects
from one_file_eval.preprocessing.crop_preprocessing_factory import get_preprocessing

SAVED_MODEL = '/work/hair/deeplearningeval/mobile-semantic-segmentation/artifacts-face/checkpoint_weights.174--0.97.h5'

size = 224
os.environ["CUDA_VISIBLE_DEVICES"] = "1"


def add_prediction_maks(input_img, annotation_img):
    class_color_mapping = {0: (46, 255, 0), 255: (255, 0, 114)}
    h, w = annotation_img.shape
    annotation_color_img = np.zeros((h, w, 3), np.uint8)
    for label in class_color_mapping:
        annotation_color_img[annotation_img == label] = class_color_mapping[label]
    display_img = cv2.addWeighted(input_img, 0.5, annotation_color_img, 0.5, 0)
    return display_img


def map_prediction_to_class(pred_scipy):
    pred_scipy[pred_scipy >= 0.5] = 1
    pred_scipy[pred_scipy < 0.5] = 0
    return pred_scipy


def main():

    with CustomObjectScope(custom_objects()):
        model = load_model(SAVED_MODEL)

    # images_path = glob.glob(pathname='test-real/attachments/**.JPG', recursive=True)
    images_path = glob.glob(pathname='/work/hair/CFD 2.0.3 Images/**/*.jpg', recursive=True)
    prep_fn = get_preprocessing("enlarge-all-direction")
    output_root_dir = "/work/hair/cfd-faces/"
    for img_path in images_path:

        img_scipy = imread(img_path)
        cropped_face_img_scipy, _,enlarge_boundary = prep_fn(img_scipy)
        cropped_face_img_scipy = imresize(cropped_face_img_scipy, (size, size))
        batched_scipy_face = [cropped_face_img_scipy]
        pred_scipy_face = model.predict(standardize(batched_scipy_face)).reshape(size, size)
        pred_scipy_face = map_prediction_to_class(pred_scipy_face)
        img_with_pred_scipy_face = add_prediction_maks(cropped_face_img_scipy, pred_scipy_face)

        cv2.imshow('pred_scipy_face',pred_scipy_face)
        cv2.waitKey(0)

        cv2.imwrite(os.path.join(output_root_dir,os.path.basename(img_path).replace(".jpg",".bmp")),pred_scipy_face)
        cv2.imwrite(os.path.join(output_root_dir,os.path.basename(img_path)),img_with_pred_scipy_face)
        txt_name = os.path.join(output_root_dir,os.path.basename(img_path).replace(".jpg",".txt"))

        with open(txt_name, 'a') as the_file:
            the_file.write(str(enlarge_boundary[0])+","+str(enlarge_boundary[1])+","+str(enlarge_boundary[2])+","+str(enlarge_boundary[3]))

        # cv2.imshow("",img_with_pred_scipy_face)
        # cv2.waitKey(0)

if __name__ == '__main__':
    main()
