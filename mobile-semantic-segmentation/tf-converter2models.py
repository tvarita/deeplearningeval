import argparse

import tensorflow as tf
from keras import backend as K
from keras.models import load_model
from tensorflow.python.framework import graph_io
from tensorflow.python.framework.graph_util_impl import convert_variables_to_constants

from nets.MobileUNet import custom_objects

num_output = 1


def main(input_model_path, output_dir, output_fn):
    """
    Convert hdf5 file to protocol buffer file to be used with TensorFlow.
    :param input_model_path:
    :param output_dir:
    :param output_fn:
    :return:
    """
    K.set_learning_phase(0)
    input_model_path2 = '/work/hair/deeplearningeval/mobile-semantic-segmentation/artifacts-hair/checkpoint_weights.216--0.92.h5'
    model = load_model(input_model_path, custom_objects=custom_objects())
    model2 = load_model(input_model_path2, custom_objects=custom_objects())

    pred_node_names = ['output_%s' % n for n in range(num_output)]
    # print('output nodes names are: ', pred_node_names)
    output_names = ['output_0','output_0_1']
    for idx, name in enumerate(pred_node_names):
        a = tf.identity(model.output[idx], name=name)
        print(a)
        # output_names.append(a.name)

    for idx, name in enumerate(pred_node_names):
        a = tf.identity(model2.output[idx], name=name)
        print(a)
        # output_names.append(a.name)

    sess = K.get_session()
    # nn = [n.name for n in tf.get_default_graph().as_graph_def().node]
    # print(nn)
    saver = tf.train.Saver(tf.global_variables())
    saver.save(sess, output_dir + "/mobile-face")
    constant_graph = convert_variables_to_constants(sess,
                                                    sess.graph.as_graph_def(),
                                                    output_names)
    graph_io.write_graph(constant_graph, output_dir, output_fn, as_text=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input_model_path',
        type=str,
        default='/work/hair/deeplearningeval/mobile-semantic-segmentation/artifacts-face/checkpoint_weights.174--0.97.h5',
    )
    parser.add_argument(
        '--output_dir',
        type=str,
        default='artifacts-face-hair-2NETS-tf',
    )
    parser.add_argument(
        '--output_fn',
        type=str,
        default='224_1_1_face-hair.pb',
    )
    args, _ = parser.parse_known_args()

    main(**vars(args))
