from __future__ import print_function

import time

import cv2
import matplotlib.pyplot as plt
import numpy as np
from keras.models import load_model
from keras.utils import CustomObjectScope
from sklearn.model_selection import train_test_split

from data import seed, standardize
from loss import np_dice_coef
from nets.MobileUNet import custom_objects
import os

SAVED_MODEL1 = 'artifacts-face/checkpoint_weights.150--0.97.h5'
SAVED_MODEL2 = 'artifacts-face/checkpoint_weights.174--0.97.h5'

size = 224
os.environ["CUDA_VISIBLE_DEVICES"] = "1"

def display_labels(input_img, annotation_img, real_annotation, class_color_mapping=None):
    if class_color_mapping is None:
        class_color_mapping = {1: (46, 255, 0), 2: (255, 0, 114)}
    # 1 is face, 2 is hair
    h, w = annotation_img.shape
    annotation_color_img = np.zeros((h, w, 3), np.uint8)
    annotation_color_img_real = np.zeros((h, w, 3), np.uint8)

    if class_color_mapping is None:
        # generate the dictionnary that maps labels to colors
        class_color_mapping = {}
        labels = np.unique(annotation_img)
        for label in labels:
            # generate randon color
            class_color_mapping[label] = np.random.randint(0, 255, (3)).tolist()

    for label in class_color_mapping:
        annotation_color_img[annotation_img == label] = class_color_mapping[label]
        annotation_color_img_real[real_annotation == label] = class_color_mapping[label]

    display_img = cv2.addWeighted(input_img, 0.5, annotation_color_img, 0.5, 0)
    display_img_real = cv2.addWeighted(input_img, 0.5, annotation_color_img_real, 0.5, 0)

    plt.subplot(2, 2, 1)
    plt.imshow(display_img)
    plt.subplot(2, 2, 2)
    plt.imshow(display_img_real)
    plt.show()
    # cv2.imshow('display_mat', display_img)
    # cv2.waitKey()
    return display_img


def main():
    with CustomObjectScope(custom_objects()):
        model1 = load_model(SAVED_MODEL1)
        model2 = load_model(SAVED_MODEL2)

    images = np.load('data-images/images-224.npy')
    masks = np.load('data-images/masks-224.npy')

    masks = masks[:, :, :, 1].reshape(-1, size, size)

    _, images, _, masks = train_test_split(images,
                                           masks,
                                           test_size=0.2,
                                           random_state=seed)

    for img, mask in zip(images, masks):
        batched1 = img.reshape(1, size, size, 3).astype(float)
        batched2 = img.reshape(1, size, size, 3).astype(float)

        t1 = time.time()
        pred1 = model1.predict(standardize(batched1)).reshape(size, size)
        elapsed = time.time() - t1
        print('elapsed1: ', elapsed)

        mask[mask == 255] = 1
        pred1[pred1 >= 0.5] = 1
        pred1[pred1 < 0.5] = 0
        display_labels(img, pred1, mask)

        if False:
            t1 = time.time()
            pred2 = model2.predict(standardize(batched2)).reshape(size, size)
            elapsed = time.time() - t1
            print('elapsed2: ', elapsed)

            dice = np_dice_coef(mask.astype(float) / 255, pred1)
            print('dice1: ', dice)

            dice = np_dice_coef(mask.astype(float) / 255, pred2)
            print('dice2: ', dice)

            plt.subplot(2, 2, 1)
            plt.imshow(img)
            plt.subplot(2, 2, 2)
            plt.imshow(mask)
            plt.subplot(2, 2, 3)
            plt.imshow(pred1)
            plt.subplot(2, 2, 4)
            plt.imshow(pred2)
            plt.show()


if __name__ == '__main__':
    main()
