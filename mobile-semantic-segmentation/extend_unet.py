import os
import sys

import datetime

import cv2
import tensorflow as tf
from sklearn.metrics import classification_report, precision_recall_fscore_support

from classic_data_reader import ClassicDataReader
import math

from metrics.calculate_accuracy import calculate_accuracy
from scripts.getClassNumberFromOneHot import getClassNumberFromOneHot
from scripts.get_incremental_filename import get_incremental_filename
from scripts.show_wrong_classifications import show_wrong_classifications

image_size = 224
slim = tf.contrib.slim
import numpy as np
# os.environ["CUDA_VISIBLE_DEVICES"] = "0"
FLAGS = tf.flags.FLAGS
tf.flags.DEFINE_string('mode', "test", "Mode train/ test/")
tf.flags.DEFINE_string("model", "./model-bald-hair-unet/", "path to model directory")
tf.flags.DEFINE_string("input_dir", "/work/hair/hair-noHair/", "path to model directory")
tf.flags.DEFINE_string("input_dir_test", "/work/hair/realBald-dataset/", "path to model directory")
tf.flags.DEFINE_integer("n_classes", 2, "path to model directory")
tf.flags.DEFINE_integer("batch_size", 16, "path to model directory")

model_hair = '/work/hair/deeplearningeval/mobile-semantic-segmentation/artifacts-hair-face-1NET-tf/mobile-face.meta'

training = tf.equal(FLAGS.mode, 'train')


def standardize(images, mean=None, std=None):
    if mean is None:
        # These values are available from all images.
        mean = [[[29.24429131, 29.24429131, 29.24429131]]]
    if std is None:
        # These values are available from all images.
        std = [[[69.8833313, 63.37436676, 61.38568878]]]
    x = (images - np.array(mean)) / (np.array(std) + 1e-7)
    return x


def run():

    with tf.Session() as sess:

        Y = tf.placeholder(tf.int32, [None])

        new_saver = tf.train.import_meta_graph(model_hair)
        new_saver.restore(sess, tf.train.latest_checkpoint(
            '/work/hair/deeplearningeval/mobile-semantic-segmentation/artifacts-hair-face-1NET-tf/'))

        embeddings = tf.get_default_graph().get_tensor_by_name('conv2d_1/BiasAdd:0')
        op = sess.graph.get_operations()
        for m in op:
            print(m.values())
        # conv_pw_13_relu/Minimum:0
        # conv_dw_13/depthwise:0
        # conv_dw_13_bn/cond/Merge:0
        # conv_18_relu/Minimum:0
        # conv2d_1/BiasAdd:0
        X = tf.get_default_graph().get_tensor_by_name('input_1:0')

        # pool2 = tf.layers.max_pooling2d(inputs=embeddings, pool_size=[2, 2], strides=2)
        embeddings_s = embeddings.shape
        total_shape = embeddings_s[1] * embeddings_s[2] * embeddings_s[3]

        embeddings_flat = tf.reshape(embeddings, [-1, total_shape])

        dense = tf.layers.dense(inputs=embeddings_flat, units=4096, activation=tf.nn.relu)

        dropout = tf.layers.dropout(inputs=dense, rate=0.5, training=training)
        logits = tf.layers.dense(inputs=dropout, units=2)

        loss = tf.losses.sparse_softmax_cross_entropy(labels=Y, logits=logits)
        loss_summary = tf.summary.scalar("loss", loss)

        optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step())


        train_writer = tf.summary.FileWriter(FLAGS.model, sess.graph)
        saver = tf.train.Saver()
        sess.run(tf.global_variables_initializer())
        best_loss = sys.float_info.max

        ckpt = tf.train.get_checkpoint_state(FLAGS.model)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            print("Model restored from", FLAGS.model)

        if FLAGS.mode == "train":
            data_reader = ClassicDataReader(FLAGS.input_dir, None,
                                            validation_per_class=200, extension=".jpg",
                                            class_mapper={'bald': 0, 'hair': 1})
            for itr in range(9999999):

                train_images, train_annotations = data_reader.next_train_batch(FLAGS.batch_size, do_flatten=False)
                train_images = standardize(train_images)
                annotations = []
                for tr_ann in train_annotations:
                    annotations.append(np.argmax(tr_ann))
                train_annotations = np.asarray(annotations,dtype=np.int32)

                feed_dict = {X: train_images, Y: train_annotations}
                sess.run(train_op, feed_dict=feed_dict)

                if itr % 25 == 0:
                    train_loss, summary = sess.run([loss, loss_summary], feed_dict=feed_dict)
                    train_writer.add_summary(summary, itr)
                    print("Step: %d, Train_loss:%g" % (itr, train_loss))

                    if math.isnan(train_loss):
                        print('Stop traning due to NaN loss')
                        exit(-128)

                    if itr % 100 == 0 :
                        best_loss = train_loss
                        print('Save local best to ', FLAGS.model)
                        saver.save(sess, os.path.join(FLAGS.model, "model-best.ckpt"))
        elif FLAGS.mode == "test":
            data_reader = ClassicDataReader(None, FLAGS.input_dir_test,
                                            validation_per_class=0, extension=".jpg",
                                            class_mapper={'bald': 0, 'hair': 1})
            test_accuracies = []
            images_annotations,file_paths = data_reader.next_test_batch(100, do_flatten=False)
            test_images, test_annotations = images_annotations
            original_test_img = []
            for t_img in test_images:
                original_test_img.append(cv2.cvtColor(t_img, cv2.COLOR_BGR2RGB))
            test_images = standardize(test_images)
            annotations = []
            for tr_ann in test_annotations:
                annotations.append(np.argmax(tr_ann))
            test_annotations = np.asarray(annotations, dtype=np.int32)
            dir_pattern = './wrong-class-bald-%s'
            dir = get_incremental_filename(dir_pattern)

            y_true = []
            y_pred = []
            while test_images is not None:

                logits_pred = sess.run(logits, feed_dict={X: test_images})

                pred_results = getClassNumberFromOneHot(logits_pred)

                y_true.extend(pred_results)
                y_pred.extend(test_annotations)
                acc = calculate_accuracy(np.asarray(y_true), np.asarray(y_pred))
                print(acc)
                class_mapper_inv = {v: k for k, v in data_reader.class_mapper.items()}
                print(file_paths)
                show_wrong_classifications(dir, pred_results, test_annotations, original_test_img, class_mapper_inv,file_paths)

                images_annotations, file_paths = data_reader.next_test_batch(100,do_flatten=False)

                if images_annotations is not None:
                    test_images, test_annotations = images_annotations
                    original_test_img = []
                    for t_img in test_images:
                        original_test_img.append(cv2.cvtColor(t_img, cv2.COLOR_BGR2RGB))
                    test_images = standardize(test_images)
                    annotations = []
                    for te_ann in test_annotations:
                        annotations.append(np.argmax(te_ann))
                    test_annotations = np.asarray(annotations, dtype=np.int32)

                else:
                    test_images = None

            c_report = classification_report(y_true, y_pred)
            print("for model", FLAGS.model)
            print(c_report)

            print("class mapper", data_reader.class_mapper)
            matrix = tf.confusion_matrix(y_true, y_pred)

            print(matrix)

            print(sess.run(matrix))

            print("%s ---> Testing_accuracie: %g" % (datetime.datetime.now(), calculate_accuracy(np.asarray(y_true),np.asarray(y_pred))))
            precision, recall, fbeta_score, support = precision_recall_fscore_support(y_true, y_pred, average="macro")

            print("precision", precision)
            print("recall", recall)
            print("fbeta_score", fbeta_score)
            print("support", support)

run()
