import glob

import tensorflow as tf
import os

FLAGS = tf.flags.FLAGS
tf.flags.DEFINE_string("data_dir_input", "/work/hair/red-multe-celeba/", "path to input dataset")
tf.flags.DEFINE_string("data_dir_output", "/work/hair/red-multe-celeba-seg/", "path to output dataset")

keep_class_nr = 2
excluded_class_nr = 1

batch_size = 32
if not os.path.exists(FLAGS.data_dir_output):
    os.mkdir(FLAGS.data_dir_output)
for dir in os.listdir(FLAGS.data_dir_input):
        offset = 0
        out_dir = FLAGS.data_dir_output + "/" + dir
        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
        images_path = [img_path for img_path in
                       glob.glob(pathname=FLAGS.data_dir_input + dir + "/**/*.png", recursive=True)]
        if True:
                pred = sess.run(vgg_fcn.pred_annotation, feed_dict={images: images_batch})
                print("transform", offset)
                pred = np.squeeze(pred, axis=3)
                for i in range(len(images_basenames)):
                    # fill holes
                    # if "Ben Foster-blond hair7-crop-0.jpg" in images_basenames[i]:
                    #     pred_no_holes = fillHoles(pred[i], keep_class_nr,excluded_class_nr)

                    original_img = images_batch[i].copy()
                    original_img[pred[i] != keep_class_nr] = [0, 0, 0]
                    pred_no_holes = fillHoles(pred[i], keep_class_nr, excluded_class_nr)
                    image_no_holes = original_img.copy()

                    only_interest_path = out_dir + "/" + images_basenames[i].replace(".png", ".bmp")
                    # cv2.imshow('',image_no_holes)
                    # cv2.waitKey(0)
                    cv2.imwrite(only_interest_path, image_no_holes)

                print(dir, "offset", offset)