from __future__ import print_function

import glob
import os
import cv2
import numpy as np
from keras.models import load_model
from keras.utils import CustomObjectScope
from scipy.ndimage import imread
from scipy.misc import imresize

from metrics.eval_segm import mean_IU, frequency_weighted_IU, mean_accuracy, pixel_accuracy
from metrics.metrics_accuracy import calculate_all_accuracies, calculate_iu_per_class
from nets.MobileUNet import custom_objects
from data import standardize
import time
# os.environ["CUDA_VISIBLE_DEVICES"] = ""
from scripts.fillHoles import fillHoles

SAVED_MODEL = '/work/hair/deeplearningeval/mobile-semantic-segmentation/artifacts-hair-face/checkpoint_weights.100--0.93.h5'



def remove_black_contour(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)
    contours = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnt = contours[0]
    x, y, w, h = cv2.boundingRect(cnt)
    crop = img[y:y + h, x:x + w]
    return crop


def add_prediction_maks(input_img, annotation_img,add_weighted = True):
    class_color_mapping = {0: (0, 0, 0), 1: (0, 0, 255),2:(255,0,0)}
    h, w = annotation_img.shape
    annotation_color_img = np.zeros((h, w, 3), np.uint8)

    for label in class_color_mapping.keys():
        annotation_color_img[annotation_img == label] = class_color_mapping[label]
    # for i in range(annotation_color_img.shape[0]):
    #         for j in range(annotation_color_img.shape[1]):
    #             if annotation_img[i, j] == 0:
    #                 annotation_color_img[i, j] = input_img[i, j]
    if add_weighted:
        display_img = cv2.addWeighted(input_img, 0.5, annotation_color_img, 0.5, 0)
        display_img = remove_black_contour(display_img)
    else:
        display_img = annotation_color_img
    return display_img

def map_predict_to_class_multi_channel(mask):
    # class_color_mapping = {0: [0, 0, 255], 1: [0, 255, 0], 2: [255, 0, 0]}
    o = {}
    result = np.zeros(shape=(mask.shape[0],mask.shape[1]))
    for i in range(mask.shape[0]):
        for j in range(mask.shape[1]):
            i_max = np.argmax(mask[i,j])
            # print(i_max)
            result[i,j] = i_max
    # print(np.unique(result))
    return result

def map_prediction_to_class(pred_scipy, thresh=0.5):
    pred_scipy[pred_scipy >= thresh] = 1
    pred_scipy[pred_scipy < thresh] = 0
    return pred_scipy


size = 224
if __name__ == "__main__":

    with CustomObjectScope(custom_objects()):
        model = load_model(SAVED_MODEL)
    input_img_dir = "/work/hair/deeplearningeval/mobile-semantic-segmentation/test-real/one"
    # input_img_dir = "/work/segmentation_fcn/data_skin/testLfw/images/"
    # input_mask_dir = "/work/segmentation_fcn/data_skin/testLfw/annotations/"
    out_dir = "/work/hair/celebA-extra2/red-seg/"
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)


    img_paths = [p for p in glob.glob(pathname=input_img_dir + "**/*.jpg", recursive=True)]
    mask_paths = [p for p in glob.glob(pathname=input_mask_dir + "**/*.bmp", recursive=True)]

    img_paths.sort()
    mask_paths.sort()

    pixel_acc_list = []
    pixel_acc_list2 = []
    mean_pixel_acc_list = []
    mean_pixel_acc_list2 = []
    mean_iu_list = []
    mean_iu_list2 = []
    freq_w_iu_list = []
    freq_w_iu_list2 = []
    classes = [0,1, 2]
    iu_per_class = {1: []}
    c = 0

    for img_path, mask_path in zip(img_paths, mask_paths):
        c += 1
        if c % 10 == 0:
            print(c)
        # if c % 100 == 0:
        #     break
        mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)
        mask = imresize(mask, (224, 224), interp='nearest')
        # mask[mask != 2] = 0

        img = imread(img_path)
        img = imresize(img, (224, 224))

        # i = add_prediction_maks(img, mask)
        # cv2.imshow('no preprocess', i)
        # cv2.waitKey(0)
        # original_img = img.copy()
        batched= [img]
        batched =standardize(batched)

        start_time = time.time()
        pred_face = model.predict(batched)
        print("--- %s seconds ---" % (time.time() - start_time))

        pred_face = pred_face.reshape(size, size, 3)
        pred_face = map_predict_to_class_multi_channel(pred_face)
        pred_face[pred_face != 2] = 0

        # original_img = cv2.cvtColor(original_img, cv2.COLOR_BGR2RGB)
        # original_img[pred_face != 2] = [0, 0, 0]
        #
        # only_interest_path = os.path.join(out_dir , os.path.basename(img_path).replace(".png", ".bmp"))
        # cv2.imwrite(only_interest_path, original_img)

        # cv2.imshow('',original_img)
        # cv2.waitKey(0)

        pixel_acc, mean_pixel_acc, mean_iu, freq_w_iu = calculate_all_accuracies(mask, pred_face,classes)

        # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # img_with_mask_w = add_prediction_maks(img,pred_face)
        # img_with_mask = add_prediction_maks(img,pred_face,False)
        # cv2.imshow('yesss  preprocess',img_with_mask)
        # cv2.waitKey(0)
        # cv2.imwrite(os.path.join(out_dir,os.path.basename(img_path)),img)
        # cv2.imwrite(os.path.join(out_dir,os.path.basename(img_path)).replace(".jpg",".png"),img_with_mask)
        # cv2.imwrite(os.path.join(out_dir,os.path.basename(img_path)).replace(".jpg",".bmp"),img_with_mask_w)

        pixel_acc_list.append(pixel_acc)
        mean_pixel_acc_list.append(mean_pixel_acc)
        mean_iu_list.append(mean_iu)
        freq_w_iu_list.append(freq_w_iu)

    print("mean_iu", np.mean(mean_iu_list))
    print("freq_w_iu", np.mean(freq_w_iu_list))
    print("pixel_acc", np.mean(pixel_acc_list))
    print("mean_pixel_acc", np.mean(mean_pixel_acc_list))
    # os.mkdir('./out_samples')
    # img_with_pred_scipy_face = add_prediction_maks(origin_img, pred_face)
    # cv2.imshow('',img_with_pred_scipy_face)
    # cv2.waitKey(0)
    # cv2.imwrite('./out_samples/'+str(c)+".jpg",img_with_pred_scipy_face)
    # cv2.imwrite('./out_samples/'+str(c)+".png",origin_img)

    # c+=1
    # if c == 100:
    #     break
    # cv2.imshow('with mask', img_with_pred_scipy_face)
    # cv2.waitKey(0)
