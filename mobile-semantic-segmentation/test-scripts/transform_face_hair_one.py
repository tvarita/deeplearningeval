from __future__ import print_function

import glob
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import cv2
import numpy as np
from keras.models import load_model
from keras.utils import CustomObjectScope
from scipy.ndimage import imread
from scipy.misc import imresize

from nets.MobileUNet import custom_objects
from data import standardize
import time
# os.environ["CUDA_VISIBLE_DEVICES"] = ""
size = 224

SAVED_MODEL = '/work/hair/deeplearningeval/mobile-semantic-segmentation/artifacts-hair-face/checkpoint_weights.100--0.93.h5'


def map_predict_to_class_multi_channel(mask):
    # class_color_mapping = {0: [0, 0, 255], 1: [0, 255, 0], 2: [255, 0, 0]}
    o = {}
    result = np.zeros(shape=(mask.shape[0], mask.shape[1]))
    for i in range(mask.shape[0]):
        for j in range(mask.shape[1]):
            i_max = np.argmax(mask[i, j])
            # print(i_max)
            result[i, j] = i_max
    # print(np.unique(result))
    return result


with CustomObjectScope(custom_objects()):
    model = load_model(SAVED_MODEL)
input_img_dir = "/work/hair/deeplearningeval/mobile-semantic-segmentation/test-real/one"
# input_img_dir = "/work/segmentation_fcn/data_skin/testLfw/images/"
# input_mask_dir = "/work/segmentation_fcn/data_skin/testLfw/annotations/"
out_dir = "/work/hair/test/"
if not os.path.exists(out_dir):
    os.mkdir(out_dir)

img_path = '/work/hair/deeplearningeval/mobile-semantic-segmentation/test-real/one/test_ipad.jpg'
img = imread(img_path)

rows,cols,c = img.shape
M = cv2.getRotationMatrix2D((cols/2,rows/2),90,1)
img = cv2.warpAffine(img,M,(cols,rows))

batched = [img]
batched = standardize(batched)

pred_face = model.predict(batched)

pred_face = pred_face.reshape(size, size, 3)
pred_face = map_predict_to_class_multi_channel(pred_face)
pred_face[pred_face != 2] = 0

img[pred_face == 2] = [0,0,0]

cv2.imshow('',img)
cv2.waitKey(0)