from __future__ import print_function

import glob
import os
from random import shuffle

import matplotlib.pyplot as plt
import time
from scipy.misc import imresize
from scipy.ndimage import imread
import cv2
import numpy as np
from keras.models import load_model
from keras.utils import CustomObjectScope

from data import standardize
from nets.MobileUNet import custom_objects
from one_file_eval.preprocessing.crop_preprocessing_factory import get_preprocessing

SAVED_MODEL = '/work/hair/deeplearningeval/mobile-semantic-segmentation/checkpoint_weights.150--0.92.h5'

size = 224
os.environ["CUDA_VISIBLE_DEVICES"] = "1"


def add_prediction_maks(input_img, annotation_img):
    class_color_mapping = {1: (46, 255, 0), 2: (255, 0, 114)}
    h, w = annotation_img.shape
    annotation_color_img = np.zeros((h, w, 3), np.uint8)

    for label in [1]:
        annotation_color_img[annotation_img == label] = class_color_mapping[label]
        for i in range(annotation_color_img.shape[0]):
            for j in range(annotation_color_img.shape[1]):
                if annotation_img[i, j] != label:
                    annotation_color_img[i, j] = input_img[i, j]

    display_img = cv2.addWeighted(input_img, 0.5, annotation_color_img, 0.5, 0)
    # display_img = remove_black_contour(annotation_color_img)
    return display_img

def map_prediction_to_class(pred_scipy):
    pred_scipy[pred_scipy >= 0.5] = 1
    pred_scipy[pred_scipy < 0.5] = 0
    return pred_scipy

def main():
    with CustomObjectScope(custom_objects()):
        model = load_model(SAVED_MODEL)

    # images_path = glob.glob(pathname='test-real/attachments/**.JPG', recursive=True)
    images_path = glob.glob(pathname='/work/hair/celebA-extra/**/*.png', recursive=True)
    out_dir = "/work/hair/celebA-extra/segmentated/"
    prep_fn = get_preprocessing("enlarge-all-direction")
    c = 0
    img_paths = [img_p for img_p in images_path]
    shuffle(img_paths)

    for img_path in img_paths:
        c +=1
        img_cv = cv2.imread(img_path)
        img_cv = cv2.resize(img_cv, (size, size))
        img_cv = cv2.cvtColor(img_cv, cv2.COLOR_BGR2RGB)

        img_scipy = imread(img_path)

        cropped_face_img_scipy, _ ,_= prep_fn(img_scipy)
        cropped_face_img_scipy = imresize(cropped_face_img_scipy, (size, size))

        img_scipy = imresize(img_scipy, (size, size))

        batched_scipy = [img_scipy]
        batched_cv2 = [img_cv]
        # batched_scipy_face = [cropped_face_img_scipy]

        # t1 = time.time()
        pred_scipy = model.predict(standardize(batched_scipy)).reshape(size, size)
        # pred_scipy_face = model.predict(standardize(batched_scipy_face)).reshape(size, size)
        pred_cv2 = model.predict(standardize(batched_cv2)).reshape(size, size)
        # elapsed = time.time() - t1
        # print('elapsed1: ', elapsed)

        pred_scipy = map_prediction_to_class(pred_scipy)
        pred_cv2 = map_prediction_to_class(pred_cv2)


        # pred_scipy_face = map_prediction_to_class(pred_scipy_face)


        img_with_pred_scipy = add_prediction_maks(img_scipy, pred_scipy)
        img_with_pred_cv = add_prediction_maks(img_cv, pred_cv2)
        # img_with_pred_scipy_face = add_prediction_maks(cropped_face_img_scipy, pred_scipy_face)

        img_scipy = cv2.cvtColor(img_scipy, cv2.COLOR_BGR2RGB)
        img_with_pred_scipy = cv2.cvtColor(img_with_pred_scipy, cv2.COLOR_BGR2RGB)
        cv2.imshow('o',img_scipy)
        cv2.imshow('s',img_with_pred_scipy)
        cv2.waitKey(0)
        cv2.imwrite(os.path.join(out_dir,os.path.basename(img_path)),img_scipy)
        cv2.imwrite(os.path.join(out_dir,os.path.basename(img_path).replace('.png','.bmp')),img_with_pred_scipy)

        # plt.subplot(1, 4, 1)
        # plt.imshow(img_with_pred_scipy)
        # plt.subplot(1, 4, 2)
        # plt.imshow(img_with_pred_cv)
        # plt.subplot(1, 4, 3)
        # plt.imshow(img_with_pred_scipy_face)
        # plt.show()
        if c == 20:
            break

if __name__ == '__main__':
    main()
