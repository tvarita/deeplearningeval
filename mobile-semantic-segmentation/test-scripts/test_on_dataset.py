from __future__ import print_function

import glob
import  os
import cv2
import numpy as np
from keras.models import load_model
from keras.utils import CustomObjectScope
from scipy.ndimage import imread
from scipy.misc import imresize

from metrics.eval_segm import mean_IU, frequency_weighted_IU, mean_accuracy, pixel_accuracy
from metrics.metrics_accuracy import calculate_all_accuracies, calculate_iu_per_class
from nets.MobileUNet import custom_objects

# os.environ["CUDA_VISIBLE_DEVICES"] = ""
from scripts.fillHoles import fillHoles

SAVED_MODEL = '/work/hair/deeplearningeval/mobile-semantic-segmentation/checkpoint_weights.150--0.92.h5'

from data import standardize

def map_prediction_to_class(pred_scipy):
    pred_scipy[pred_scipy >= 0.2] = 1
    pred_scipy[pred_scipy < 0.2] = 0
    return pred_scipy


def remove_black_contour(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 1, 255, cv2.THRESH_BINARY)
    contours = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnt = contours[0]
    x, y, w, h = cv2.boundingRect(cnt)
    crop = img[y:y + h, x:x + w]
    return crop


def add_prediction_maks(input_img, annotation_img):
    class_color_mapping = {0: (46, 255, 0), 2: (0, 0, 255)}
    h, w = annotation_img.shape
    annotation_color_img = np.zeros((h, w, 3), np.uint8)

    for label in [2]:
        annotation_color_img[annotation_img == label] = class_color_mapping[label]
        for i in range(annotation_color_img.shape[0]):
            for j in range(annotation_color_img.shape[1]):
                if annotation_img[i,j] != label:
                    annotation_color_img[i,j] = input_img[i,j]
    
    display_img = cv2.addWeighted(input_img, 0.5, annotation_color_img, 0.5, 0)
    display_img = remove_black_contour(annotation_color_img)
    return display_img

def map_prediction_to_class(pred_scipy,thresh = 0.5):
    pred_scipy[pred_scipy >= thresh] = 1
    pred_scipy[pred_scipy < thresh] = 0
    return pred_scipy

size = 224
if __name__ == "__main__":

    with CustomObjectScope(custom_objects()):
        model = load_model(SAVED_MODEL)
    # input_img_dir = "/work/hair/deeplearningeval/scripts/test/"
    input_img_dir = "/work/segmentation_fcn/data_skin/testCelebA/images/"
    # input_mask_dir = "/work/hair/deeplearningeval/scripts/test/"
    input_mask_dir = "/work/segmentation_fcn/data_skin/testCelebA/annotations/"
    img_paths = [p for p in glob.glob(pathname=input_img_dir + "**/*.jpg", recursive=True)]
    mask_paths = [p for p in glob.glob(pathname=input_mask_dir + "**/*.bmp", recursive=True)]
    img_paths.sort()
    mask_paths.sort()
    c=0
    pixel_acc_list = []
    pixel_acc_list2 = []
    mean_pixel_acc_list = []
    mean_pixel_acc_list2 = []
    mean_iu_list = []
    mean_iu_list2 = []
    freq_w_iu_list = []
    freq_w_iu_list2 = []
    classes = [0,1,2]
    iu_per_class = {1: []}
    c= 0
    for img_path, mask_path in zip(img_paths, mask_paths):
        c+=1
        if c % 10 == 0:
            print(c)
        mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)
        # mask[mask == 1] = 0
        # mask[mask == 2] = 1
        mask = imresize(mask, (224, 224), interp='nearest')
        mask[mask!=2]=0
        # mask = map_prediction_to_class(mask)

        img = imread(img_path)
        img = imresize(img, (224, 224))

        # i = add_prediction_maks(img, mask)
        # cv2.imshow('no preprocess', i)
        # cv2.waitKey(0)

        # origin_img = cv2.imread(img_path)
        # origin_img = imresize(origin_img,(size,size))
        batched_scipy_face = [img]

        pred_face = model.predict(standardize(batched_scipy_face)).reshape(size, size)
        pred_face = map_prediction_to_class(pred_face)

        pred_squeeze = np.squeeze(pred_face)
        truth_squeeze = np.squeeze(mask)

        # i = add_prediction_maks(origin_img, pred_squeeze)
        # cv2.imshow('no preprocess', i)


        # pred_squeeze = fillHoles(pred_squeeze,interest_label=1,label_to_be_excluded=0)
        # mean_iu_list2.append(mean_IU(pred_squeeze,truth_squeeze))
        # freq_w_iu_list2.append(frequency_weighted_IU(pred_squeeze,truth_squeeze))
        # pixel_acc_list2.append(pixel_accuracy(pred_squeeze,truth_squeeze))
        # mean_pixel_acc_list2.append(mean_accuracy(pred_squeeze,truth_squeeze))

        pixel_acc, mean_pixel_acc, mean_iu, freq_w_iu = calculate_all_accuracies(truth_squeeze, pred_squeeze,
                                                                                 classes)

        # i = add_prediction_maks(origin_img,pred_squeeze)
        # cv2.imshow('yesss  preprocess',i)
        # cv2.waitKey(0)

        pixel_acc_list.append(pixel_acc)
        mean_pixel_acc_list.append(mean_pixel_acc)
        mean_iu_list.append(mean_iu)
        freq_w_iu_list.append(freq_w_iu)

    print("pixel_acc", np.mean(pixel_acc_list))
    # print("pixel_acc2", np.mean(pixel_acc_list2))
    print("mean_pixel_acc", np.mean(mean_pixel_acc_list))
    # print("mean_pixel_acc2", np.mean(mean_pixel_acc_list2))
    print("mean_iu", np.mean(mean_iu_list))
    # print("mean_iu2", np.mean(mean_iu_list2))
    print("freq_w_iu", np.mean(freq_w_iu_list))
    # print("freq_w_iu2", np.mean(freq_w_iu_list2))
        # pred_face = map_prediction_to_class(pred_face)
        # os.mkdir('./out_samples')
        # img_with_pred_scipy_face = add_prediction_maks(origin_img, pred_face)
        # cv2.imshow('',img_with_pred_scipy_face)
        # cv2.waitKey(0)
        # cv2.imwrite('./out_samples/'+str(c)+".jpg",img_with_pred_scipy_face)
        # cv2.imwrite('./out_samples/'+str(c)+".png",origin_img)

        # c+=1
        # if c == 100:
        #     break
        # cv2.imshow('with mask', img_with_pred_scipy_face)
        # cv2.waitKey(0)
