import tensorflow as tf

image_size = 224
slim = tf.contrib.slim

model_hair = '/work/hair/deeplearningeval/mobile-semantic-segmentation/artifacts/model-hair/mobile-saved'
model_face = '/work/hair/deeplearningeval/mobile-semantic-segmentation/artifacts/model-face/mobile-face'

export_dir = '/work/hair/deeplearningeval/mobile-semantic-segmentation/artifacts-face-hair-2NETS-tf/'


def run():
    with tf.Graph().as_default() as g:
        # with tf.variable_scope('hair'):
        init_fn = slim.assign_from_checkpoint_fn(model_hair,
                                                 slim.get_model_variables(''))
        with tf.Session() as sess:
            init_fn(sess)
            nn = [n.name for n in tf.get_default_graph().as_graph_def().node]
            saver = tf.train.Saver(tf.global_variables())
            saver.save(sess, export_dir + "mobile-hair-face")


run()
